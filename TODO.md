## TODOs:

### Unmapped:
 - [ ] add possibility to load colors from picture histogram (maybe Item.grabToImage()?)
 - [ ] builder: use profiletool or AmbienceInfo to find sound files, add to packageN(optional)
 - [ ] clean shelves when ambience uuid vanished
 - [ ] clean up and centralize functions and interfaces
 - [ ] consolidate shelves
 - [ ] create named shelf when exporting ambience
 - [ ] export: select picture, make ambience with current theme
 - [ ] look into Kvarken new Ambience functons (reset)
 - [ ] move handbook to html/.md files, display by WebView, translate as new project
 - [ ] move some things from dconf to Qt Settings (db)
 - [ ] support "plugins" for Generators (Name, algorithm)
 - [ ] taking a completely empty shelf to lab resets all colors to Ambience default. use this somehow?

## ROADMAP:

### 3.0+
 - [ ] SailJail: installed ambience names not accessible
 - [ ] Make Showroom fixed, scaled, non-scrollable. Scroll only Lab. Also in Flow Landscape layout...
 - [ ] daemon: bug: triggers when ambiences are edited in Settings, which is annoying
 - [ ] daemon: day/night modes: load default and night mode shelf from dconf
 - [ ] deamon: actually do something when night/day threshold is reached, not only ambience change signal (use a timer?)
 - [ ] deamon: apply scheme from top shelf on ambience changed signal
 - [ ] deamon: catch dconf writes from ambienced to determine ambience name/uuid
 - [ ] deamon: compute night scheme from top shelf if not available
 - [ ] export: select picture, make ambience with current theme
 - [ ] investigate reset resulting in black screen: https://openrepos.net/comment/38918#comment-38918
 - [ ] look into 4.x Sailfish.Silica.Background options (LightGlow/DarkGlow)
 - [ ] look into 4.x options (Materials)
 - [ ] look into 4.x options [Added support for customizing the background blur effect.](https://forum.sailfishos.org/t/release-notes-koli-4-0-1/4542)
 - [ ] move shelves storage to LocalStorage (see https://forum.sailfishos.org/t/qml-only-settings-persistent-storage/7035/9)
 - [ ] move some things from dconf to Qt Settings (db)
 - [ ] revisit/fix dimmer color
 - [ ] support "plugins" for Generators (Name, algorithm)
 - [ ] support editing blur parameters (dconf /desktop/lipstick-jolla-home, /usr/share/lipstick-jolla-home-qt5/main/Desktop.qml)
 - [ ] support editing transparency (https://openrepos.net/comment/37225#comment-37225)

### 3.0
 - [x] daemon: day/night modes: load default and night mode shelf from dconf
 - [x] deamon: apply scheme from top shelf on ambience changed signal
 - [c] deamon: catch dconf writes from ambienced to determine ambience name/uuid <-- not needed, name taken from file name
 - [x] Jolla Sandboxing: Ambience Builder does not work any more
 - [x] Jolla Sandboxing: Controlling Daemon does not work any more
 - [x] Jolla Sandboxing: Open Ambience Settings does not work any more
 - [x] Jolla Sandboxing: Showroom background image not accessible

## DONE:

### 2.9
 * [w] showroom: remorse has system colors not mypalette (not fixable, except by patching RemorseBase)
 * [x] showroom: selected menuitem has system colors not mypalette
 * [x] support loading .ambience file
 * [x] daemon: listen on dbus ambience changed signal
 * [x] export: improve file name input: https://openrepos.net/comment/38595#comment-38595
 * [x] export: improve file name input: https://openrepos.net/comment/38595#comment-38595
 * [x] make Showroom opening/closing persistent across Page and Input Mode changes (test: collapse Text-SR, switch to Copier Input Mode, Siwtch to SLider input mode --> Showroom is open!)
 * [x] make Showroom opening/closing use States: mini, text, ui, and both
 * [x] merge Slider and Text input modes
 * [x] settings: enable/disable apply-on-start
 * [x] settings: enable/disable daemon
 * [x] settings: enable/disable daemon day/night mode operation
 * [x] showroom display for selected menu item on unpatched OS

### 2.8
 * [x] improve documentation (https://openrepos.net/comment/37419#comment-37419)
 * [x] swapper: re initialize map on palette change
 * [c] export ambience file: support light ambience (replace in template) <-- was never an issue

### 2.7
 * [x] export ambience file/ generate ambience RPM: write shell script + desktop link, open on done
 * [x] preliminary support for editing transparency (https://openrepos.net/comment/37225#comment-37225)
 * [x] support writing highlightDimmerColor
 * [c] support bg color (remorse) <-- not useful
 * [x] swapper: add missing colors
 * [x] generator: add missing colors
 * [x] Generators: externalize all functions in external file, use "tint" and "darken" in generators
 * [x] steal Flow layout idea from BatteryBuddy for landscape
 * [x] correct shelves saved as dimgray, as taking them to lab makes it uses the dimgray instead of undefined

### pre 2.7
 * [x] add reset to default option (https://openrepos.net/comment/37079#comment-37079)
 * [x] autocompute theme
 * [x] handle ambience changed dbus signal
 * [x] save slots
 * [x] support backgroundGlowColor
