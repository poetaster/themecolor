#!/usr/bin/env bash

#set -x
set -e

function error_out() {
  if [ -z "$1" ]; then
	printf "ERROR: something went wrong.\n"
	notificationtool -o add --application=TCRpmBuilder --icon=icon-s-filled-warning --hint="x-nemo-feedback general_warning" "" "" "Error" "Something went wrong."
  else
	printf "%s\n" "$1"
	notificationtool -o add --application=TCRpmBuilder --icon=icon-s-filled-warning --hint="x-nemo-feedback general_warning" "" "" "Error" "$1"
  fi
  printf "exit.\n"
  exit 1
}

templates=/usr/share/openrepos-themecolor/templates
tmpdir=$(/usr/bin/mktemp -d -p /tmp -t tcrpmbuild_XXXXXX)

ambname=$(dconf read /org/nephros/openrepos-themecolor/rpmparam/ambname | sed "s/'//g" )
imgname=$(dconf read /org/nephros/openrepos-themecolor/rpmparam/imgname | sed "s/'//g" )
tcver=$(dconf read /org/nephros/openrepos-themecolor/rpmparam/tcver | sed "s/'//g" )
ambfilename=$(dconf read /org/nephros/openrepos-themecolor/rpmparam/ambfilename | sed "s/'//g" )
ambroot="/usr/share/ambience/ambience-${ambname}"

[ -z "$ambname" ] && error_out "Export was not set up correctly in ThemeColor"
[ -z "$imgname" ] && error_out "Export was not set up correctly in ThemeColor"
[ -z "$ambfilename" ] && error_out "Export was not set up correctly in ThemeColor"

# put sources
spec=${tmpdir}/SPECS/${ambname}.spec
/usr/bin/install -D  ${templates}/template.spec "$spec"  || exit 1
#install -D  ${templates}/template.ambience "${tmpdir}/BUILDROOT/${ambroot}/${ambname}.ambience"
/usr/bin/install -D -m 644 "${imgname}" "${tmpdir}/BUILDROOT/${ambroot}/images/${ambname}.jpg"  || error_out "RPM build preparation failed"
/usr/bin/install -D -m 644 "${ambfilename}" "${tmpdir}/BUILDROOT/${ambroot}/${ambname}.ambience"  || error_out "RPM build preparation failed"
# pretty print:
/usr/bin/python3 -m json.tool "${ambfilename}" > "${tmpdir}/BUILDROOT/${ambroot}/${ambname}.ambience"  || error_out "RPM build preparation failed"
sed -i -e "s/@@ambname@@/${ambname}/g"  "${tmpdir}/SPECS/${ambname}.spec"
# replace full path with our name:
sed -i -e "s@\(\"wallpaper\": \"\).*\([,\"]$\)@\1${ambname}.jpg\2@" "${tmpdir}/BUILDROOT/${ambroot}/${ambname}.ambience" || error_out "RPM build preparation failed"
# set version:
sed -i -e "s/@@tcver@@/${tcver}/g"  "${tmpdir}/SPECS/${ambname}.spec"

tag=$( date "+%m.%d.%H%M")

resdir="${HOME}/Documents"

if [ ! -w "${resdir}" ] ; then
  error_out "ERROR: output directory ${resdir} does not exist"
fi

echo all set up, trying build...
# build rpm, do not clean sources
cd "${tmpdir}" >/dev/null || error_out "Could not change directory"
rpm=$( (env LANG=C /usr/bin/rpmbuild --short-circuit --buildroot="${tmpdir}"/BUILDROOT --define "_rpmdir ${tmpdir}/out" --define "ambtag ${tag}" -bb --noclean "$spec" || error_out "RPM build failed: $?" )| awk '/Wrote:/ {print $2}' )
cp "$rpm" "${resdir}/" || error_out "Copying RPM failed: $?"
cd - >/dev/null


# and clean up
rm -rf "${tmpdir}"

echo "Done. RPM built. Launching installer on ${resdir}/$(basename ${rpm}) ..."
exec /usr/bin/xdg-open "${resdir}/"$(basename ${rpm})
