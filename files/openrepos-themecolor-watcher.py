#!/usr/bin/env python3

usage = """Usage:
python3 watcher.py
"""

# Copyright (C) 2004-2006 Red Hat Inc. <http://www.redhat.com/>
# Copyright (C) 2005-2007 Collabora Ltd. <http://www.collabora.co.uk/>
# Copyright (C) 2021 Peter G. <sailfish@nephros.org>
#
# Permission is hereby granted, free of charge, to any person
# obtaining a copy of this software and associated documentation
# files (the "Software"), to deal in the Software without
# restriction, including without limitation the rights to use, copy,
# modify, merge, publish, distribute, sublicense, and/or sell copies
# of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
# HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.

import sys
import os
import subprocess
import traceback
import time
import datetime

from gi.repository import GLib

import dbus
import dbus.mainloop.glib

import dconfjson
import json

def dconf_load(dconf_path):
        cmd = "dconf dump %s" % dconf_path
        tmp = subprocess.Popen( cmd, shell=True, stdout=subprocess.PIPE )
        (out, err) = tmp.communicate()
        config_dict = dconfjson.dconf_json(out.decode("utf-8"))
        print("loaded config from %s, found %d entries." %(dconf_path, len(config_dict)))

        return config_dict

def dconf_write(data, path):
    dconf_data = {}
    dconf_data["/"] = data
    conf =  dconfjson.json_dconf(dconf_data)
    cmd = "dconf load %s " % path
    tmp = subprocess.Popen( cmd, shell=True, stdin=subprocess.PIPE )
    (out, err) = tmp.communicate(input=conf.encode("utf-8"))

def theme_write(theme):
    print("Applying theme colors:")
    print(json.dumps(theme, sort_keys=False, indent=4))
    dconf_write(theme, "/desktop/jolla/theme/color/")

def isOpenConfigured():
    return daemonconf['autoopen'] == 'true'

def isApplyConfigured():
    return daemonconf['autoapply'] == 'true'

def isApplyNightConfigured():
    return daemonconf['autoapplynight'] == 'true'

def apply_theme(which):
    print("Will apply %s theme now..." % which)
    amb = find_current_ambience()
    shelfconf = dconf_load("/org/nephros/openrepos-themecolor/storage/")
    ambkey = amb + " "
    shelf = {}
    if ambkey in shelfconf:
        shelf = shelfconf[amb + " "]
        for key in { 'store0_p', 'store0_s', 'store0_h', 'store0_sh', 'store0_hbg', 'store0_bgg', 'store0_hdc',
                     'store1_p', 'store1_s', 'store1_h', 'store1_sh', 'store1_hbg', 'store1_bgg', 'store1_hdc' }:
            if key in shelf:
                continue
            else:
                print("Warning: shelf has no color defined for %s" % key)
                shelf[key] = "'FIXME'" # fixup later
    else:
        print("No stored theme found for %s" % amb)
        return

    if (which == "night"):
        theme = {
            'primary'              : shelf['store0_p'],
            'secondary'            : shelf['store0_s'],
            'highlight'            : shelf['store0_h'],
            'secondaryHighlight'   : shelf['store0_sh'],
            'highlightBackground'  : shelf['store0_hbg'],
            'backgroundGlow'       : shelf['store0_bgg'],
            'highlightDimmer'      : shelf['store0_hdc']
        }
    else: # "day" or "default"
        theme = {
            'primary'              : shelf['store1_p'],
            'secondary'            : shelf['store1_s'],
            'highlight'            : shelf['store1_h'],
            'secondaryHighlight'   : shelf['store1_sh'],
            'highlightBackground'  : shelf['store1_hbg'],
            'backgroundGlow'       : shelf['store1_bgg'],
            'highlightDimmer'      : shelf['store1_hdc']
        }
    # remove the fixme keys again:
    for key in { 'primary', 'secondary', 'highlight', 'secondaryHighlight', 'highlightBackground', 'backgroundGlow', 'highlightDimmer' }:
        if theme[key] ==  "'FIXME'":
            del theme[key]
    theme_write(theme)

def find_current_ambience():
    themeconf = dconf_load("/desktop/jolla/theme/")
    themeconf = themeconf['']
    themeconf = themeconf[' ']
    ambience      = themeconf["active_ambience"].replace("'", '')
    if ambience.endswith(".ambience"):
        # /usr/share/ambience/Terry/Terry_s_Big_Adventure_3.ambience
        ambname = os.path.basename(ambience)
        ambname = ambname[0:-9]
    else:
        # /home/nemo/.local/share/ambienced/wallpapers/29a2087179434c0783b78e5e4f6e98bchp.jpg
        ambname = ambience[-38:-4]
    print("Active Ambience Name: " + ambname)
    return ambname

def is_night():
    d = datetime.datetime.now()
    t = d.time()
    nh = int(daemonconf["nighthour"])
    dh = int(daemonconf["dayhour"])
    nm = int(daemonconf["nightminute"])
    dm = int(daemonconf["dayminute"])
    ### TODO: ignoring the minutes for now...
    ## midnight is night
    if ( (t.hour == 0) and (t.minute == 0) ): return True
    ## Night Thresh < Time < 24  is night
    if ( nh < t.hour <= 24 ): return True
    ## 0 < Time < DayThresh  is night
    if ( 0 < t.hour <= dh ): return True
    return False

def signal_handler(message):
    print("Received an Ambience change signal: " , message)
    time.sleep(5.0) # wait a bit for things to settle
    if isApplyConfigured():
        if isApplyNightConfigured():
            if is_night():
                apply_theme("night")
            else:
                apply_theme("day")
        else:
            apply_theme("default")
    if isOpenConfigured():
        os.system("/usr/bin/invoker -F /usr/share/applications/openrepos-themecolor.desktop -n -s --type=silica-qt5 sailfish-qml openrepos-themecolor")


if __name__ == '__main__':
    dbus.mainloop.glib.DBusGMainLoop(set_as_default=True)

    bus = dbus.SessionBus()
    daemonconf = dconf_load("/org/nephros/openrepos-themecolor/daemon/")
    daemonconf = daemonconf['']
    daemonconf = daemonconf[' ']

    try:
        object  = bus.get_object("com.jolla.ambienced","/com/jolla/ambienced")

        object.connect_to_signal("contentChanged", signal_handler, dbus_interface="com.jolla.ambienced")
    except dbus.DBusException:
        traceback.print_exc()
        sys.exit(1)

    loop = GLib.MainLoop()
    loop.run()
