import QtQuick 2.6
import Sailfish.Silica 1.0
import Nemo.Thumbnailer 1.0
import "../../components"

HelpSection {
    SectionHeader {text: qsTr("Exporting Ambiences", "Help Section") }
    HelpLabel {
      //: argument is the name of the menu entry 'Export to Ambience Package'
      text: qsTr(
        '<p>To export an Ambience, after having edited the colors, select the \"%1\" menu entry to bring up the Export page.</p>').arg(qsTr("Export to Ambience package")
      )
    }
    Image {
      anchors.horizontalCenter: parent.horizontalCenter
      source: "image://nemoThumbnail/" +  Qt.resolvedUrl("images/export.jpg")
      sourceSize.width: 768
      asynchronous: true
    }
    HelpLabel {
      //: the argument is also a translated string, the name of the builder tool
      text: qsTr(
'<p>Enter a name for your Ambience there and click the Ambience Name box to save.
This will do two things: write a .ambience file to your Documents folder, and save some settings for the %1 companion tool (see below).<br />
If you like you can now edit the .ambience file using a text editor, e.g. for adding ringtone and sound information.
</p>').arg(qsTr(AppInfo.builderDisplayName))
    }
    SectionHeader {text: qsTr("The Builder", "Help Section") }
    HelpLabel {
      //: the argument is also a translated string, the name of the builder tool
      text: qsTr(
'<p>
When ready, launch the %1.
The Builder picks up the information from dconf and the .ambience file to create an RPM file, and prompt to install it if successful.
The package will also be saved in the Documents folder so you can share it with your friends.
</p>').arg(qsTr(AppInfo.builderDisplayName))
    }
    //: as in something to add, not a small piece of paper
    SectionHeader {text: qsTr("Notes", "Help Section") }
    HelpLabel {
      text: qsTr(
'<ul>
<li>The Builder is non-graphical and non-interactive. It needs to be configured through the app as described or it will not work.</li>
<li>Installing an Ambience will switch it from Anonymous to a named Ambience. This means it will get a new Ambience shelf, which will be empty. Therefore it is a good idea to save the Scheme to the General Shelf.</li>
<li>While both the .ambience file and the RPM package include most color values, not all are picked up by lipstick when switching Ambiences.  So, after having switched, open the App again and use the Ambience Shelf to load and apply your favourite scheme.</li>
</ul>
')
    }
}
// vim: expandtab ts=4 st=4 filetype=javascript
