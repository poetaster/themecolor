import QtQuick 2.6
import Sailfish.Silica 1.0
import "../../components"

HelpSection {
    SectionHeader {text: qsTr("About", "Help Section") }
    DetailItem { label: qsTr("Version:");      value: AppInfo.versionstring }
    DetailItem { label: qsTr("Copyright:");    value: AppInfo.copyright;                                   BackgroundItem { anchors.fill: parent; onClicked: Qt.openUrlExternally(AppInfo.email) } }
    DetailItem { label: qsTr("License:");      value: AppInfo.license + " (" + AppInfo.licenseurl + ")";   BackgroundItem { anchors.fill: parent; onClicked: Qt.openUrlExternally(AppInfo.licenseurl) } }
    DetailItem { label: qsTr("Source Code:");  value: AppInfo.source;                                      BackgroundItem { anchors.fill: parent; onClicked: Qt.openUrlExternally(AppInfo.source) } }
    SectionHeader {text: qsTr("Translations") }
    Repeater {
      model: AppInfo.translators
      // TODO: how to get localized name of langid:
      //DetailItem { label: Qt.locale(langid).name + "/" + Qt.locale(langid).nativeLanguageName ;  value: persons;                          BackgroundItem { anchors.fill: parent; onClicked: Qt.openUrlExternally(AppInfo.transurl) } }
      DetailItem { label: langname + "/" + Qt.locale(langid).nativeLanguageName ;  value: persons;                          BackgroundItem { anchors.fill: parent; onClicked: Qt.openUrlExternally(AppInfo.transurl) } }

    }
}

// vim: expandtab ts=4 st=4 filetype=javascript
