import QtQuick 2.6
import Sailfish.Silica 1.0
import "../../components"

HelpSection {
    SectionHeader {text: qsTr("The four basic Colors", "Help Section"); font.pixelSize: Theme.fontSizeSmall }
    HelpLabel {
      text: qsTr('<p>Jolla Ambiences define four colors which are used for the various text elements around the UI.:<br />') +
        '<ul>' +
        '<li>' + Theme.highlightText(qsTr('primary, for most interactive elements (it is black or white depending on Ambience Scheme)'), "primary", MyPalette.primaryColor) + "</li>" +
        '<li>' + Theme.highlightText(qsTr('secondary, per default derived from primary by making it a little less opaque, is used for inactive elements, and most text'), "secondary", MyPalette.secondaryColor) + "</li>" +
        '<li>' + Theme.highlightText(qsTr('highlight, which is what you selected in the Ambience Settings, colors active or activated elements'), "highlight", MyPalette.highlightColor) + "</li>" +
        '<li>' + Theme.highlightText(qsTr('secondary highlight, derived through some magic from the highlight color.'), "secondary highlight", MyPalette.secondaryHighlightColor) + "</li>" +
        '</ul></p>'
    }
    SectionHeader {text: qsTr("Additional Colors", "Help Section"); font.pixelSize: Theme.fontSizeSmall }
    HelpLabel {
      linkColor: Theme.highlightColor
      text: qsTr('
<p>
Other colors used:<br />
<ul>
<li>Background and HighlightBackground style the background of Menus, Buttons and the like.</li>
<li>DimmerHighlightBackground does the same, but for darker areas or high contrast elements.</li>
<li>GlowColor is used for glassy things like Switches, forward/back indicators etc.</li>
<li>CoverOverlayColor is/was used for Application Covers, but see note below.</li>
</ul>
</p>
<p>
For more information about colors and other aspects of the GUI, see the official <a href="https://sailfishos.org/develop/docs/silica/qml-sailfishsilica-sailfish-silica-theme.html">Theme Documentation</a>
</p>
<p>
<i>
It must be said that while all those <b>Theme</b> colors are still defined, as SailfishOS progresses, with UI developers continue to use QML\'s<b>palette</b> property directly.
As we only deal with Theme colors, this unfortunately means more and more UI components stop reacting to those changes.
</i>
</p>
')
    }
}
// vim: expandtab ts=4 st=4 filetype=javascript
