import QtQuick 2.6
import Sailfish.Silica 1.0
import Nemo.Thumbnailer 1.0
import "../../components"

HelpSection {
    SectionHeader {text: qsTr("The Watcher Daemon", "Help Section") }
    HelpLabel {
      text: qsTr('<p>The Watcher Daemon is a background service which can take care of some tasks while %1 is not running. Its main purpose is to apply color schemes after the %2 has changed.</p>').arg(AppInfo.displayName).arg(qsTr("Ambience"))
          + qsTr('<p>It can be configured by tapping the <i>Daemon Settings</i> button on the Advanced page.</p>')
          + qsTr('<p>This is an ever-evolving (and experimental) feature with more functions added each release.</p>')
          + qsTr('<p>One upcoming feature is to apply &quot;Night Mode&quot; color schemes at the appropriate time.</p>')
    }
    Image {
      anchors.horizontalCenter: parent.horizontalCenter
      source: "image://nemoThumbnail/" +  Qt.resolvedUrl("images/daemonsettings.jpg")
      sourceSize.width: 768
      asynchronous: true
    }

}
// vim: expandtab ts=4 st=4 filetype=javascript
