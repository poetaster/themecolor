import QtQuick 2.6
import Sailfish.Silica 1.0

//LinkedLabel {
Label {
  anchors {
    horizontalCenter: parent.horizontalCenter
    leftMargin: Theme.horizontalPageMargin
    rightMargin: Theme.horizontalPageMargin
  }
  width: parent.width
  color: Theme.primaryColor
  font.pixelSize: Theme.fontSizeSmall
  horizontalAlignment: Text.AlignJustify
  wrapMode: Text.WordWrap
  textFormat: Text.StyledText
  //plainText: this.text
}

// vim: expandtab ts=4 st=4 filetype=javascript
