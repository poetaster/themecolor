import QtQuick 2.6
import Sailfish.Silica 1.0
import "../../components"

HelpSection {
    SectionHeader {text: qsTr("Tips and Caveats", "Help Section") }
    HelpLabel {
      text: qsTr('
<p>
The app frequently gets confused about colors when switching editing modes, applying colors to system, or taking palettes from the Cupboard. If that happens, try reloading from the system, that helps in most cases.<br />
</p>
<p>
It is possible to create color schemes which make parts of the UI unreadable. Check especially non-obvious areas such as the virtual Keyboard.<br />
It\'s a good idea to store a known-good color scheme in the Cupboard so you can go restore easily.
</p>
<p>
If you have messed up the colors somehow, use the PullUp menu option to reset everything, or from command line issue:<br />
<pre>dconf dump /desktop/jolla/theme/color/</pre>
<pre>dconf reset /desktop/jolla/theme/color/highlightBackground</pre>
and repeat for all the other colors stored there. <br />
Changing the Ambience from the System Settings may also help.
</p>
')
    }
}

// vim: expandtab ts=4 st=4 filetype=javascript
