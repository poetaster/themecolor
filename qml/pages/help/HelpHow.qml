import QtQuick 2.6
import Sailfish.Silica 1.0

HelpSection {
    //SectionHeader {text: qsTr("Technical Stuff") }
    SectionHeader {text: qsTr("Loading and Applying Colors", "Help Section"); font.pixelSize: Theme.fontSizeSmall }
    HelpLabel {
      text: qsTr('
<p>
        Ambience Colors are stored in several places: the .ambience file for installed Ambiences, a user-local database for user-created Ambiences, and <i>dconf</i>.
        When an Ambience is loaded, its color values are written to a DConf location, and lipstick reads the values from there and uses it for its Theme.<br />
</p>
<p>
        We use that last part in this application, in that when we "Apply" colors to the system, we simply write to that DConf location, and lipstick notices and adjusts it theme.
        The good thing is, while official Ambiences only define four colors (two of which are always the same in practice), writing additional values to that location works, if they are defined as changable by the Sailfish Theme.
</p>
<p>
        The dconf path in question is <pre>/desktop/jolla/theme/color</pre> and can be read from command line using <pre>dconf dump</pre>
</p>
')
    }
    SectionHeader {text: qsTr("App Configuration and State", "Help Section"); font.pixelSize: Theme.fontSizeSmall }
    HelpLabel {
      text: qsTr('
<p>
        Speaking of dconf, the application uses it in its own location to store things like Shelves and information for the builder.<br />
        Unfortuately, if the user creates and deletes Ambiences a lot, and uses the Ambience Shelf, there might be a lot of stale setting in there which may warrant cleaning out from time to time.
</p>
<p>
        The location for this is <pre>/org/nephros/openrepos-themecolor/storage</pre>
</p>
')
    }
    SectionHeader {text: qsTr("The Builder",  "Help Section"); font.pixelSize: Theme.fontSizeSmall }
    HelpLabel {
      text: qsTr('
<p>
        The Ambience builder is a shell script launched from a oneshot systemd service with the following mode of operation:
</p>
<p>
        First it reads DConf values prepared by the App to figure out things like Ambience name and location of files.<br />
        It then puts them in a temporary directory together with a .spec file. It calls the <pre>rpmbuild</pre> command which produces (hopfully) a package.<br />
        If successful, that package is opened, prompting the user to install it.
</p>
')
    }
}

// vim: expandtab ts=4 st=4 filetype=javascript
