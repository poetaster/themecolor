import QtQuick 2.6
import Sailfish.Silica 1.0
import Nemo.Thumbnailer 1.0
import "../../components"

HelpSection {
    SectionHeader {text: qsTr("The main UI sections", "Help Section") }
   HelpLabel {
     text: qsTr('<p>The main UI consists of the following:
       <ul>
       <li>The Showroom</li>
       <li>The Laboratory</li>
       <li>The Cupboards</li>
       <li>PullDown Menu</li>
       </ul></p>')
   }
   SectionHeader {text: qsTr("The Showroom", "Help Section"); font.pixelSize: Theme.fontSizeSmall }
   Image {
     anchors.horizontalCenter: parent.horizontalCenter
     source: "image://nemoThumbnail/" + Qt.resolvedUrl("images/showroom.jpg")
     sourceSize.width: 768
     asynchronous: true
   }
   HelpLabel {
     //: argument is the translation of "Showroom"
     text: qsTr('<p>The top area on the first page (%1) shows the colors that are selected currently.
This is so you can preview and check how your creations will look.</p>
<p>
There are two parts: the top one showing text in various combinations of colors, and the bottom one for the UI elements like buttons etc.
Tapping on either of those parts will hide it. When both are hidden, a miniature version will appear.<br />
To unhide all again tap the section header.
</p>'
).arg('"Showroom"')
   }
   Image {
     anchors.horizontalCenter: parent.horizontalCenter
     source: "image://nemoThumbnail/" + Qt.resolvedUrl("images/minisr.jpg")
     sourceSize.width: 768
     asynchronous: true
   }
   SectionHeader {text: qsTr("The Laboratory", "Help Section"); font.pixelSize: Theme.fontSizeSmall }
   HelpLabel {
     //: argument is the translation of "Laboratory"
     text: qsTr('<p>The next component is the %1. This is the where you edit the various colors.</p>
<p>
On the top you find the input mode selector.
There are several ways to do this editing, called Input Modes, explained below.
The selector switches between them.
<ul>'
).arg("Laboratory") + qsTr(
'<li><strong>Slider</strong> input mode is the main editing mode.<br />
There is a set of sliders for each color you can edit. Use them to adjust the colors using their red, green, blue (RGB) channels. 
Two slim indicators on top of each slider section shows the current, and the edited color, and the hex value will be shown in the area below them. If you tap that area, the sliders will reset to the value of the unedited color.<br />
The button with the keyboard symbol opens a dialog where you can put in the value directly.<br />
The button on the left side opens the Color Selector dialog from Sailfish Silica, in case you want to select a color from there.<br />
At the bottom below the RGB sliders you will find the Glow editor. This works a little different than the others, allowing you to select the hue of any UI elements that have a glow effect. Below the slider there is a set of buttons that apply either black, white, or the four default colors to the Glow color.'
)
+ qsTr(
'<li><strong>Generators:</strong> this is a collection of various functions which manipulate all colors of the theme. Some are more useful than others, some are just for fun.
<ul>
<li><i>Randomizer</i> shuffles the color values to generate a random theme</li>
<li><i>Filters</i> will change the existing theme colors slightly, e.g. darken or brighten them</li>
<li><i>Scheme Generators</i> is a collection of things which will create a full theme</li>
</ul>
</li>'
)
+ qsTr( '<li><strong>Swapper/Copier</strong> is a helper mode which lets you copy the value of one color to another, or swap two values.</li>')
+ qsTr( '<li><strong>Jolla Original</strong> you already know, a remake of the element used in the Ambience Settings</li>')
+ '</ul></p>'
   }
   Image {
     anchors.horizontalCenter: parent.horizontalCenter
     source: "image://nemoThumbnail/" + Qt.resolvedUrl("images/slider.jpg")
     sourceSize.width: 768
     asynchronous: true
   }
   Image {
     anchors.horizontalCenter: parent.horizontalCenter
     source: "image://nemoThumbnail/" + Qt.resolvedUrl("images/generator.jpg")
     sourceSize.width: 768
     asynchronous: true
   }
   SectionHeader {text: qsTr("The Cupboard", "Help Section"); font.pixelSize: Theme.fontSizeSmall }
   HelpLabel {
     //: arguments are the names of the menu entries "Put on Shelf" and "Take to Lab"
     text: qsTr('<p>
Cupboards are found to the right of the main page. These allow you to store your created palettes for re-use later.<br />
The first page is the global Cupboard, where you can store any palette.
The second page is the Cupboard specific for the current Ambience, and its contents will change when the Ambience changes.
</p>
<p>
Note that only Ambiences installed from a package can have a name, those created from Gallery will show as anonymous (for now)
</p>
<p>
Per default the shelves are empty (showing all color pots as gray), but tapping the <i>%1</i> button will save your current palette to that shelf, overwriting any values that may have been there.
The <i>%2</i> button will load the stored palette and switch back to the main page.
</p>
'
).arg(qsTr("Put on Shelf")).arg(qsTr("Take to Lab"))
   }
   Image {
     anchors.horizontalCenter: parent.horizontalCenter
     source: "image://nemoThumbnail/" + Qt.resolvedUrl("images/shelf.jpg")
     sourceSize.width: 768
     asynchronous: true
   }
   SectionHeader {text: qsTr("Menus", "Help Section"); font.pixelSize: Theme.fontSizeSmall }
   Image {
     anchors.horizontalCenter: parent.horizontalCenter
     source: "image://nemoThumbnail/" + Qt.resolvedUrl("images/menu.jpg")
     sourceSize.width: 768
     asynchronous: true
   }
   HelpLabel {
     text: qsTr('<p>
       The PullDown menu contains the most used commands.
       <p>
       <ul>
       <li>Apply: The central command of the app. This will apply the current color theme to the system.</li>
       <li>Reload: Loads the theme that is currently in use in the system.</li>
       <li>Export: opens the Export Ambience page. See the <b>Export</b> chapter for more.</li>
       <li>Advanced: takes you to a page containing various lesser used, experimental or destructive commands.</li>
       </ul>
       </p>')
   }
}

// vim: expandtab ts=4 st=4 filetype=javascript
