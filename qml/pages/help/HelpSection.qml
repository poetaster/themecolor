import QtQuick 2.6
import Sailfish.Silica 1.0

Column {
  width: (isLandscape) ? ( parent.width * 2/3)  : parent.width - Theme.horizontalPageMargin
  spacing: (isLandscape) ? Theme.paddingMedium : Theme.paddingLarge
  anchors.horizontalCenter: parent.horizontalCenter
}

// vim: expandtab ts=4 st=4 filetype=javascript
