import QtQuick 2.6
import QtQml.Models 2.1
import Sailfish.Silica 1.0
import "../components"
import "help"

Page {
  id: helppage

  allowedOrientations: (devicemodel === 'planetgemini') ? Orientation.LandscapeInverted : defaultAllowedOrientations
  ObjectModel {
    id: helpObjModel
    Component { Page { property string name; PageHeader{title: parent.name} SilicaFlickable { contentHeight: contentItem.childrenRect.height; clip: true; width: parent.width; height: parent.height - Theme.itemSizeMedium; anchors.bottom: parent.bottom; HelpGeneral{}  } } }
    Component { Page { property string name; PageHeader{title: parent.name} SilicaFlickable { contentHeight: contentItem.childrenRect.height; clip: true; width: parent.width; height: parent.height - Theme.itemSizeMedium; anchors.bottom: parent.bottom; HelpUIMain{}   } } }
    Component { Page { property string name; PageHeader{title: parent.name} SilicaFlickable { contentHeight: contentItem.childrenRect.height; clip: true; width: parent.width; height: parent.height - Theme.itemSizeMedium; anchors.bottom: parent.bottom; HelpColors{}   } } }
    Component { Page { property string name; PageHeader{title: parent.name} SilicaFlickable { contentHeight: contentItem.childrenRect.height; clip: true; width: parent.width; height: parent.height - Theme.itemSizeMedium; anchors.bottom: parent.bottom; HelpExport{}   } } }
    Component { Page { property string name; PageHeader{title: parent.name} SilicaFlickable { contentHeight: contentItem.childrenRect.height; clip: true; width: parent.width; height: parent.height - Theme.itemSizeMedium; anchors.bottom: parent.bottom; HelpDaemon{}   } } }
    Component { Page { property string name; PageHeader{title: parent.name} SilicaFlickable { contentHeight: contentItem.childrenRect.height; clip: true; width: parent.width; height: parent.height - Theme.itemSizeMedium; anchors.bottom: parent.bottom; HelpHow{}      } } }
    Component { Page { property string name; PageHeader{title: parent.name} SilicaFlickable { contentHeight: contentItem.childrenRect.height; clip: true; width: parent.width; height: parent.height - Theme.itemSizeMedium; anchors.bottom: parent.bottom; HelpTips{}     } } }
    Component { Page { property string name; PageHeader{title: parent.name} SilicaFlickable { contentHeight: contentItem.childrenRect.height; clip: true; width: parent.width; height: parent.height - Theme.itemSizeMedium; anchors.bottom: parent.bottom; HelpCredits{}  } } }
  }
  ListModel {
    id: helpIndexModel
    ListElement{ name: qsTr("General Information"       , "Help Index") }
    ListElement{ name: qsTr("About the UI"              , "Help Index") }
    ListElement{ name: qsTr("About Colors"              , "Help Index") }
    ListElement{ name: qsTr("Exporting Ambiences"       , "Help Index") }
    ListElement{ name: qsTr("Watcher Daemon"            , "Help Index") }
    ListElement{ name: qsTr("How it works"              , "Help Index") }
    ListElement{ name: qsTr("Limitations , Tips etc."   , "Help Index") }
    ListElement{ name: qsTr("Credits"                   , "Help Index") }
  }
  SilicaListView {
    id: view
    anchors.fill: parent
    width: parent.width; height: parent.height
    header: PageHeader {
      id: head
      //: User Manual, Application Manual, Handbook, Howto
      title: qsTr("Handbook", "Help Index")
      }
    //footer: Label { width: view.width; anchors.horizontalCenter: parent.horizontalCenter; text: qsTr("Please select a help section above") }
    model: helpIndexModel
    delegate: ValueButton {
      //: chapter in the handbook. argument is a number
      label: qsTr("Chapter %1:", "Help Index Chapter Prefix").arg(index + 1 )
      //labelColor: Theme.secondaryColor
      //valueColor: Theme.secondaryHighlightColor
      value: name
      //description: "click to open"
      onClicked: pageStack.push(helpObjModel.get(index), { "name": name })
    }
  }

}

// vim: expandtab ts=4 st=4 filetype=javascript
