import QtQuick 2.6
import Sailfish.Silica 1.0
import "../components"

Page {
  id: page

  // map dropdown selection to ints
  //TODO: is there a more native type for this?
  readonly property var inputMode: QtObject {
    property int sliders: 0
    property int swapper: 1
    property int generators: 2
    property int jolla: 3
    property int defaultValue: this.sliders
  }

  Component.onCompleted: { initColors() }
  onStatusChanged: {
    if ( status === PageStatus.Active && pageStack.nextPage() === null ) { pageStack.pushAttached(Qt.resolvedUrl("Saver.qml")) }
  }

  allowedOrientations: (devicemodel === 'planetgemini') ?  Orientation.LandscapeInverted : defaultAllowedOrientations

  SilicaFlickable {
    id: flick
    anchors.fill: parent
    contentHeight: flw.height
    PageHeader { id: head ; title: qsTr("Adjust Theme Colors") }
    Flow {
      id: flw
      width: parent.width - Theme.horizontalPageMargin
      anchors.horizontalCenter: parent.horizontalCenter
      anchors.top: head.bottom
      spacing: 0
      //spacing: Theme.paddingLarge
      // cool, but too noisy/jumpy
      //move: Transition { NumberAnimation { properties: "x,y"; duration: 240; easing.type: Easing.OutQuad } }
      add:      Transition { FadeAnimation { duration: 1200 } }
      move:     Transition { FadeAnimation { duration: 1200 } }
      populate: Transition { FadeAnimation { duration: 1200 } }
      //PageHeader { id: head ; title: qsTr("Adjust Theme Colors") }
      Column {
        id: srcol
        width: (isLandscape) ? ( parent.width / 2)  - Theme.horizontalPageMargin : parent.width
        CollapsingHeader {
          text: qsTr("Showroom")
          target: showroom
          BackgroundItem { anchors.fill: parent; onClicked: {
              showroom.visible = ! showroom.visible;
              app.showroomCollapsed = ! app.showroomCollapsed;
            }
          }
        }
        Column {
          id: showroom
          width: parent.width
          anchors.horizontalCenter: parent.horizontalCenter
          spacing: Theme.paddingMedium
          //add:  Transition { FadeAnimation { duration: 1200 } }
          states: [
            State { name: "both" },     // must be first!!
            State { name: "uionly" },
            State { name: "txtonly" },
            State { name: "mini" }
          ]
          Component.onCompleted: state = app.showroomState ? app.showroomState : states[0].name
          onStateChanged: { app.showroomState = state }
          onVisibleChanged: { visible ? state = app.showroomState : true }
          ShowRoomMini {
            enabled: visible
            visible: showroom.state == "mini"
            theme: MyPalette
            BackgroundItem { anchors.fill: parent; onClicked: { showroom.state = "both" } }
          }
          ShowRoom {
            id: sr1
            state: "text"
            enabled: visible
            visible: ( showroom.state == "both" || showroom.state == "txtonly" )
            BackgroundItem { anchors.fill: parent; onClicked: { showroom.state = ( showroom.state == "both" ) ? "uionly" : "mini" } }
          }
          ShowRoom {
            id: sr2
            state: "ui"
            enabled: visible
            visible: ( showroom.state == "both" || showroom.state == "uionly" )
            BackgroundItem { anchors.fill: parent; onClicked: { showroom.state = ( showroom.state == "both" ) ? "txtonly" : "mini" } }
          }
        }
      }
      Column {
        id: labcol
        width: (isLandscape) ? ( parent.width / 2)  - Theme.horizontalPageMargin : parent.width
        //width: isLandscape ? parent.width / 2 : parent.width
        CollapsingHeader {
            width: parent.width
            text: qsTr("Laboratory")
            target: editcol
            BackgroundItem { anchors.fill: parent;
              onClicked: {
                      modeSelector.visible = ! modeSelector.visible;
                      editcol.visible = ! editcol.visible;
                      app.labCollapsed = ! app.labCollapsed;
              }
            }
        }
        ComboBox {
          id: modeSelector
          width: parent.width
          anchors.horizontalCenter: parent.horizontalCenter
          label: qsTr("Input Mode" + ":")
          description: qsTr("Tap to switch")
          currentIndex: inputMode.defaultValue
          menu: ContextMenu {
            // TODO: anchoring throws Type errors, but does work...
            // we need these two because the menu otherwise gets full-width and centered in landscape view.
            anchors.horizontalCenter: ( parent !== null ) ? parent.horizontalCenter : undefined
            container: modeSelector; width: container.width

            MenuItem { text: qsTr("Sliders") }
            MenuItem { text: qsTr("Swapper/Copier") }
            MenuItem { text: qsTr("Generators") }
            MenuItem { text: qsTr("Jolla Original") }
          }
        }
        Column {
          id: editcol
          anchors.horizontalCenter: parent.horizontalCenter
          width: parent.width
          visible: !app.labCollapsed
          ColorSliders {
              visible: modeSelector.currentIndex === inputMode.sliders
              //opacity: visible ? 1.0 : 0
              //Behavior on opacity { FadeAnimation { duration: 800 } }
          }
          //ColorTextInputs {
          //    visible: modeSelector.currentIndex === inputMode.text
          //    opacity: visible ? 1.0 : 0
          //    Behavior on opacity { FadeAnimation { duration: 800 } }
          //}
          ColorSwapper {
              visible: modeSelector.currentIndex === inputMode.swapper
              //opacity: visible ? 1.0 : 0
              //Behavior on opacity { FadeAnimation { duration: 800 } }
          }
          ColorSwapper {
              visible: modeSelector.currentIndex === inputMode.swapper
              //opacity: visible ? 1.0 : 0
              //Behavior on opacity { FadeAnimation { duration: 800 } }
              copy: true
          }
          ColorGenerators {
              visible: modeSelector.currentIndex === inputMode.generators
              //opacity: visible ? 1.0 : 0
              //Behavior on opacity { FadeAnimation { duration: 800 } }
              GeneratorRemorse { id: generatorRemorse }
          }
          CollaSlider {
              visible: modeSelector.currentIndex === inputMode.jolla
              //opacity: visible ? 1.0 : 0
              //Behavior on opacity { FadeAnimation { duration: 800 } }
          }
          // spacer to pullupmenu:
          SilicaItem {
            width: parent.width
            height: Theme.itemSizeLarge
          }
        }
      }
    }
    PullDownMenu {
        MenuItem { text: qsTr("Handbook", "User Manual, Application Manual, Handbook, Howto");
                   onClicked: { pageStack.push(Qt.resolvedUrl("HelpPage.qml")) }
                 }
        MenuItem { text: qsTr("Advanced…");
                   onClicked: { pageStack.push(Qt.resolvedUrl("AdvancedPage.qml"), { isAdvanced: true }) }
                 }
        MenuItem { text: qsTr("Export to Ambience package");
                   onClicked: { pageStack.push(Qt.resolvedUrl("advanced/SaveAmbience.qml")) }
                 }
        MenuItem { text: qsTr("Apply Colors to System");
                   onClicked: { applyRemorse.execute(qsTr("Applying") + "…", function () { applyThemeColors() } ) }
                 }
        MenuItem { text: qsTr("Reload Colors from System");
                   onClicked: {
                     reloadThemeColors();
                     colorsInitialized = false;
                     initColors()
                   }
                 }
    }
    /*
    PushUpMenu {
        MenuLabel { text: qsTr("Experimental or dangerous actions") }

        MenuItem { text: qsTr("Reset all values and restart");
                    onClicked: { applyRemorse.execute( qsTr("Resetting") + "…", function () { resetDconf(); restartLipstick(); } ) }
                 }
        MenuItem { text: qsTr("Reset nonstandard values");
                    onClicked: { applyRemorse.execute( qsTr("Resetting") + "…", function () { resetDconf(); } ) }
                 }
    }
    */
    RemorsePopup { id: applyRemorse }
    VerticalScrollDecorator {}
  }
}

// vim: expandtab ts=4 st=4 filetype=javascript
