import QtQuick 2.6
import Sailfish.Silica 1.0
import ".."

Switch {
  property color iconcolor
  icon.sourceSize.height: Theme.iconSizeMedium;
  icon.source: "image://theme/icon-m-ambience?" + iconcolor
  TouchBlocker { anchors.fill: parent }
}
// vim: expandtab ts=4 st=4 filetype=javascript
