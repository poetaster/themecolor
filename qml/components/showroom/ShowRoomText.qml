import QtQuick 2.6
import Sailfish.Silica 1.0
import ".."

Column {
  id: textcol
  anchors.horizontalCenter: parent.horizontalCenter
  width: parent.width
  SilicaItem {
    anchors.horizontalCenter: parent.horizontalCenter
    width: parent.width
    height: lblcol0.height
    Column {
      id: lblcol0
      width: parent.width
      spacing: Theme.paddingSmall
      Label { anchors.horizontalCenter: parent.horizontalCenter; horizontalAlignment: Text.AlignHCenter ; text: qsTr("A very long line showing Text in") + " " + qsTr("Primary Color");   color: MyPalette.primaryColor }
      Label { anchors.horizontalCenter: parent.horizontalCenter; horizontalAlignment: Text.AlignHCenter ; text: qsTr("A very long line showing Text in") + " " + qsTr("Secondary Color"); color: MyPalette.secondaryColor }
      Label { anchors.horizontalCenter: parent.horizontalCenter; horizontalAlignment: Text.AlignHCenter ; text: qsTr("A very long line showing Text in") + " " + qsTr("Highlight Color"); color: MyPalette.highlightColor }
      Label { anchors.horizontalCenter: parent.horizontalCenter; horizontalAlignment: Text.AlignHCenter ; text: qsTr("A very long line showing Text in") + " " + qsTr("Secondary Highlight Color");   color: MyPalette.secondaryHighlightColor }
      //Label { anchors.horizontalCenter: parent.horizontalCenter; horizontalAlignment: Text.AlignHCenter ; text: qsTr("A very long line showing Text in") + " " + qsTr("Error Color");     color: MyPalette.errorColor}
    }
  }
  /* ************* Background Colors *****************/
  ShowRoomTextBG {
    //bgcolor: MyPalette.highlightBackgroundColor
    //bgopacity: MyPalette.highlightBackgroundOpacity
    bgcolor: Theme.rgba(MyPalette.highlightBackgroundColor, MyPalette.highlightBackgroundOpacity)
    text1: qsTr("Primary Color") + " " + qsTr("Text") + ", " + qsTr("Highlight Background Color")
    text2: qsTr("Secondary Color") + " " + qsTr("Text") + ", " + qsTr("Highlight Background Color")
    textcolor1: MyPalette.primaryColor
    textcolor2: MyPalette.secondaryColor
  }

  ShowRoomTextBG {
    bgcolor: MyPalette.overlayBackgroundColor
    bgopacity: Theme.opacityOverlay
    text1: qsTr("Primary Color") + " " + qsTr("Text") + ", " + qsTr("Overlay Background Color")
    text2: qsTr("Secondary Color") + " " + qsTr("Text") + ", " + qsTr("Overlay Background Color")
    textcolor1: MyPalette.primaryColor
    textcolor2: MyPalette.secondaryColor
  }

//  ShowRoomTextBG {
//    bgcolor: MyPalette.highlightDimmerColor
//    bgopacity: Theme.opacityMedium
//    text1: qsTr("Highlight Color") + " " + qsTr("Text") + ", " + qsTr("Dim Highlight Color")
//    text2: qsTr("Secondary Highlight Color") + " " + qsTr("Text") + ", " + qsTr("Dim Highlight Color")
//    textcolor1: MyPalette.highlightColor
//    textcolor2: MyPalette.secondaryHighlightColor
//  }
//
//  ShowRoomTextBG {
//    bgcolor: MyPalette.highlightBackgroundColor
//    text1: qsTr("Highlight Color") + " " + qsTr("Text") + ", " + qsTr("Highlight Background Color")
//    text2: qsTr("Dim Highlight Color") + " " + qsTr("Text") + ", " + qsTr("Highlight Background Color")
//    textcolor1: MyPalette.highlightColor
//    textcolor2: MyPalette.highlightDimmerColor
//  }

  ShowRoomTextBG {
    bgcolor: MyPalette._coverOverlayColor
    text1: qsTr("Primary Color") + " " + qsTr("Text") + ", " + qsTr("Cover Background Color")
    text2: qsTr("Secondary Color") + " " + qsTr("Text") + ", " + qsTr("Cover Background Color")
    textcolor1: MyPalette.primaryColor
    textcolor2: MyPalette.secondaryColor
  }

  ShowRoomTextBG {
    bgcolor: MyPalette._wallpaperOverlayColor
    //bgopacity: Theme.opacityOverlay
    text1: qsTr("Primary Color") + " " + qsTr("Text") + ", " + qsTr("Wallpaper Overlay Color")
    text2: qsTr("Secondary Color") + " " + qsTr("Text") + ", " + qsTr("Wallpaper Overlay Color")
    textcolor1: MyPalette.primaryColor
    textcolor2: MyPalette.secondaryColor
  }

}

// vim: expandtab ts=4 softtabstop=4 filetype=javascript
