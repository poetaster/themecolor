import QtQuick 2.6
import Sailfish.Silica 1.0

SilicaItem {
  property color textcolor1
  property color textcolor2
  property alias bgcolor:   rect.color
  property alias bgopacity: rect.opacity
  property string text1
  property string text2

  anchors.horizontalCenter: parent.horizontalCenter
  width: parent.width
  height: col.height
  Rectangle {
    id: rect
    anchors.fill: parent
    radius: Theme.paddingSmall
  }
  Column {
    id: col
    width: parent.width
    spacing: Theme.paddingSmall
    Label {
      text: text1
      color: textcolor1
      anchors.horizontalCenter: parent.horizontalCenter
      horizontalAlignment: Text.AlignHCenter
    }
    Label {
      text: text2
      color: textcolor2
      anchors.horizontalCenter: parent.horizontalCenter
      horizontalAlignment: Text.AlignHCenter
      font.pixelSize: Theme.fontSizeSmall
    }
  }
}
// vim: expandtab ts=4 softtabstop=4 filetype=javascript
