import QtQuick 2.6
import Sailfish.Silica 1.0
import ".."

Column {
   id: uicol
   anchors.horizontalCenter: parent.horizontalCenter
   /* ************* Progress and Remorse Simulator *****************/
   SilicaItem {
       id: pbitem
       width: parent.width
       height: pb.height
       anchors.horizontalCenter: parent.horizontalCenter;
       readonly property int maxcounter: 20
       property int counter: maxcounter
       onVisibleChanged: { timer.running = visible ; counter = maxcounter; }
       Timer {
         id: timer
         interval: 500
         repeat: true
         running: ( parent.counter > 0 && parent.visible )
         triggeredOnStart: true
         onTriggered: { parent.counter -= 1 }
       }
       ProgressBar {
           id: pb
           width: parent.width
           value: parent.counter
           minimumValue: 0
           maximumValue: parent.maxcounter
           //indeterminate: ( value < parent.maxcounter /2 )
           label: qsTr("Progress Bar Demo")
           onValueChanged: {
             if ( value === 0 ) {
               remorse.execute(pb, qsTr("Remorse Item Demo") + "…", function() { parent.counter = -1 } )
             }
           }
           BackgroundItem {
             anchors.fill: parent
             onClicked: { if (parent.counter !== pb.minimumValue)  { counter = pb.minimumValue } else { counter = maxcounter } }
           }
       }
       RemorseItem { id: remorse; }
   }
   /* ************* TopMenu Simulator *****************/
   SilicaItem {
       width: brow.width
       height: tbrow.height + Theme.paddingLarge * 2
       anchors.horizontalCenter: parent.horizontalCenter;
       Rectangle {
         z: -1
         anchors.fill: parent
         anchors.centerIn: parent
         anchors.horizontalCenter: parent.horizontalCenter;
         color: MyPalette.overlayBackgroundColor
         opacity: Theme.opacityOverlay
       }
       Row {
         id: tbrow
         anchors.horizontalCenter: parent.horizontalCenter
         anchors.verticalCenter: parent.verticalCenter;
         //height: Theme.iconSizeMedium
         spacing: Theme.iconSizeMedium
         ColorIndicator3 { iconcolor: MyPalette.highlightColor;   icon: "image://theme/icon-m-wlan-2" }
         ColorIndicator3 { iconcolor: MyPalette.primaryColor; icon: "image://theme/icon-m-location" }
         ColorIndicator3 { iconcolor: MyPalette.highlightColor; icon: "image://theme/icon-m-bluetooth"; }
         ColorIndicator3 { iconcolor: MyPalette.primaryColor; icon: "image://theme/icon-m-ambience"; }
       }
   }
   /* ************* Pulley Menu Simulator *****************/
   SilicaItem {
      width: brow.width
      height: gicol.height + Theme.paddingLarge * 2
      anchors.horizontalCenter: parent.horizontalCenter;
      Rectangle {
        id: girec
        anchors.fill: gicol
        height: gicol.height
        color: MyPalette._wallpaperOverlayColor
        gradient: Gradient {
          GradientStop { position: 0.0; color: Theme.rgba(MyPalette.highlightBackgroundColor, MyPalette.highlightBackgroundOpacity + 0.1) }
          GradientStop { position: (gicol.height - gicol.spacing)/gicol.height; color: Theme.rgba(MyPalette.highlightBackgroundColor, Theme.opacityHigh) }
          GradientStop { position: 1.0; color: Theme.rgba(MyPalette.backgroundColor, 0.0) }
        }
      }
      Column {
        id: gicol
        spacing: 0
        width: parent.width
        MenuItem { text: qsTr("MenuItem");                                   color: MyPalette.highlightColor; height: Theme.itemSizeExtraSmall; highlighted: true}
        MenuItem { text: qsTr("MenuItem") + " " + qsTr("disabled"); height: Theme.itemSizeExtraSmall; enabled: false; }
        MenuItem { text: qsTr("MenuItem") + " " + qsTr("selected");          color: MyPalette.primaryColor;   height: Theme.itemSizeExtraSmall; down: false; highlighted: false
          HighlightBar {
          width: gicol.width
          highlightedItem: parent;
          anchors.horizontalCenter: parent.horizontalCenter;
          anchors.verticalCenter: parent.verticalCenter;
          color: Theme.rgba(MyPalette.highlightBackgroundColor, Theme.highlightBackgroundOpacity)
          }
        }
        MenuItem { text: " "; height: Theme.itemSizeTiny; enabled: false; }
      }
     /*
      // old pulley menu style glassy indicator simulator
      GlassItem {
        width: parent.width
        height: Theme.paddingLarge
        anchors.horizontalCenter: parent.horizontalCenter;
        anchors.verticalCenter: gicol.bottom
        anchors.verticalCenterOffset: -gicol.spacing
        radius: 0.35
        color: MyPalette.highlightBackgroundColor
        opacity: 1.0
        falloffRadius:  0.2
        brightness: 1.0
      }
      */
   }
   /* ************* Buttons *****************/
   Row {
     id: brow
     anchors.horizontalCenter: parent.horizontalCenter;
     spacing: Theme.paddingSmall
     Button { text: qsTr("Button");               width: Theme.buttonWidthSmall; color: MyPalette.primaryColor }
     Button { text: qsTr("Button"); down: false;  width: Theme.buttonWidthSmall; color: MyPalette.primaryColor }
   }
   Row {
     id: ibrow
     anchors.horizontalCenter: parent.horizontalCenter;
     ColorIndicator1 { enabled: false; iconcolor: MyPalette.primaryColor}
     ColorIndicator1 { checked: false; iconcolor: MyPalette.primaryColor }
     ColorIndicator1 { checked: true;  iconcolor: MyPalette.secondaryColor }
     ColorIndicator1 { checked: false; iconcolor: MyPalette.highlightColor }
     ColorIndicator1 { checked: true;  iconcolor: MyPalette.secondaryHighlightColor }
   }
   FakeVKB{ }
}

// vim: expandtab ts=4 softtabstop=4 filetype=javascript
