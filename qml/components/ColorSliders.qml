import QtQuick 2.6
import Sailfish.Silica 1.0
import "../components"
import "controls"

Column {
  id: col
  width: parent.width
  anchors.horizontalCenter: parent.horizontalCenter
    SectionHeader { text: qsTr("Primary Color"); color: Theme.primaryColor}
    ColorSlider {
      onOutcolorChanged: MyPalette.primaryColor = outcolor
      onVisibleChanged: { incolor = MyPalette.primaryColor }
    }
    SectionHeader { text: qsTr("Secondary Color"); color: Theme.primaryColor}
    ColorSlider {
      onOutcolorChanged: MyPalette.secondaryColor = outcolor
      onVisibleChanged: { incolor = MyPalette.secondaryColor }
    }
    SectionHeader { text: qsTr("Highlight Color"); color: Theme.primaryColor}
    ColorSlider {
      onOutcolorChanged: MyPalette.highlightColor = outcolor
      onVisibleChanged: { incolor = MyPalette.highlightColor }
    }
    SectionHeader { text: qsTr("Secondary Highlight Color") ; color: Theme.primaryColor}
    ColorSlider {
      onOutcolorChanged: MyPalette.secondaryHighlightColor = outcolor
      onVisibleChanged: { incolor = MyPalette.secondaryHighlightColor }
    }
    SectionHeader { text: qsTr("Background Highlight Color"); color: Theme.primaryColor}
    ColorSlider {
      onOutcolorChanged: MyPalette.highlightBackgroundColor = outcolor
      onVisibleChanged: { incolor = MyPalette.highlightBackgroundColor }
    }
    SectionHeader { text: qsTr("Dim Highlight Color"); color: Theme.primaryColor}
    ColorSlider {
      onOutcolorChanged: MyPalette.highlightDimmerColor = outcolor
      onVisibleChanged: { incolor = MyPalette.highlightDimmerColor }
    }
    SectionHeader { text: qsTr("Background Glow Color"); color: Theme.primaryColor}
    GlowSlider { }
}

// vim: expandtab ts=4 st=4 filetype=javascript
