import QtQuick 2.6
import Sailfish.Silica 1.0
import "functions.js" as Functions
import ".."

ButtonLayout {
    //width: parent.width
    anchors.horizontalCenter: parent.horizontalCenter
    preferredWidth: Theme.buttonWidthLarge

    Button {
      id: btn
      text:  qsTr("%1 Theme", "parameter is name for a theme, e.g. night, day, random... you might just use the parameter and not translate theme at all.").arg(qsTr("Solarize", "as in 'solarized' color scheme"))
      onClicked: {
        Functions.solarizeColors(MyPalette);
        generatorRemorse.execute(qsTr("Applying") + "…", function(){}, 2400 );
      }
    }
    Button {
      text:  qsTr("Generate") + " " + qsTr("from") + " " + qsTr("Highlight")
      onClicked: {
        Functions.computeColors(MyPalette);
        generatorRemorse.execute(qsTr("Applying") + "…", function(){}, 2400 );
      }
      ButtonLayout.newLine: true
    }
    Button {
      text:  qsTr("%1 Theme", "parameter is name for a theme, e.g. night, day, random... you might just use the parameter and not translate theme at all.").arg(qsTr("Night"))
      onClicked: {
        Functions.nightColors(MyPalette);
        generatorRemorse.execute(qsTr("Applying") + "…", function(){}, 2400 );
      }
    }
    Button {
      text:  qsTr("%1 Theme", "parameter is name for a theme, e.g. night, day, random... you might just use the parameter and not translate theme at all.").arg(qsTr("Summer"))
      onClicked: {
        Functions.dayColors(MyPalette);
        generatorRemorse.execute(qsTr("Applying") + "…", function(){}, 2400 );
      }
      ButtonLayout.newLine: true
    }
    Repeater {
      model: 4
      Button {
        property int idx: 0
        property int maxidx: 4
        preferredWidth: isLandscape ? Theme.buttonWidthSmall : Theme.buttonWidthTiny
        text:  qsTr("%1 Color Blindness", "%1 is replaced with the variant of color blindness, as in 'red-green color-blindness'").arg(qsTr("R/G", "abbreviation for red-green color blindness")) + " " + qsTr("#%1/%2","for lists, as in 'number one of four' is #1/4").arg(index+1).arg(maxidx);
        //width: Theme.buttonWidthSmall
        onClicked: {
          Functions.blindColors(MyPalette, index);
          //idx = idx+1;
          //if (idx >= maxidx) { idx = 0 };
          generatorRemorse.execute(qsTr("Applying") + "…", function(){}, 2400 );
          //console.debug("idx: " + idx);
        }
      }
      }
}

// vim: expandtab ts=4 st=4 filetype=javascript
