import QtQuick 2.6
import Sailfish.Silica 1.0
import "../components"
import "showroom"

SilicaItem {
    highlighted: false
    height: showcol.height + header.height
    width: parent.width
    anchors.horizontalCenter: parent.horizontalCenter

    states: [
      State {
        name: "text"
        changes: [
          PropertyChanges { target: header; text: qsTr("Text Elements") },
          PropertyChanges { target: showcol; showui: false }
          //PropertyChanges { target: bg; showbgRects: false }
        ]
      },
      State {
        name: "ui"
        changes: [
          PropertyChanges { target: header; text: qsTr("UI Elements") },
          PropertyChanges { target: showcol; showui: true }
          //PropertyChanges { target: bg; showbgRects: false }
        ]
      }
    ]

    SectionHeader {
      id: header
      anchors.horizontalCenter: parent.horizontalCenter
      width: parent.width
      height: implicitHeight
    }

   ShowRoomBG {
        id: bg
        anchors.top: showcol.top
        height: showcol.height
        width: showcol.width
        //fillMode: Image.PreserveAspectCrop
    }
    Column {
        id: showcol
        width: parent.width
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: header.bottom
        spacing: Theme.paddingMedium
        property bool showui: false
        ShowRoomText { visible: !showcol.showui }
        ShowRoomUI   { visible: showcol.showui }
    }
}

// vim: expandtab ts=4 softtabstop=4 filetype=javascript
