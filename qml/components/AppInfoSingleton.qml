pragma Singleton
import QtQuick 2.6

QtObject {
  Component.onCompleted: {
    // set by filejail from the .desktop file
    appname                = Qt.application.name;
    organame               = Qt.application.organization;
    Qt.application.domain  = orgadomain;
    Qt.application.version = versionstring;
  }
  // these are filled in by spec file:
  readonly property var version: 0
  readonly property var release: 0
  default property string versionstring: version + "-" + release
  property string orgadomain: "sailfish.nephros.org"
  // overridden by Qt.application.*, init to undefined
  property string organame: undefined
  property string appname: undefined
  //readonly property string organame: "."
  readonly property string displayName: qsTr("ThemeColor", "this will also be used for the app icon")
  readonly property string builderDisplayName: qsTr("ThemeColor® RPM Builder™", "this will also be used for the app icon")
  readonly property string copyright: "2021 Peter G. (nephros) <" + this.email + ">"
  readonly property string email: "sailfish@nephros.org"
  readonly property string license: "MIT License"
  readonly property string licenseurl: "https://opensource.org/licenses/MIT"
  readonly property string source: "https://gitlab.com/nephros/openrepos-themecolor/"
  readonly property string transurl: "https://hosted.weblate.org/git/theme-color/app"
  // TODO: further complicate things by keeping persons in another data structure
  readonly property ListModel translators: ListModel {
       ListElement { langid: "de";    langname: qsTr("German", "Language name in local language. So, if local language is French, 'German' should be 'allemande', not 'Deutsch'.");
            persons: 'Peter G. (nephros), J. Lavoie (Edanas)'
       }
       ListElement { langid: "en_IE";    langname: qsTr("English", "Language name in local language. So, if local language is French, 'German' should be 'allemande', not 'Deutsch'.");
            persons:  "Peter G. (nephros)"
       }
       ListElement { langid: "es"; langname: qsTr("Spanish", "Language name in local language. So, if local language is French, 'German' should be 'allemande', not 'Deutsch'.");
            persons: 'Carmen F. B. (carmenfdez),
Adolfo Jayme Barrientos (fitojb)'
       }
       ListElement { langid: "fr";    langname: qsTr("French", "Language name in local language. So, if local language is French, 'German' should be 'allemande', not 'Deutsch'.");
            persons:  "J. Lavoie (Edanas)"
       }
       ListElement { langid: "nb_NO"; langname: qsTr("Norwegian Bokmål", "Language name in local language. So, if local language is French, 'German' should be 'allemande', not 'Deutsch'.");
            persons:  'Allan Nordhøy (kingu)'
       }
       ListElement { langid: "sv"; langname: qsTr("Swedish", "Language name in local language. So, if local language is French, 'German' should be 'allemande', not 'Deutsch'.");
            persons: 'Åke Engelbrektson (eson), Luna Jernberg'
       }
       ListElement { langid: "zh_CN"; langname: qsTr("Simplified Chinese", "Language name in local language. So, if local language is French, 'German' should be 'allemande', not 'Deutsch'.");
            persons: 'Rui Kon (dashinfantry)'
       }
   }
}
// vim: expandtab ts=4 st=4 filetype=javascript
