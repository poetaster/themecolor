import QtQuick 2.6
import Sailfish.Silica 1.0
import "../components"

SilicaItem {
    id: msr
    highlighted: false
    height: msrcol.height + header.height
    width: parent.width
    anchors.horizontalCenter: parent.horizontalCenter
    property var rowHeight: Theme.paddingSmall * 2
    property alias title: header.text
    property Palette theme

    SectionHeader {
      id: header
      anchors.horizontalCenter: parent.horizontalCenter
      width: parent.width
      height: implicitHeight
      font.pixelSize: Theme.fontSizeTiny
      text: qsTr("Mini", "small showroom") + "-" + qsTr("Showroom");
    }

    Column {
        id: msrcol
        width: parent.width
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: header.bottom
        spacing: 1
        Row {
          anchors.horizontalCenter: parent.horizontalCenter
          width: parent.width
          spacing: 0
          Rectangle { height: msr.rowHeight; width: msrcol.width / 4 ;  color: theme.primaryColor }
          Rectangle { height: msr.rowHeight; width: msrcol.width / 4 ;  color: theme.secondaryColor }
          Rectangle { height: msr.rowHeight; width: msrcol.width / 4 ;  color: theme.highlightColor }
          Rectangle { height: msr.rowHeight; width: msrcol.width / 4 ;  color: theme.secondaryHighlightColor }
        }
        Row {
          anchors.horizontalCenter: parent.horizontalCenter
          width: parent.width
          spacing: 0
          Rectangle { height: msr.rowHeight; width: msrcol.width / 4 ;  color: theme.overlayBackgroundColor }
          Rectangle { height: msr.rowHeight; width: msrcol.width / 4 ;  color: theme.highlightBackgroundColor }
          Rectangle { height: msr.rowHeight; width: msrcol.width / 4 ;  color: theme.highlightDimmerColor }
          Rectangle { height: msr.rowHeight; width: msrcol.width / 4 ;  color: theme._coverOverlayColor }
        }
    }
}

// vim: expandtab ts=4 softtabstop=4 filetype=javascript
