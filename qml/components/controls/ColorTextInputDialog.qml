import QtQuick 2.6
import Sailfish.Silica 1.0

Dialog {
  id: dialog
  property string coltext
  canAccept: field.acceptableInput
  width: parent.width
  Column {
    width: parent.width
    DialogHeader { title: qsTr("Color input") }
    Row {
      anchors.horizontalCenter: parent.horizontalCenter
      ColorIndicator2 {
        anchors.verticalCenter: field.verticalCenter
        iconcolor: field.acceptableInput ? field.text : "transparent"
        opacity:  field.acceptableInput ? 1.0 : Theme.opacityLow
        Behavior on opacity { FadeAnimator {} }
      }
      ColorField {
        id: field
        width: Math.max(dialog.width - Theme.horizontalPageMargin * 4 ,  Theme.buttonWidthLarge)
        col: incolor;
        name: qsTr("Color input")
        text: outcolor
        EnterKey.enabled: acceptableInput
        EnterKey.onClicked: {
          focus = false;
          if (acceptableInput) dialog.accept();
        }
      }
    }
  }
  onDone: {
    if (result == DialogResult.Accepted) {
      var startsWith = /^#/;
      ( ! startsWith.test(field.text) ) ?  coltext = field.text.replace(/^/, "#") : coltext = field.text;
      console.debug("returned from dialog: " + field.text )
    }
  }
}
// vim: expandtab ts=4 st=4 filetype=javascript
