import QtQuick 2.6
import QtQml.Models 2.1
import Sailfish.Silica 1.0
import ".."

Column {
    width: parent.width
    anchors.horizontalCenter: parent.horizontalCenter
    spacing: 0

    GlowIndicator {
        id: dot
        height: Theme.iconSizeExtraLarge
        width: height
        anchors.horizontalCenter: parent.horizontalCenter
    }
    Slider {
        id: glowSlider
        width: parent.width
        //width: parent.width - dot.width
        //anchors.verticalCenter: dot.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
        minimumValue: 0.0
        maximumValue: 0.9999
        stepSize: 1/359
        value: MyPalette.highlightColor.hsvHueF ? MyPalette.highlightColor.hsvHueF : 0.5
        color: MyPalette.highlightColor.hsvHueF ? Theme.highlightFromColor(Qt.hsva(value, 1.0, 0.5, 0.0), MyPalette.colorScheme) : MyPalette.backgroundGlowColor 
        property real hue
        label: "Hue " + Math.round( value * 360 ) + "°"
        onValueChanged: {
          hue = value;
          glowSlider.enabled =  false;
          MyPalette.backgroundGlowColor = Theme.highlightFromColor(Qt.hsva(hue, 0.5, 0.5, 0.0), MyPalette.colorScheme);
          glowSlider.enabled =  true;
        }
    }
    Grid {
        anchors.horizontalCenter: parent.horizontalCenter
        columns: 4
        columnSpacing: Theme.paddingLarge
        rowSpacing: Theme.paddingLarge
        Item { height: Theme.iconSizeMedium; width: Theme.iconSizeMedium }
        GlassItem {
            height: Theme.iconSizeMedium
            width: Theme.iconSizeMedium
            radius: 0.22
            falloffRadius: 0.18
            backgroundColor: Theme.rgba("white", 1.0)
            BackgroundItem { anchors.fill: parent; anchors.centerIn: parent ; onClicked: {MyPalette.backgroundGlowColor = "white" }}
            Rectangle { anchors.fill: parent; anchors.centerIn: parent ; color: "transparent"; border.width: 1; border.color:  "gray" ; radius: 10 }
        }
        GlassItem {
            height: Theme.iconSizeMedium
            width: Theme.iconSizeMedium
            radius: 0.22
            falloffRadius: 0.18
            backgroundColor: Theme.rgba("black", 1.0)
            BackgroundItem { anchors.fill: parent; anchors.centerIn: parent ; onClicked: {MyPalette.backgroundGlowColor = "black" }}
            Rectangle { anchors.fill: parent; anchors.centerIn: parent ; color: "transparent"; border.width: 1; border.color:  "gray" ; radius: 10 }
        }
        Item { height: Theme.iconSizeMedium; width: Theme.iconSizeMedium }
        GlassItem {
            height: Theme.iconSizeMedium
            width: Theme.iconSizeMedium
            radius: 0.22
            falloffRadius: 0.18
            backgroundColor: MyPalette.primaryColor 
            BackgroundItem { anchors.fill: parent; anchors.centerIn: parent ; onClicked: {MyPalette.backgroundGlowColor = MyPalette.primaryColor  }}
            Rectangle { anchors.fill: parent; anchors.centerIn: parent ; color: "transparent"; border.width: 1; border.color:  "gray" ; radius: 10 }
        }
        GlassItem {
            height: Theme.iconSizeMedium
            width: Theme.iconSizeMedium
            radius: 0.22
            falloffRadius: 0.18
            backgroundColor: MyPalette.secondaryColor
            BackgroundItem { anchors.fill: parent; anchors.centerIn: parent ; onClicked: {MyPalette.backgroundGlowColor = MyPalette.secondaryColor  }}
            Rectangle { anchors.fill: parent; anchors.centerIn: parent ; color: "transparent"; border.width: 1; border.color:  "gray" ; radius: 10 }
        }
        GlassItem {
            height: Theme.iconSizeMedium
            width: Theme.iconSizeMedium
            radius: 0.22
            falloffRadius: 0.18
            backgroundColor: MyPalette.highlightColor
            BackgroundItem { anchors.fill: parent; anchors.centerIn: parent ; onClicked: {MyPalette.backgroundGlowColor = MyPalette.highlightColor  }}
            Rectangle { anchors.fill: parent; anchors.centerIn: parent ; color: "transparent"; border.width: 1; border.color:  "gray" ; radius: 10 }
        }
        GlassItem {
            height: Theme.iconSizeMedium
            width: Theme.iconSizeMedium
            radius: 0.22
            falloffRadius: 0.18
            backgroundColor: MyPalette.secondaryHighlightColor
            BackgroundItem { anchors.fill: parent; anchors.centerIn: parent ; onClicked: {MyPalette.backgroundGlowColor = MyPalette.secondaryHighlightColor  }}
            Rectangle { anchors.fill: parent; anchors.centerIn: parent ; color: "transparent"; border.width: 1; border.color:  "gray" ; radius: 10 }
        }
    }
}

// vim: expandtab ts=4 st=4 filetype=javascript
