import QtQuick 2.6
import Sailfish.Silica 1.0

Column {
    property color incolor
    property color outcolor
    onIncolorChanged: resetOutcolor();

    function resetOutcolor() {
        outcolor = incolor;
        resetSliders(incolor);
    }

    function resetSliders(col) {
        colr.value = col.r;
        colg.value = col.g;
        colb.value = col.b;
    }
    function shufflePickerColors() {
      var pcols = [
        "#e60003", "#e6007c", "#e700cc", "#9d00e7",
        "#7b00e6", "#5d00e5", "#0077e7", "#01a9e7",
        "#00cce7", "#00e696", "#00e600", "#99e600",
        "#e3e601", "#e5bc00", "#e78601"];

        for(var i = 0; i < pcols.length; i++) {
            pcols[i] = Qt.tint(pcols[i], Theme.rgba( incolor,0.5));
        }
        return pcols;
    }
    Component.onCompleted: resetOutcolor();

    Component {
      id: tiDialog
      ColorTextInputDialog {}
    }

    //width: page.isLandscape ? parent.width - Theme.itemSizeLarge * 2 : parent.width
    width: parent.width
    anchors.horizontalCenter: parent.horizontalCenter
    spacing: 0
    Row {
      anchors.horizontalCenter: parent.horizontalCenter
      width: btnrow.width
      spacing:0
      ColRect { width: parent.width / 2; height: Theme.fontSizeTiny; color: incolor }
      // keep this tied to slider values, so its interactive
      ColRect { id: colrect ; width: parent.width / 2; color: Qt.rgba( colr.value, colg.value, colb.value, incolor.a ) }
    }
    Row {
      id: btnrow
      width: parent.width
      anchors.horizontalCenter: parent.horizontalCenter
      spacing: 0
      ValueButton {
         id: valbutton
         width: parent.width * ( 2 / 4 )
         description: qsTr("Adjust sliders, tap to reset")
         label: ( incolor.a < 1 ) ? "(a)RGB" : "RGB"
         // keep this tied to slider values, so its interactive
         value: Qt.rgba( colr.value, colg.value, colb.value, incolor.a );
         valueColor: Theme.secondaryColor
         labelColor: Theme.secondaryColor
         onClicked: { resetOutcolor() }
         onValueChanged: { outcolor = value }
      }
      IconButton {
        id: tibutton
        anchors.verticalCenter: valbutton.verticalCenter
        height: valbutton.height
        width: parent.width * ( 1/ 4 )
        icon.source: "image://theme/icon-m-text-input?" + Theme.highlightColor
        onClicked: {
          var dialog = pageStack.push(tiDialog)
          dialog.accepted.connect(function() { outcolor = dialog.coltext })
        }
       }
      IconButton {
        id: pickbutton
        anchors.verticalCenter: valbutton.verticalCenter
        height: valbutton.height
        width: parent.width * ( 1 / 4 )
        icon.source: "image://theme/icon-m-wizard?" + Theme.highlightColor
        onClicked: {
          var dialog = pageStack.push("Sailfish.Silica.ColorPickerDialog", { colors: shufflePickerColors() })
          dialog.accepted.connect(function() { outcolor = dialog.color; resetSliders() })
        }
      }
    }
    Slider { id: colr
      width: parent.width
      transform: Scale { yScale: 0.8 }
      handleVisible: true
      highlighted: false
      anchors.horizontalCenter: parent.horizontalCenter
      minimumValue: 0; maximumValue: 1.0; stepSize: 1/256
      //value: incolor.r
      color: Qt.rgba( value, 0, 0, 1.0)
    }
    Slider { id: colg
      width: parent.width
      transform: Scale { yScale: 0.8 }
      handleVisible: true
      highlighted: false
      anchors.horizontalCenter: parent.horizontalCenter
      minimumValue: 0 ; maximumValue: 1.0; stepSize: 1/256
      //value: incolor.g
      color: Qt.rgba( 0, value, 0, 1.0)
    }
    Slider { id: colb
      width: parent.width
      transform: Scale { yScale: 0.8 }
      handleVisible: true
      highlighted: false
      anchors.horizontalCenter: parent.horizontalCenter
      minimumValue: 0 ; maximumValue: 1.0; stepSize: 1/256
      //value: incolor.b
      color: Qt.rgba( 0, 0, value, 1.0)
    }
}

// vim: expandtab ts=4 st=4 filetype=javascript
