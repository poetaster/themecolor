import QtQuick 2.6
import Sailfish.Silica 1.0
import "generators"

Column {
  id: col
  width: parent.width
  anchors.horizontalCenter: parent.horizontalCenter
  spacing: Theme.paddingSmall
  SectionHeader{ text: qsTr("Randomizer") }
  ColorRandomizer { }
  SectionHeader{ text: qsTr("Filters") }
  ColorFilters { }
  SectionHeader{ text: qsTr("Scheme Generators") }
  ColorSchemeGenerator { }
}

// vim: expandtab ts=4 st=4 filetype=javascript
