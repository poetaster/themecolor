<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="nb_NO">
<context>
    <name>AdvancedPage</name>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="17"/>
        <source>Advanced Options</source>
        <translation>Avanserte valg</translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="26"/>
        <source>System Functions</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="29"/>
        <location filename="../qml/pages/AdvancedPage.qml" line="35"/>
        <source>Restart %1</source>
        <comment>restart an application, as in &apos;restart service foo&apos;</comment>
        <translation type="unfinished">Start %1 på ny</translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="30"/>
        <location filename="../qml/pages/AdvancedPage.qml" line="57"/>
        <source>Lipstick</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="30"/>
        <source>Home Screen</source>
        <translation type="unfinished">Hjemmeskjerm</translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="30"/>
        <source>Restart the %1 service. This will close all apps and relaunch the %2</source>
        <comment>arguments are &apos;Lipstick&apos; and &apos;Home Screen&apos;, one id the application name, one is the commonly used name for the same.</comment>
        <translation>Start %1-tjenesten på ny. Dette vil lukke alle programmer og starte %2 på nytt.</translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="31"/>
        <location filename="../qml/pages/AdvancedPage.qml" line="37"/>
        <source>Restarting</source>
        <translation type="unfinished">Starter på ny</translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="36"/>
        <source>Restart the %1 service. This sometimes fixes color problems</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="36"/>
        <location filename="../qml/pages/AdvancedPage.qml" line="78"/>
        <source>Ambience</source>
        <translation>Omgivelse</translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="49"/>
        <source>Config Management</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="51"/>
        <source>Reset Config</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="52"/>
        <source>Reset all color-related configuration values</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="53"/>
        <location filename="../qml/pages/AdvancedPage.qml" line="58"/>
        <source>Resetting</source>
        <translation type="unfinished">Tilbakestiller</translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="56"/>
        <source>Reset Config and Restart</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="57"/>
        <source>Reset all color-related configuration values and restart %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="75"/>
        <source>Background Service</source>
        <translation>Bakgrunnstjeneste</translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="77"/>
        <source>Set up daemon</source>
        <translation>Sett opp daemon</translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="78"/>
        <source>Background service which watches for %1 changes and applies actions.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="81"/>
        <source>Import/Export</source>
        <translation>Importer/eksporter</translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="84"/>
        <source>Load Ambience File</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="85"/>
        <source>Load a color scheme from a .ambience file.</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>AppInfoSingleton</name>
    <message>
        <source>ThemeColor</source>
        <translation type="vanished">DraktFarge</translation>
    </message>
    <message>
        <source>ThemeColor® RPM Builder™</source>
        <translation type="vanished">ThemeColor® RPM-bygger™</translation>
    </message>
    <message>
        <location filename="../qml/components/AppInfoSingleton.qml" line="21"/>
        <source>ThemeColor</source>
        <comment>this will also be used for the app icon</comment>
        <translation>DraktFarge</translation>
    </message>
    <message>
        <location filename="../qml/components/AppInfoSingleton.qml" line="22"/>
        <source>ThemeColor® RPM Builder™</source>
        <comment>this will also be used for the app icon</comment>
        <translation>DraktFarge® RPM-bygger™</translation>
    </message>
    <message>
        <location filename="../qml/components/AppInfoSingleton.qml" line="34"/>
        <source>English</source>
        <comment>Language name in local language. So, if local language is French, &apos;German&apos; should be &apos;allemande&apos;, not &apos;Deutsch&apos;.</comment>
        <translation>Engelsk</translation>
    </message>
    <message>
        <location filename="../qml/components/AppInfoSingleton.qml" line="31"/>
        <source>German</source>
        <comment>Language name in local language. So, if local language is French, &apos;German&apos; should be &apos;allemande&apos;, not &apos;Deutsch&apos;.</comment>
        <translation>Tysk</translation>
    </message>
    <message>
        <location filename="../qml/components/AppInfoSingleton.qml" line="44"/>
        <source>Norwegian Bokmål</source>
        <comment>Language name in local language. So, if local language is French, &apos;German&apos; should be &apos;allemande&apos;, not &apos;Deutsch&apos;.</comment>
        <translation>Norwegian Bokmål</translation>
    </message>
    <message>
        <location filename="../qml/components/AppInfoSingleton.qml" line="37"/>
        <source>Spanish</source>
        <comment>Language name in local language. So, if local language is French, &apos;German&apos; should be &apos;allemande&apos;, not &apos;Deutsch&apos;.</comment>
        <translation>Spansk</translation>
    </message>
    <message>
        <location filename="../qml/components/AppInfoSingleton.qml" line="41"/>
        <source>French</source>
        <comment>Language name in local language. So, if local language is French, &apos;German&apos; should be &apos;allemande&apos;, not &apos;Deutsch&apos;.</comment>
        <translation>Fransk</translation>
    </message>
    <message>
        <location filename="../qml/components/AppInfoSingleton.qml" line="47"/>
        <source>Swedish</source>
        <comment>Language name in local language. So, if local language is French, &apos;German&apos; should be &apos;allemande&apos;, not &apos;Deutsch&apos;.</comment>
        <translation>Svensk</translation>
    </message>
    <message>
        <location filename="../qml/components/AppInfoSingleton.qml" line="50"/>
        <source>Simplified Chinese</source>
        <comment>Language name in local language. So, if local language is French, &apos;German&apos; should be &apos;allemande&apos;, not &apos;Deutsch&apos;.</comment>
        <translation>Forenklet kinesisk</translation>
    </message>
</context>
<context>
    <name>ColorField</name>
    <message>
        <source>specify RGB or aRGB value, e.g.</source>
        <translation type="vanished">angi en RGB- eller aRGB-verdi, f.eks.</translation>
    </message>
    <message>
        <location filename="../qml/components/controls/ColorField.qml" line="9"/>
        <source>an %1 value</source>
        <comment>parameter is either RGB or aRGB</comment>
        <translation>en %1-verdi</translation>
    </message>
    <message>
        <location filename="../qml/components/controls/ColorField.qml" line="10"/>
        <source>(the # is optional)</source>
        <translation>(# er valgfritt)</translation>
    </message>
    <message>
        <location filename="../qml/components/controls/ColorField.qml" line="10"/>
        <source>e.g.</source>
        <translation>g.eks.</translation>
    </message>
</context>
<context>
    <name>ColorFilters</name>
    <message>
        <source>Tint</source>
        <translation type="obsolete">Fargelegging</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorFilters.qml" line="17"/>
        <source>Red</source>
        <translation>Rød</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorFilters.qml" line="17"/>
        <source>Tint %1</source>
        <translation>Fargetone %1</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorFilters.qml" line="25"/>
        <source>Darken</source>
        <translation>Mørkne</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorFilters.qml" line="33"/>
        <source>Brighten</source>
        <translation>Lysne</translation>
    </message>
</context>
<context>
    <name>ColorGenerators</name>
    <message>
        <location filename="../qml/components/ColorGenerators.qml" line="10"/>
        <source>Randomizer</source>
        <translation>Tilfeldighetsgenerator</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorGenerators.qml" line="12"/>
        <source>Filters</source>
        <translation>Filtre</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorGenerators.qml" line="14"/>
        <source>Scheme Generators</source>
        <translation>Draktgeneratorer</translation>
    </message>
</context>
<context>
    <name>ColorRandomizer</name>
    <message>
        <location filename="../qml/components/generators/ColorRandomizer.qml" line="16"/>
        <source>Random</source>
        <translation>Tilfeldig</translation>
    </message>
    <message>
        <source>Colors</source>
        <translation type="vanished">Farger</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorRandomizer.qml" line="21"/>
        <location filename="../qml/components/generators/ColorRandomizer.qml" line="35"/>
        <location filename="../qml/components/generators/ColorRandomizer.qml" line="45"/>
        <source>Generated</source>
        <translation>Generert</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorRandomizer.qml" line="26"/>
        <source>Bright</source>
        <translation>Lyst</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorRandomizer.qml" line="26"/>
        <source>Dark</source>
        <translation>Mørk</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorRandomizer.qml" line="40"/>
        <source>Gray</source>
        <translation>Grå</translation>
    </message>
</context>
<context>
    <name>ColorSchemeGenerator</name>
    <message>
        <source>Solarize</source>
        <translation type="vanished">Solarisert</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="13"/>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="28"/>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="35"/>
        <source>%1 Theme</source>
        <comment>parameter is name for a theme, e.g. night, day, random... you might just use the parameter and not translate theme at all.</comment>
        <translation>%1 Theme</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="13"/>
        <source>Solarize</source>
        <comment>as in &apos;solarized&apos; color scheme</comment>
        <translation>Solarisert</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="20"/>
        <source>Generate</source>
        <translation>Generer</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="20"/>
        <source>from</source>
        <translation>fra</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="20"/>
        <source>Highlight</source>
        <translation>Framhev</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="28"/>
        <source>Night</source>
        <translation>Natt</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="48"/>
        <source>%1 Color Blindness</source>
        <comment>%1 is replaced with the variant of color blindness, as in &apos;red-green color-blindness&apos;</comment>
        <translation>%1-fargeblindhet</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="48"/>
        <source>R/G</source>
        <comment>abbreviation for red-green color blindness</comment>
        <translation>R/G</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="48"/>
        <source>#%1/%2</source>
        <comment>for lists, as in &apos;number one of four&apos; is #1/4</comment>
        <translation>#%1/%2</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation type="vanished">Drakt</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="35"/>
        <source>Summer</source>
        <translation>Sommer</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="16"/>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="23"/>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="31"/>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="38"/>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="54"/>
        <source>Applying</source>
        <translation>Legger til</translation>
    </message>
</context>
<context>
    <name>ColorSelectors</name>
    <message>
        <source>Color</source>
        <translation type="vanished">Colour</translation>
    </message>
    <message>
        <source>Primary Color</source>
        <translation type="vanished">Primary Colour</translation>
    </message>
    <message>
        <source>Secondary Color</source>
        <translation type="vanished">Secondary Colour</translation>
    </message>
    <message>
        <source>Highlight Color</source>
        <translation type="vanished">Highlight Colour</translation>
    </message>
    <message>
        <source>Secondary Highlight Color</source>
        <translation type="vanished">Secondary Highlight Colour</translation>
    </message>
</context>
<context>
    <name>ColorSlider</name>
    <message>
        <location filename="../qml/components/controls/ColorSlider.qml" line="58"/>
        <source>Adjust sliders, tap to reset</source>
        <translation>Juster glidebrytere, trykk for å tilbakestille</translation>
    </message>
</context>
<context>
    <name>ColorSliders</name>
    <message>
        <source>Color</source>
        <translation type="vanished">Colour</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSliders.qml" line="10"/>
        <source>Primary Color</source>
        <translation>Primærfarge</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSliders.qml" line="15"/>
        <source>Secondary Color</source>
        <translation>Sekundærfarge</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSliders.qml" line="20"/>
        <source>Highlight Color</source>
        <translation>Framhevingsfarge</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSliders.qml" line="25"/>
        <source>Secondary Highlight Color</source>
        <translation>Sekundær framhevingsfarge</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSliders.qml" line="30"/>
        <source>Background Highlight Color</source>
        <translation>Framhevelsesbakgrunnsfarge</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSliders.qml" line="35"/>
        <source>Dim Highlight Color</source>
        <translation>Diffus framhevelsesfarge</translation>
    </message>
    <message>
        <source>Highlight Background Color</source>
        <translation type="vanished">Framhevelsesbakgrunnsfarge</translation>
    </message>
    <message>
        <source>Highlight Dimmer Color</source>
        <translation type="vanished">Framhev fordunklingsfarge</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSliders.qml" line="40"/>
        <source>Background Glow Color</source>
        <translation>Bakgrunnsglødefarge</translation>
    </message>
</context>
<context>
    <name>ColorSwapper</name>
    <message>
        <location filename="../qml/components/ColorSwapper.qml" line="74"/>
        <source>Copy</source>
        <translation>Kopier</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSwapper.qml" line="74"/>
        <source>Swap</source>
        <translation>Veksle</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSwapper.qml" line="95"/>
        <location filename="../qml/components/ColorSwapper.qml" line="122"/>
        <source>Primary Color</source>
        <translation>Primærfarge</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSwapper.qml" line="96"/>
        <location filename="../qml/components/ColorSwapper.qml" line="123"/>
        <source>Secondary Color</source>
        <translation>Sekundærfarge</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSwapper.qml" line="97"/>
        <location filename="../qml/components/ColorSwapper.qml" line="124"/>
        <source>Highlight Color</source>
        <translation>Framhevelsesfarge</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSwapper.qml" line="98"/>
        <location filename="../qml/components/ColorSwapper.qml" line="125"/>
        <source>Secondary Highlight Color</source>
        <translation>Sekundær framhevelsesfarge</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSwapper.qml" line="99"/>
        <location filename="../qml/components/ColorSwapper.qml" line="126"/>
        <source>Background Highlight Color</source>
        <translation>Framhevelsesbakgrunnsfarge</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSwapper.qml" line="100"/>
        <location filename="../qml/components/ColorSwapper.qml" line="127"/>
        <source>Dim Highlight Color</source>
        <translation>Diffus framhevelsesfarge</translation>
    </message>
    <message>
        <source>Highlight Background Color</source>
        <translation type="vanished">Framhevelsesbakgrunnsfarge</translation>
    </message>
    <message>
        <source>Highlight Dimmer Color</source>
        <translation type="vanished">Framhev fordunklingsfarge</translation>
    </message>
    <message>
        <source>Color</source>
        <translation type="obsolete">Colour</translation>
    </message>
</context>
<context>
    <name>ColorTextInput</name>
    <message>
        <source>Color input</source>
        <translation type="vanished">Fargeinndata</translation>
    </message>
    <message>
        <source>Input value, tap to reset</source>
        <translation type="vanished">Inndataverdi, trykk for å tilbakestille</translation>
    </message>
</context>
<context>
    <name>ColorTextInputDialog</name>
    <message>
        <location filename="../qml/components/controls/ColorTextInputDialog.qml" line="11"/>
        <location filename="../qml/components/controls/ColorTextInputDialog.qml" line="24"/>
        <source>Color input</source>
        <translation>Fargeinndata</translation>
    </message>
</context>
<context>
    <name>ColorTextInputs</name>
    <message>
        <source>Primary Color</source>
        <translation type="vanished">Primærfarge</translation>
    </message>
    <message>
        <source>Secondary Color</source>
        <translation type="vanished">Sekundærfarge</translation>
    </message>
    <message>
        <source>Highlight Color</source>
        <translation type="vanished">Framhevelsesfarge</translation>
    </message>
    <message>
        <source>Secondary Highlight Color</source>
        <translation type="vanished">Sekundær framhevelsesfarge</translation>
    </message>
    <message>
        <source>Background Highlight Color</source>
        <translation type="vanished">Framhevelsesbakgrunnsfarge</translation>
    </message>
    <message>
        <source>Dim Highlight Color</source>
        <translation type="vanished">Diffus framhevelsesfarge</translation>
    </message>
    <message>
        <source>Highlight Background Color</source>
        <translation type="vanished">Framhevelsesbakgrunnsfarge</translation>
    </message>
    <message>
        <source>Highlight Dimmer Color</source>
        <translation type="vanished">Framhev fordunklingsfarge</translation>
    </message>
    <message>
        <source>Background Glow Color</source>
        <translation type="vanished">Bakgrunnsglødefarge</translation>
    </message>
</context>
<context>
    <name>CoverPage</name>
    <message>
        <source>ThemeColor</source>
        <translation type="vanished">DraktFarge</translation>
    </message>
</context>
<context>
    <name>DaemonSettings</name>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="105"/>
        <source>Daemon Settings</source>
        <translation>Nisse-innstillinger</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="115"/>
        <source>Enable Watcher daemon</source>
        <translation>Skru på oppsynsnisse</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="110"/>
        <source>Watches for %1 changes and applies actions defined below.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="116"/>
        <source>Daemon is </source>
        <translation>Nissen er </translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="116"/>
        <source>active</source>
        <translation>påslått</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="116"/>
        <source>inactive</source>
        <translation>inaktiv</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="134"/>
        <source>Actions</source>
        <translation>Handlinger</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="140"/>
        <source>Select what happens when an %1 change is detected</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="145"/>
        <source>launch %1</source>
        <translation>start %1</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="146"/>
        <source>just opens the app</source>
        <translation>åpner kun programmet</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="156"/>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="158"/>
        <source>Warning:</source>
        <translation>Advarsel:</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="157"/>
        <source>The features below are experimental and do not work reliably.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="165"/>
        <source>apply Top theme</source>
        <translation>ifør topp-drakt</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="166"/>
        <source>applies theme from the top %1 shelf</source>
        <translation>ifører drakt fra øverste %1-hylle</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="173"/>
        <source>apply Night theme</source>
        <translation>ifør nattdrakt</translation>
    </message>
    <message>
        <source>ThemeColor</source>
        <translation type="obsolete">DraktFarge</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="110"/>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="140"/>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="166"/>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="174"/>
        <source>Ambience</source>
        <translation>Omgivelse</translation>
    </message>
    <message>
        <source>The features below are a developer preview and do not work.</source>
        <translation type="vanished">Funksjonene nedenfor er en utviklerforhåndsvisning og vil ikke virke.</translation>
    </message>
    <message>
        <source>You can configure them here, but the daemon does not support them yet.</source>
        <translation type="vanished">Du kan sette dem opp her, men nissen støtter dem ikke enda.</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="174"/>
        <source>taken from the second %1 shelf</source>
        <translation>tatt fra andre %1-hylle</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="187"/>
        <source>Night begins:</source>
        <translation>Natt begynner:</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="201"/>
        <source>Night ends:</source>
        <translation>Natt slutter:</translation>
    </message>
</context>
<context>
    <name>FirstPage</name>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="29"/>
        <source>Adjust Theme Colors</source>
        <translation>Juster draktfarger</translation>
    </message>
    <message>
        <source>Current Color Model:</source>
        <translation type="vanished">Current Colour Model:</translation>
    </message>
    <message>
        <source>Primary Color</source>
        <translation type="vanished">Primary Colour</translation>
    </message>
    <message>
        <source>Secondary Color</source>
        <translation type="vanished">Secondary Colour</translation>
    </message>
    <message>
        <source>Highlight Color</source>
        <translation type="vanished">Highlight Colour</translation>
    </message>
    <message>
        <source>Secondary Highlight Color</source>
        <translation type="vanished">Secondary Highlight Colour</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="47"/>
        <source>Showroom</source>
        <translation>Visningsrom</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="98"/>
        <source>Laboratory</source>
        <translation>Laboratorium</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="112"/>
        <source>Input Mode:</source>
        <translation>Inndatamodus:</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="113"/>
        <source>Tap to switch</source>
        <translation>Trykk for å bytte</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="176"/>
        <source>Advanced…</source>
        <translation>Avansert…</translation>
    </message>
    <message>
        <source>gray</source>
        <translation type="vanished">grey</translation>
    </message>
    <message>
        <source>Apply colors to system</source>
        <translation type="vanished">Apply colours to system</translation>
    </message>
    <message>
        <source>Reload colors from system</source>
        <translation type="vanished">Reload colors from system</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="122"/>
        <source>Swapper/Copier</source>
        <translation>Veksler/kopierer</translation>
    </message>
    <message>
        <source>Compute all Colors from Highlight</source>
        <translation type="vanished">Regn ut alle farger fra framheving</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="183"/>
        <source>Applying</source>
        <translation>Legger til</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="182"/>
        <source>Apply Colors to System</source>
        <translation>Bruk farger for systemet</translation>
    </message>
    <message>
        <source>Set up daemon</source>
        <translation type="vanished">Sett opp nisse</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="185"/>
        <source>Reload Colors from System</source>
        <translation>Reload Colours from System</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="123"/>
        <source>Generators</source>
        <translation>Generatorer</translation>
    </message>
    <message>
        <source>Reload Colors from current Theme</source>
        <translation type="vanished">Gjeninnlast farger fra nåværende drakt</translation>
    </message>
    <message>
        <source>Reload Colors from System Config</source>
        <translation type="vanished">Gjeninnlast farger fra systemoppsett</translation>
    </message>
    <message>
        <source>Experimental or dangerous actions</source>
        <translation type="vanished">Eksperimentelle eller farlige handlinger</translation>
    </message>
    <message>
        <source>Export to Ambience file</source>
        <translation type="vanished">Eksporter til omgivelsesfil</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="179"/>
        <source>Export to Ambience package</source>
        <translation>Exporter til Ambience-pakke</translation>
    </message>
    <message>
        <source>Edit Transparency</source>
        <translation type="vanished">Rediger gjennomsiktighet</translation>
    </message>
    <message>
        <source>(not implemented)</source>
        <translation type="vanished">(ikke implementert)</translation>
    </message>
    <message>
        <source>Save Theme to current Ambience</source>
        <translation type="vanished">Lagre drakt til nåværende omgivelse</translation>
    </message>
    <message>
        <source>Saving</source>
        <translation type="vanished">Lagrer</translation>
    </message>
    <message>
        <source>Resetting</source>
        <translation type="vanished">Tilbakestiller</translation>
    </message>
    <message>
        <source>Restart %1</source>
        <comment>restart an application, as in &apos;restart service foo&apos;</comment>
        <translation type="vanished">Start %1 på ny</translation>
    </message>
    <message>
        <source>Restarting</source>
        <translation type="vanished">Starter på ny</translation>
    </message>
    <message>
        <source>Saving...</source>
        <translation type="vanished">Saving…</translation>
    </message>
    <message>
        <source>Reset all values and restart</source>
        <translation type="vanished">Tilbakestill alle verdier og start på ny</translation>
    </message>
    <message>
        <source>Reset nonstandard values</source>
        <translation type="vanished">Tilbakestill alle ikke-standardverdier</translation>
    </message>
    <message>
        <source>Restart Lipstick</source>
        <translation type="vanished">Start Lipstick på ny</translation>
    </message>
    <message>
        <source>Help</source>
        <translation type="vanished">Hjelp</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="121"/>
        <source>Sliders</source>
        <translation>Glidebrytere</translation>
    </message>
    <message>
        <source>Text</source>
        <translation type="vanished">Tekst</translation>
    </message>
    <message>
        <source>Swapper</source>
        <translation type="vanished">Swapper</translation>
    </message>
    <message>
        <source>Randomizer</source>
        <translation type="vanished">Tilfeldighetsgenerator</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="124"/>
        <source>Jolla Original</source>
        <translation>Opprinnelig Jolla</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="173"/>
        <source>Handbook</source>
        <comment>User Manual, Application Manual, Handbook, Howto</comment>
        <translation>Håndbok</translation>
    </message>
    <message>
        <source>Open Ambience Settings</source>
        <translation type="vanished">Åpne omgivelsesinnstillinger</translation>
    </message>
    <message>
        <source>Apply Colors to current Theme</source>
        <translation type="vanished">Apply colours to current Theme</translation>
    </message>
    <message>
        <source>Reload Colors</source>
        <translation type="vanished">Reload Colours</translation>
    </message>
    <message>
        <source>Color</source>
        <translation type="vanished">Colour</translation>
    </message>
    <message>
        <source>Reset Colors</source>
        <translation type="vanished">Reset Colours</translation>
    </message>
</context>
<context>
    <name>HelpColors</name>
    <message>
        <source>The four basic Colors</source>
        <translation type="vanished">De fire grunnleggende fargene</translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpColors.qml" line="6"/>
        <source>The four basic Colors</source>
        <comment>Help Section</comment>
        <translation>De fire grunnleggende fargene</translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpColors.qml" line="8"/>
        <source>&lt;p&gt;Jolla Ambiences define four colors which are used for the various text elements around the UI.:&lt;br /&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpColors.qml" line="10"/>
        <source>primary, for most interactive elements (it is black or white depending on Ambience Scheme)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpColors.qml" line="11"/>
        <source>secondary, per default derived from primary by making it a little less opaque, is used for inactive elements, and most text</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpColors.qml" line="12"/>
        <source>highlight, which is what you selected in the Ambience Settings, colors active or activated elements</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpColors.qml" line="13"/>
        <source>secondary highlight, derived through some magic from the highlight color.</source>
        <translation>sekundær framheving, utledet via litt magi fra framhevingsfargen.</translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpColors.qml" line="16"/>
        <source>Additional Colors</source>
        <comment>Help Section</comment>
        <translation>Ekstra farger</translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpColors.qml" line="19"/>
        <source>
&lt;p&gt;
Other colors used:&lt;br /&gt;
&lt;ul&gt;
&lt;li&gt;Background and HighlightBackground style the background of Menus, Buttons and the like.&lt;/li&gt;
&lt;li&gt;DimmerHighlightBackground does the same, but for darker areas or high contrast elements.&lt;/li&gt;
&lt;li&gt;GlowColor is used for glassy things like Switches, forward/back indicators etc.&lt;/li&gt;
&lt;li&gt;CoverOverlayColor is/was used for Application Covers, but see note below.&lt;/li&gt;
&lt;/ul&gt;
&lt;/p&gt;
&lt;p&gt;
For more information about colors and other aspects of the GUI, see the official &lt;a href=&quot;https://sailfishos.org/develop/docs/silica/qml-sailfishsilica-sailfish-silica-theme.html&quot;&gt;Theme Documentation&lt;/a&gt;
&lt;/p&gt;
&lt;p&gt;
&lt;i&gt;
It must be said that while all those &lt;b&gt;Theme&lt;/b&gt; colors are still defined, as SailfishOS progresses, with UI developers continue to use QML&apos;s&lt;b&gt;palette&lt;/b&gt; property directly.
As we only deal with Theme colors, this unfortunately means more and more UI components stop reacting to those changes.
&lt;/i&gt;
&lt;/p&gt;
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Additional Colors</source>
        <translation type="vanished">Ekstra farger</translation>
    </message>
</context>
<context>
    <name>HelpCredits</name>
    <message>
        <source>About</source>
        <translation type="vanished">Om</translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpCredits.qml" line="6"/>
        <source>About</source>
        <comment>Help Section</comment>
        <translation>Om</translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpCredits.qml" line="7"/>
        <source>Version:</source>
        <translation>Versjon:</translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpCredits.qml" line="8"/>
        <source>Copyright:</source>
        <translation>Opphavsrett:</translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpCredits.qml" line="9"/>
        <source>License:</source>
        <translation>Lisens:</translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpCredits.qml" line="10"/>
        <source>Source Code:</source>
        <translation>Kildekode:</translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpCredits.qml" line="11"/>
        <source>Translations</source>
        <translation>Oversettelser</translation>
    </message>
</context>
<context>
    <name>HelpDaemon</name>
    <message>
        <location filename="../qml/pages/help/HelpDaemon.qml" line="7"/>
        <source>The Watcher Daemon</source>
        <comment>Help Section</comment>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpDaemon.qml" line="9"/>
        <source>Ambience</source>
        <translation>Omgivelse</translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpDaemon.qml" line="9"/>
        <source>&lt;p&gt;The Watcher Daemon is a background service which can take care of some tasks while %1 is not running. Its main purpose is to apply color schemes after the %2 has changed.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Oppsynsnissen er en bakgrunnstjeneste som håndterer noen oppgaver mens %1 ikke kjører. Hovedformålet er å ta i bruk fargedrakter etter at %2 har endret seg.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpDaemon.qml" line="10"/>
        <source>&lt;p&gt;It can be configured by tapping the &lt;i&gt;Daemon Settings&lt;/i&gt; button on the Advanced page.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Kan settes opp ved å trykke &lt;i&gt;Nisse-innstillinger&lt;/i&gt;-knappen på «Avansert»-siden.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpDaemon.qml" line="11"/>
        <source>&lt;p&gt;This is an ever-evolving (and experimental) feature with more functions added each release.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Dette er en eksperimentell funksjon i konstant utvikling der flere funksjoner legges til ved hver utgivelse.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpDaemon.qml" line="12"/>
        <source>&lt;p&gt;One upcoming feature is to apply &amp;quot;Night Mode&amp;quot; color schemes at the appropriate time.&lt;/p&gt;</source>
        <translation>&lt;p&gt;En kommende funksjon er at &amp;quot;Nattmodus&amp;quot; iføres til rett tid.&lt;/p&gt;</translation>
    </message>
</context>
<context>
    <name>HelpExport</name>
    <message>
        <source>Exporting Ambiences</source>
        <translation type="vanished">Eksportere atmosfærer</translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpExport.qml" line="7"/>
        <source>Exporting Ambiences</source>
        <comment>Help Section</comment>
        <translation>Eksportere atmosfærer</translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpExport.qml" line="10"/>
        <source>&lt;p&gt;To export an Ambience, after having edited the colors, select the &quot;%1&quot; menu entry to bring up the Export page.&lt;/p&gt;</source>
        <extracomment>argument is the name of the menu entry &apos;Export to Ambience Package&apos;</extracomment>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpExport.qml" line="11"/>
        <source>Export to Ambience package</source>
        <translation>Exporter til Ambience-pakke</translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpExport.qml" line="22"/>
        <source>&lt;p&gt;Enter a name for your Ambience there and click the Ambience Name box to save.
This will do two things: write a .ambience file to your Documents folder, and save some settings for the %1 companion tool (see below).&lt;br /&gt;
If you like you can now edit the .ambience file using a text editor, e.g. for adding ringtone and sound information.
&lt;/p&gt;</source>
        <extracomment>the argument is also a translated string, the name of the builder tool</extracomment>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpExport.qml" line="28"/>
        <source>The Builder</source>
        <comment>Help Section</comment>
        <translation>Byggmesteren</translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpExport.qml" line="31"/>
        <source>&lt;p&gt;
When ready, launch the %1.
The Builder picks up the information from dconf and the .ambience file to create an RPM file, and prompt to install it if successful.
The package will also be saved in the Documents folder so you can share it with your friends.
&lt;/p&gt;</source>
        <extracomment>the argument is also a translated string, the name of the builder tool</extracomment>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpExport.qml" line="39"/>
        <source>Notes</source>
        <comment>Help Section</comment>
        <extracomment>as in something to add, not a small piece of paper</extracomment>
        <translation>Merknader</translation>
    </message>
    <message>
        <source>The Builder</source>
        <translation type="obsolete">Byggmesteren</translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpExport.qml" line="41"/>
        <source>&lt;ul&gt;
&lt;li&gt;The Builder is non-graphical and non-interactive. It needs to be configured through the app as described or it will not work.&lt;/li&gt;
&lt;li&gt;Installing an Ambience will switch it from Anonymous to a named Ambience. This means it will get a new Ambience shelf, which will be empty. Therefore it is a good idea to save the Scheme to the General Shelf.&lt;/li&gt;
&lt;li&gt;While both the .ambience file and the RPM package include most color values, not all are picked up by lipstick when switching Ambiences.  So, after having switched, open the App again and use the Ambience Shelf to load and apply your favourite scheme.&lt;/li&gt;
&lt;/ul&gt;
</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>HelpGeneral</name>
    <message>
        <source>General</source>
        <translation type="vanished">Generelt</translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpGeneral.qml" line="6"/>
        <source>General</source>
        <comment>Help Section</comment>
        <translation>Generelt</translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpGeneral.qml" line="8"/>
        <source>&lt;p&gt;
This application allows you to modify the current color scheme of Lipstick (a.k.a. Home Screen).
&lt;/p&gt;
&lt;p&gt;
It can not (yet) however, change saved Ambiences directly. Applied changes will not survive an Ambience change, Lipstick restart, or device reboot.&lt;br/&gt;
However, using the Shelves feature you can restore schemes you previously saved and re-apply them afterwards.
Also, by exporting changes to an installable package, changes can be made permanent.
&lt;/p&gt;
&lt;p&gt;
Currently only some colors can be edited. There are others in use by the system which are usually autocomputed from the basic four and can not be modified.&lt;br/&gt;
&lt;/p&gt;
&lt;p&gt;
We are working on overcoming some of these limitations, but some things would require patching the system UI elements which is beyond the scope of this app.
&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpGeneral.qml" line="24"/>
        <source>Basic Workflow</source>
        <comment>Help Section</comment>
        <translation>Grunnleggende arbeidsflyt</translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpGeneral.qml" line="26"/>
        <source>
&lt;p&gt;The basic workflow for re-coloring an existing Ambience or creating a new one would be something like this:&lt;/p&gt;

&lt;ol&gt;
&lt;li&gt;switch to an Ambience of your liking using the usual methods&lt;/li&gt;
&lt;li&gt;launch ThemeColor&lt;/li&gt;
&lt;li&gt;using the Sliders editing mode, adjust the four basic colors (primary, secondary, highlight and secondary highlight) to your liking.&lt;/li&gt;
&lt;li&gt;use the PullDown menu &lt;i&gt;Apply Colors to System&lt;/i&gt; to apply to the current theme&lt;/li&gt;
&lt;li&gt;explore the Sailfish UI a bit to make sure all looks right. Always check the VKB,  that can easily become unreadable&lt;/li&gt;
&lt;li&gt;go back to ThemeColor, and use the Shelves to store your colors for later reuse from within the app.&lt;/li&gt;
&lt;li&gt;adjust if necessary and repeat the last two steps above until everything looks pretty&lt;/li&gt;
&lt;li&gt;(optional:) to export and create an ambience RPM, and optionally install it, use the menu item &lt;i&gt;Export Ambience&lt;/i&gt;&lt;/li&gt;
&lt;/ol&gt;

&lt;p&gt; … and done! Your new Ambience should now show up in the usual place in Settings to select.&lt;/p&gt;

&lt;p&gt;If it does not (sometimes it happens) that means that ambienced has crashed.
In that case you can try to use the two &lt;i&gt;Restart&lt;/i&gt; functions on the Advanced page
to restart either Lipstick (the Home Screen), or just ambienced (the Theme engine)).
&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Basic Workflow</source>
        <translation type="vanished">Grunnleggende arbeidsflyt</translation>
    </message>
</context>
<context>
    <name>HelpHow</name>
    <message>
        <source>Loading and Applying Colors</source>
        <translation type="vanished">Laste inn og bruke farger</translation>
    </message>
    <message>
        <source>App Configuration and State</source>
        <translation type="vanished">Appkonfigurasjon og tilstand</translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpHow.qml" line="6"/>
        <source>Loading and Applying Colors</source>
        <comment>Help Section</comment>
        <translation>Laste inn og bruke farger</translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpHow.qml" line="8"/>
        <source>
&lt;p&gt;
        Ambience Colors are stored in several places: the .ambience file for installed Ambiences, a user-local database for user-created Ambiences, and &lt;i&gt;dconf&lt;/i&gt;.
        When an Ambience is loaded, its color values are written to a DConf location, and lipstick reads the values from there and uses it for its Theme.&lt;br /&gt;
&lt;/p&gt;
&lt;p&gt;
        We use that last part in this application, in that when we &quot;Apply&quot; colors to the system, we simply write to that DConf location, and lipstick notices and adjusts it theme.
        The good thing is, while official Ambiences only define four colors (two of which are always the same in practice), writing additional values to that location works, if they are defined as changable by the Sailfish Theme.
&lt;/p&gt;
&lt;p&gt;
        The dconf path in question is &lt;pre&gt;/desktop/jolla/theme/color&lt;/pre&gt; and can be read from command line using &lt;pre&gt;dconf dump&lt;/pre&gt;
&lt;/p&gt;
</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpHow.qml" line="22"/>
        <source>App Configuration and State</source>
        <comment>Help Section</comment>
        <translation>Appkonfigurasjon og tilstand</translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpHow.qml" line="24"/>
        <source>
&lt;p&gt;
        Speaking of dconf, the application uses it in its own location to store things like Shelves and information for the builder.&lt;br /&gt;
        Unfortuately, if the user creates and deletes Ambiences a lot, and uses the Ambience Shelf, there might be a lot of stale setting in there which may warrant cleaning out from time to time.
&lt;/p&gt;
&lt;p&gt;
        The location for this is &lt;pre&gt;/org/nephros/openrepos-themecolor/storage&lt;/pre&gt;
&lt;/p&gt;
</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpHow.qml" line="34"/>
        <source>The Builder</source>
        <comment>Help Section</comment>
        <translation>Byggmesteren</translation>
    </message>
    <message>
        <source>The Builder</source>
        <translation type="vanished">Byggmesteren</translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpHow.qml" line="36"/>
        <source>
&lt;p&gt;
        The Ambience builder is a shell script launched from a oneshot systemd service with the following mode of operation:
&lt;/p&gt;
&lt;p&gt;
        First it reads DConf values prepared by the App to figure out things like Ambience name and location of files.&lt;br /&gt;
        It then puts them in a temporary directory together with a .spec file. It calls the &lt;pre&gt;rpmbuild&lt;/pre&gt; command which produces (hopfully) a package.&lt;br /&gt;
        If successful, that package is opened, prompting the user to install it.
&lt;/p&gt;
</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>HelpPage</name>
    <message>
        <source>Use the Sliders in the lower section to adjust the colors.&lt;br /&gt;            When satisfied, tap the area above the slider to set the color.            Check what your theme will look like in the Showroom display.&lt;br /&gt;            &lt;br /&gt;            When you&apos;re done, use the PullUp Manu or the PushUp Menu to apply your creation.</source>
        <translation type="vanished">Use the Sliders in the lower section to adjust the colours.&lt;br /&gt;            When satisfied, tap the area above the slider to set the colour.            Check what your theme will look like in the Showroom display.&lt;br /&gt;            &lt;br /&gt;            When you&apos;re done, use the PullUp Manu or the PushUp Menu to apply your creation.</translation>
    </message>
    <message>
        <source>How to Use</source>
        <translation type="vanished">Anvendelse</translation>
    </message>
    <message>
        <source>General</source>
        <translation type="vanished">Generelt</translation>
    </message>
    <message>
        <source>This application allows you to modify the current color scheme of Lipstick. It will not (yet) however, change or create new ambiences,           nor will your changes survive an Ambience change, Lipstick restart, or device reboot.&lt;br /&gt;
          Currently, only four colors can be edited: primaryColor, secondaryColor, highlightColor, and secondaryHighlightColor, as these are the ones saved by the system.          There are other colors in use by the system, which are autocomputed from these basic four, and can not be modified at the moment.&lt;br/&gt;&lt;br/&gt;
          We are working on overcoming some of these limitations.</source>
        <translation type="vanished">This application allows you to modify the current colour scheme of Lipstick. It will not (yet) however, change or create new ambiences,           nor will your changes survive an Ambience change, Lipstick restart, or device reboot.&lt;br /&gt;
          Currently, only four colours can be edited: primaryColor, secondaryColor, highlightColor, and secondaryHighlightColor (sic!), as these are the ones saved by the system.          There are other colours in use by the system, which are autocomputed from these basic four, and can not be modified at the moment.&lt;br/&gt;&lt;br/&gt;
          We are working on overcoming some of these limitations.</translation>
    </message>
    <message>
        <source>This application allows you to modify the current color scheme of Lipstick. It will not (yet) however, change or create new ambiences,           nor will your changes survive an Ambience change, Lipstick restart, or device reboot.&lt;br /&gt;
          Currently, only some colors can be edited.          There are other colors in use by the system, which are autocomputed from the basic four, and can not be modified at the moment.&lt;br/&gt;&lt;br/&gt;
          We are working on overcoming some of these limitations.</source>
        <translation type="vanished">This application allows you to modify the current colour scheme of Lipstick. It will not (yet) however, change or create new ambiences, nor will your changes survive an Ambience change, Lipstick restart, or device reboot.&lt;br /&gt;
Currently, only some colours can be edited. There are other colours in use by the system, which are autocomputed from the basic four, and can not be modified at the moment.&lt;br/&gt;&lt;br/&gt;
We are working on overcoming some of these limitations.</translation>
    </message>
    <message>
        <source>The Showroom</source>
        <translation type="vanished">Visningsrommet</translation>
    </message>
    <message>
        <source>The Top area on the first page (&quot;Showroom&quot;) is non-interactive          and just shows the colors that are selected currently.&lt;br /&gt;          Here you can preview your creation.          </source>
        <translation type="vanished">The Top area on the first page (&quot;Showroom&quot;) is non-interactive          and just shows the colours that are selected currently.&lt;br /&gt;          Here you can preview your creation.          </translation>
    </message>
    <message>
        <source>The Laboratory</source>
        <translation type="vanished">Laboratoriet</translation>
    </message>
    <message>
        <source>In Slider Input Mode, use the Sliders in the lower section to adjust the colors.&lt;br /&gt;            In Text Input Mode, you can enter color values directly.&lt;br /&gt;
            Check what your theme will look like in the Showroom display.&lt;br /&gt;            &lt;br /&gt;            When you&apos;re done, use the PullUp Menu or the PushUp Menu to apply your creation to the Session.
          </source>
        <translation type="vanished">In Slider Input Mode, use the Sliders in the lower section to adjust the colours.&lt;br /&gt;            In Text Input Mode, you can enter colour values directly.&lt;br /&gt;
            Check what your theme will look like in the Showroom display.&lt;br /&gt;            &lt;br /&gt;            When you&apos;re done, use the PullUp Menu or the PushUp Menu to apply your creation to the Session.
          </translation>
    </message>
    <message>
        <source>In Slider input mode, use the Sliders in the lower section to adjust the colors.            In Text input mode, you can enter color values directly. Randomizer does what it sais, and Jolla Original you already know.&lt;br /&gt;             Check what your theme will look like in the Showroom display.&lt;br /&gt;            &lt;br /&gt;            When you&apos;re done, use the PullDown Menu.
          </source>
        <translation type="vanished">In Slider input mode, use the Sliders in the lower section to adjust the colours. In Text input mode, you can enter colour values directly. Randomizer does what it sais, and Jolla Original you already know.&lt;br /&gt;Check what your theme will look like in the Showroom display.&lt;br /&gt; &lt;br /&gt; When you&apos;re done, use the PullDown Menu.
          </translation>
    </message>
    <message>
        <source>The Cupboard</source>
        <translation type="vanished">Skapet</translation>
    </message>
    <message>
        <source>This area allows you to store your created palettes for re-use later.             There is one global Cupboard, and one specific for the current ambience.&lt;br /&gt;             Note that only system-wide Ambiences have a name, custon ones will show as anonymous (for now)</source>
        <translation type="vanished">This area allows you to store your created palettes for re-use later. There is one global Cupboard, and one specific for the current Ambience.&lt;br /&gt; Note that only system-wide Ambiences have a name, custom ones will show as anonymous (for now)</translation>
    </message>
    <message>
        <source>Tips and Caveats</source>
        <translation type="vanished">Tips og triks</translation>
    </message>
    <message>
        <source>It is possible to create color schemes which make parts of the UI unreadable. Check especially non-obvious areas such as the virtual Keyboard.&lt;br /&gt;
          It&apos;s a good idea to store a known-good color sceme in the Cupboard so you can go restore easily.&lt;br /&gt;          &lt;br /&gt;          If you have messed up the colours completely, use the PullUp menu option to reset everything, or from command line issue:&lt;br /&gt;          &lt;pre&gt;dconf dump /desktop/jolla/theme/color/&lt;/pre&gt;          &lt;pre&gt;dconf reset /desktop/jolla/theme/color/highlightBackground&lt;/pre&gt;          and repeat for all the other colors stored there. &lt;br /&gt;          Changing the Ambience from the System Settings may also help.</source>
        <translation type="vanished">It is possible to create colour schemes which make parts of the UI unreadable. Check especially non-obvious areas such as the virtual Keyboard.&lt;br /&gt;
It&apos;s a good idea to store a known-good colour scheme in the Cupboard so you can go restore easily.&lt;br /&gt;&lt;br /&gt;If you have messed up the colours completely, use the PullUp menu option to reset everything, or from command line issue:&lt;br /&gt;&lt;pre&gt;dconf dump /desktop/jolla/theme/color/&lt;/pre&gt;&lt;pre&gt;dconf reset /desktop/jolla/theme/color/highlightBackground&lt;/pre&gt;and repeat for all the other colours stored there. &lt;br /&gt;Changing the Ambience from the System Settings may also help.</translation>
    </message>
    <message>
        <source>This application allows you to modify the current color scheme of Lipstick. It will not (yet) however, change or create new Ambiences, 
nor will your changes survive an Ambience change, Lipstick restart, or device reboot.&lt;br /&gt;
Currently, only some colors can be edited.
There are other colors in use by the system, which are autocomputed from the basic four, and can not be modified.&lt;br/&gt;&lt;br/&gt;
We are working on overcoming some of these limitations.</source>
        <translation type="vanished">This application allows you to modify the current colour scheme of Lipstick. It will not (yet) however, change or create new Ambiences, nor will your changes survive an Ambience change, Lipstick restart, or device reboot.&lt;br /&gt;
Currently, only some colours can be edited.
There are other colours in use by the system, which are autocomputed from the basic four, and can not be modified.&lt;br/&gt;&lt;br/&gt;
We are working on overcoming some of these limitations.</translation>
    </message>
    <message>
        <source>The top area on the first page (&quot;Showroom&quot;) is non-interactive and just shows the colors that are selected currently.&lt;br /&gt;Here you can preview your creation.</source>
        <translation type="vanished">Toppområdet på første side («Visningsrommet») er ikke interaktivt, og viser kun fargene som er valgt.&lt;br /&gt;Her kan du bivåne din kreasjon.</translation>
    </message>
    <message>
        <source>In Slider input mode, use the Sliders in the lower section to adjust the colors.  In Text input mode, you can enter color values directly. Randomizer
does what it sais, and Jolla Original you already know.  Swapper lets you change color definitions.&lt;br /&gt; Check what your theme will look like in the
Showroom display.&lt;br /&gt;
&lt;br /&gt;
When you&apos;re done, use the PullDown Menu to apply the colors to the curent session.</source>
        <translation type="vanished">In Slider input mode, use the Sliders in the lower section to adjust the colours. In Text input mode, you can enter colour values directly. Randomizer
does what it sais, and Jolla Original you already know. Swapper lets you change colour definitions.&lt;br /&gt; Check what your theme will look like in the
Showroom display.&lt;br /&gt;
&lt;br /&gt;
When you&apos;re done, use the PullDown Menu to apply the colours to the current session.</translation>
    </message>
    <message>
        <source>This area allows you to store your created palettes for re-use later.  There is one global Cupboard, and one specific for the current Ambience.&lt;br /&gt;
Note that only system-wide Ambiences have a name, custom ones will show as anonymous (for now)</source>
        <translation type="vanished">This area allows you to store your created palettes for re-use later.  There is one global Cupboard, and one specific for the current Ambience.&lt;br /&gt;
Note that only system-wide Ambiences have a name, custom ones will show as anonymous (for now)</translation>
    </message>
    <message>
        <source>Jolla Ambiences only define four colors, primary (which is black or white depending on Ambience Scheme), secondary which is a little less opaque primary with some added, 
plus highlight and secondary highlight which are those selected in the Ambience Settings.&lt;br /&gt;
The App lets you edit other colors than these four, but they will not be affected by Ambience changes or Lipstick restarts as they are stored in the dconf database and stay there. 
This means once applied through the app, they will always stay the same until you change them again in the App. 
You can use the reset option documented below to get rid of them if necessary.
&lt;br /&gt;
The app frequently gets confused about colors when switching editing modes, applying colors to system, or taking palettes from the Cupboard.
If that happens, try reloading the colors from the system, that helps in most cases.&lt;br /&gt;
&lt;br /&gt;
It is possible to create color schemes which make parts of the UI unreadable. Check especially non-obvious areas such as the virtual Keyboard.&lt;br /&gt;
It&apos;s a good idea to store a known-good color scheme in the Cupboard so you can go restore easily.&lt;br /&gt;
&lt;br /&gt;
If you have messed up the colors somehow, use the PullUp menu option to reset everything, or from command line issue:&lt;br /&gt;
&lt;pre&gt;dconf dump /desktop/jolla/theme/color/&lt;/pre&gt;
&lt;pre&gt;dconf reset /desktop/jolla/theme/color/highlightBackground&lt;/pre&gt;
and repeat for all the other colors stored there. &lt;br /&gt;
Changing the Ambience from the System Settings may also help.</source>
        <translation type="vanished">Jolla Ambiences only define four colours, primary (which is black or white depending on Ambience Scheme), secondary which is a little less opaque primary with some added,
plus highlight and secondary highlight which are those selected in the Ambience Settings.&lt;br /&gt;
The App lets you edit other colours than these four, but they will not be affected by Ambience changes or Lipstick restarts as they are stored in the dconf database and stay there.
This means once applied through the app, they will always stay the same until you change them again in the App.
You can use the reset option documented below to get rid of them if necessary.
&lt;br /&gt;
The app frequently gets confused about colours when switching editing modes, applying colours to system, or taking palettes from the Cupboard.
If that happens, try reloading the colours from the system, that helps in most cases.&lt;br /&gt;
&lt;br /&gt;
It is possible to create colour schemes which make parts of the UI unreadable. Check especially non-obvious areas such as the virtual Keyboard.&lt;br /&gt;
It&apos;s a good idea to store a known-good colour scheme in the Cupboard so you can go restore easily.&lt;br /&gt;
&lt;br /&gt;
If you have messed up the colours somehow, use the PullUp menu option to reset everything, or from command line issue:&lt;br /&gt;
&lt;pre&gt;dconf dump /desktop/jolla/theme/color/&lt;/pre&gt;
&lt;pre&gt;dconf reset /desktop/jolla/theme/color/highlightBackground&lt;/pre&gt;
and repeat for all the other colours stored there. &lt;br /&gt;
Changing the Ambience from the System Settings may also help.</translation>
    </message>
    <message>
        <source>This application allows you to modify the current color scheme of Lipstick. It will not (yet) however, change or create new Ambiences, nor will your changes survive an Ambience change, Lipstick restart, or device reboot.&lt;br /&gt;
Currently, only some colors can be edited. There are other colors in use by the system, which are autocomputed from the basic four, and can not be modified.&lt;br/&gt;
&lt;br/&gt;
We are working on overcoming some of these limitations.</source>
        <translation type="vanished">Programmet lar deg endre fargepaletten på Lipstick. Det vil ikke (enda) opprette eller endre nye omgivelser, ei heller overlever det endring av omgivelse, omstart av Lipstick, eller omstart av enheten.&lt;br /&gt;
Kun noen farger kan redigeres. Det er andre farger i bruk av systemet som automatisk utregnes fra de grunnleggende fire, og de kan ikke endres.&lt;br/&gt;
&lt;br/&gt;
Det jobbes med å overkomme disse hindrene.</translation>
    </message>
    <message>
        <source>The top area on the first page (&quot;Showroom&quot;) shows the colors that are selected currently using various UI elements.&lt;br /&gt;Here you can preview your creation.&lt;br /&gt;Tapping on either area will hide it. To unhide, tap the title header.</source>
        <translation type="vanished">Toppområdet på første side («Visningsrommet») er ikke interaktivt, og viser kun fargene som er valgt.&lt;br /&gt;Her kan du bivåne din kreasjon.&lt;br /&gt;Å trykke på et område skjuler det. For å vise det igjen kan du trykke på tittelfeltet.</translation>
    </message>
    <message>
        <source>In Slider input mode, use the Sliders in the lower section to adjust the colors. In Text input mode, you can enter color values directly. Randomizer does what it sais, and Jolla Original you already know. Swapper lets you change color definitions.&lt;br /&gt;
Check what your theme will look like in the Showroom display.&lt;br /&gt;
&lt;br /&gt;
When you&apos;re done, use the PullDown Menu to apply the colors to the curent session.</source>
        <translation type="vanished">I glidebryterinngangsmodus, kan du bruke dem for å senke en del for justering av fargene. I tekstinndatamodus kan du skrive inn fargeverdier direkte. Tilfeldighetsgeneratoren gjør det den sier, og den opprinnelige Jolla-oppførselen er det den ble laget som. Veksleren lar deg endre fargedefinisjoner.&lt;br /&gt;
Sjekk hvordan din drakt vil se ut i visningsrommet.&lt;br /&gt;
&lt;br /&gt;
Når du er ferdig kan du bruke nedtrekksmenyen for å bruke fargene i nåværende økt.</translation>
    </message>
    <message>
        <source>This area allows you to store your created palettes for re-use later. There is one global Cupboard, and one specific for the current Ambience.&lt;br /&gt;
Note that only system-wide Ambiences have a name, custom ones will show as anonymous (for now)</source>
        <translation type="vanished">Dette området lar deg lagre dine opprettede fargepaletter for gjenbruk senere. Det er et skap for hele systemet, og ett som er spesifikt for omgivelse.&lt;br /&gt;
Merk at kun omgivelser for hele systemet har et navn. Egendefinerte vil vises som anonyme (for øyeblikket).</translation>
    </message>
    <message>
        <source>Jolla Ambiences only define four colors, primary (which is black or white depending on Ambience Scheme), secondary which is primary  a little less opaque primary, plus highlight and secondary highlight which are those selected in the Ambience Settings.&lt;br /&gt;
The App lets you edit other colors than these four, but they will not be affected by Ambience changes or Lipstick restarts as they are stored in the dconf database and stay there. This means once applied through the app, they will always stay the same until you change them again in the App. You can use the reset option documented below to get rid of them if necessary.
&lt;br /&gt;
The app frequently gets confused about colors when switching editing modes, applying colors to system, or taking palettes from the Cupboard. If that happens, try reloading from the system, that helps in most cases.&lt;br /&gt;
&lt;br /&gt;
It is possible to create color schemes which make parts of the UI unreadable. Check especially non-obvious areas such as the virtual Keyboard.&lt;br /&gt;
It&apos;s a good idea to store a known-good color scheme in the Cupboard so you can go restore easily.&lt;br /&gt;
&lt;br /&gt;
If you have messed up the colors somehow, use the PullUp menu option to reset everything, or from command line issue:&lt;br /&gt;
&lt;pre&gt;dconf dump /desktop/jolla/theme/color/&lt;/pre&gt;
&lt;pre&gt;dconf reset /desktop/jolla/theme/color/highlightBackground&lt;/pre&gt;
and repeat for all the other colors stored there. &lt;br /&gt;
Changing the Ambience from the System Settings may also help.</source>
        <translation type="vanished">Jolla-omgivelser definerer kun fire farger. «Primær» (som er svart eller hvit avhengig av omgivelsesinnstilling), «Sekundær», har litt mindre dekkevne med litt lagt til, pluss «Framhevet» og «Sekundær framhevelsesfarge» som er de valgt i omgivelsesinnstillingene.
&lt;br /&gt;
Programmet lar deg redigere andre farger enn disse fire, men de blir ikke påvirket av endringer i omgivelse eller Lipstick-omstarter, siden de lagres i dconf-databasen og blir der. Dette betyr at når de er lagt til gjennom programmet, vil de alltid forbli de samme til du endrer dem igjen i programmet. Du kan tilbakestille valget nedenfor for å bli kvitt dem hvis nødvendig.
&lt;br /&gt;
Programmet blir ofte forvirret om farger ved veksling eller redigering av modus, tillegging av farger i systemet, eller å ta fargepaletter fra skapet. Hvis det skjer kan du prøve å gjeninnlaste fargene fra systemet. Det hjelper som oftest.&lt;br /&gt;
&lt;br /&gt;
Det er mulig å skape fargepaletter som gjør deler av grensesnittet uleselig. Sjekk spesielt ikke-åpenbare områder som det virtuelle tastaturet.&lt;br /&gt;
Det er en god idé å lagre farger du vet fungerer i skapet slik at du kan gjenopprette dem enkelt.&lt;br /&gt;
&lt;br /&gt;
Hvis du har tullet til fargene på noe vis, kan du bruke dragingsmenyen for å tilbakestille alt, eller bruke kommandolinjen:&lt;br /&gt;
&lt;pre&gt;dconf dump /desktop/jolla/theme/color/&lt;/pre&gt;
&lt;pre&gt;dconf reset /desktop/jolla/theme/color/highlightBackground&lt;/pre&gt;
og gjenta for alle de andre fargene lagret der.&lt;br /&gt;
Endring av omgivelse fra systeminnstillingene kan også hjelpe.</translation>
    </message>
    <message>
        <source>Jolla Ambiences only define four colors, primary (which is black or white depending on Ambience Scheme), secondary which is a little less opaque primary, plus highlight and secondary highlight which are those selected in the Ambience Settings.&lt;br /&gt;
The App lets you edit other colors than these four, but they will not be affected by Ambience changes or Lipstick restarts as they are stored in the dconf database and stay there. This means once applied through the app, they will always stay the same until you change them again in the App. You can use the reset option documented below to get rid of them if necessary.
&lt;br /&gt;
The app frequently gets confused about colors when switching editing modes, applying colors to system, or taking palettes from the Cupboard. If that happens, try reloading from the system, that helps in most cases.&lt;br /&gt;
&lt;br /&gt;
It is possible to create color schemes which make parts of the UI unreadable. Check especially non-obvious areas such as the virtual Keyboard.&lt;br /&gt;
It&apos;s a good idea to store a known-good color scheme in the Cupboard so you can go restore easily.&lt;br /&gt;
&lt;br /&gt;
If you have messed up the colors somehow, use the PullUp menu option to reset everything, or from command line issue:&lt;br /&gt;
&lt;pre&gt;dconf dump /desktop/jolla/theme/color/&lt;/pre&gt;
&lt;pre&gt;dconf reset /desktop/jolla/theme/color/highlightBackground&lt;/pre&gt;
and repeat for all the other colors stored there. &lt;br /&gt;
Changing the Ambience from the System Settings may also help.</source>
        <translation type="vanished">Jolla-omgivelser definerer kun fire farger. «Primær» (som er svart eller hvit avhengig av omgivelsesinnstilling), «Sekundær», har litt mindre dekkevne med litt lagt til, pluss «Framhevet» og «Sekundær framhevelsesfarge» som er de valgt i omgivelsesinnstillingene.
&lt;br /&gt;
Programmet lar deg redigere andre farger enn disse fire, men de blir ikke påvirket av endringer i omgivelse eller Lipstick-omstarter, siden de lagres i dconf-databasen og blir der. Dette betyr at når de er lagt til gjennom programmet, vil de alltid forbli de samme til du endrer dem igjen i programmet. Du kan tilbakestille valget nedenfor for å bli kvitt dem hvis nødvendig.
&lt;br /&gt;
Programmet blir ofte forvirret om farger ved veksling eller redigering av modus, tillegging av farger i systemet, eller å ta fargepaletter fra skapet. Hvis det skjer kan du prøve å gjeninnlaste fargene fra systemet. Det hjelper som oftest.&lt;br /&gt;
&lt;br /&gt;
Det er mulig å skape fargepaletter som gjør deler av grensesnittet uleselig. Sjekk spesielt ikke-åpenbare områder som det virtuelle tastaturet.&lt;br /&gt;
Det er en god idé å lagre farger du vet fungerer i skapet slik at du kan gjenopprette dem enkelt.&lt;br /&gt;
&lt;br /&gt;
Hvis du har tullet til fargene på noe vis, kan du bruke dragingsmenyen for å tilbakestille alt, eller bruke kommandolinjen:&lt;br /&gt;
&lt;pre&gt;dconf dump /desktop/jolla/theme/color/&lt;/pre&gt;
&lt;pre&gt;dconf reset /desktop/jolla/theme/color/highlightBackground&lt;/pre&gt;
og gjenta for alle de andre fargene lagret der.&lt;br /&gt;
Endring av omgivelse fra systeminnstillingene kan også hjelpe.</translation>
    </message>
    <message>
        <source>Version:</source>
        <translation type="vanished">Versjon:</translation>
    </message>
    <message>
        <source>Copyright:</source>
        <translation type="vanished">Opphavsrett:</translation>
    </message>
    <message>
        <source>License:</source>
        <translation type="vanished">Lisens:</translation>
    </message>
    <message>
        <source>Source Code:</source>
        <translation type="vanished">Kildekode:</translation>
    </message>
    <message>
        <source>Translations:</source>
        <translation type="vanished">Oversettelser:</translation>
    </message>
    <message>
        <source>Jolla Ambiences only define four colors, primary (which is black or white depending on Ambience Scheme), secondary which is a little less opaque primary with some added, plus highlight and secondary highlight which are those selected in the Ambience Settings.&lt;br /&gt;
The App lets you edit other colors than these four, but they will not be affected by Ambience changes or Lipstick restarts as they are stored in the dconf database and stay there. This means once applied through the app, they will always stay the same until you change them again in the App.  You can use the reset option documented below to get rid of them if necessary.
&lt;br /&gt;
The app frequently gets confused about colors when switching editing modes, applying colors to system, or taking palettes from the Cupboard. If that happens, try reloading the colors from the system, that helps in most cases.&lt;br /&gt;
&lt;br /&gt;
It is possible to create color schemes which make parts of the UI unreadable. Check especially non-obvious areas such as the virtual Keyboard.&lt;br /&gt;
It&apos;s a good idea to store a known-good color scheme in the Cupboard so you can go restore easily.&lt;br /&gt;
&lt;br /&gt;
If you have messed up the colors somehow, use the PullUp menu option to reset everything, or from command line issue:&lt;br /&gt;
&lt;pre&gt;dconf dump /desktop/jolla/theme/color/&lt;/pre&gt;
&lt;pre&gt;dconf reset /desktop/jolla/theme/color/highlightBackground&lt;/pre&gt;
and repeat for all the other colors stored there. &lt;br /&gt;
Changing the Ambience from the System Settings may also help.</source>
        <translation type="vanished">Jolla-omgivelser definerer kun fire farger. «Primær» (som er svart eller hvit avhengig av omgivelsesinnstilling), «Sekundær», har litt mindre dekkevne med litt lagt til, pluss «Framhevet» og «Sekundær framhevelsesfarge» som er de valgt i omgivelsesinnstillingene.
&lt;br /&gt;
Programmet lar deg redigere andre farger enn disse fire, men de blir ikke påvirket av endringer i omgivelse eller Lipstick-omstarter, siden de lagres i dconf-databasen og blir der. Dette betyr at når de er lagt til gjennom programmet, vil de alltid forbli de samme til du endrer dem igjen i programmet. Du kan tilbakestille valget nedenfor for å bli kvitt dem hvis nødvendig.
&lt;br /&gt;
Programmet blir ofte forvirret om farger ved veksling eller redigering av modus, tillegging av farger i systemet, eller å ta fargepaletter fra skapet. Hvis det skjer kan du prøve å gjeninnlaste fargene fra systemet. Det hjelper som oftest.&lt;br /&gt;
&lt;br /&gt;
Det er mulig å skape fargepaletter som gjør deler av grensesnittet uleselig. Sjekk spesielt ikke-åpenbare områder som det virtuelle tastaturet.&lt;br /&gt;
Det er en god idé å lagre farger du vet fungerer i skapet slik at du kan gjenopprette dem enkelt.&lt;br /&gt;
&lt;br /&gt;
Hvis du har tullet til fargene på noe vis, kan du bruke dragingsmenyen for å tilbakestille alt, eller bruke kommandolinjen:&lt;br /&gt;
&lt;pre&gt;dconf dump /desktop/jolla/theme/color/&lt;/pre&gt;
&lt;pre&gt;dconf reset /desktop/jolla/theme/color/highlightBackground&lt;/pre&gt;
og gjenta for alle de andre fargene lagret der.&lt;br /&gt;
Endring av omgivelse fra systeminnstillingene kan også hjelpe.</translation>
    </message>
    <message>
        <source>About</source>
        <translation type="vanished">Om</translation>
    </message>
    <message>
        <source>Version: </source>
        <translation type="vanished">Versjon: </translation>
    </message>
    <message>
        <source>Copyright: </source>
        <translation type="vanished">Opphavsrett: </translation>
    </message>
    <message>
        <source>License: </source>
        <translation type="vanished">Lisens: </translation>
    </message>
    <message>
        <source>Source Code: </source>
        <translation type="vanished">Kildekode: </translation>
    </message>
    <message>
        <source>General Information</source>
        <translation type="vanished">Generell informasjon</translation>
    </message>
    <message>
        <source>About the UI</source>
        <translation type="vanished">Om brukergrensesnittet</translation>
    </message>
    <message>
        <source>About Colors</source>
        <translation type="vanished">Om farger</translation>
    </message>
    <message>
        <source>Exporting Ambiences</source>
        <translation type="vanished">Eksportere atmosfærer</translation>
    </message>
    <message>
        <source>Limitations, Tips etc.</source>
        <translation type="vanished">Begrensninger, tips osv.</translation>
    </message>
    <message>
        <source>Credits</source>
        <translation type="vanished">Bidragsytere</translation>
    </message>
    <message>
        <source>Handbook</source>
        <comment>User Manual, Application Manual, Handbook, Howto</comment>
        <translation type="vanished">Håndbok</translation>
    </message>
    <message>
        <source>Chapter %1:</source>
        <comment>chapter in the handbook. argument is a number</comment>
        <translation type="vanished">Kapittel %1:</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="24"/>
        <source>General Information</source>
        <comment>Help Index</comment>
        <translation>Generell informasjon</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="25"/>
        <source>About the UI</source>
        <comment>Help Index</comment>
        <translation>Om brukergrensesnittet</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="26"/>
        <source>About Colors</source>
        <comment>Help Index</comment>
        <translation>Om farger</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="27"/>
        <source>Exporting Ambiences</source>
        <comment>Help Index</comment>
        <translation>Eksportere atmosfærer</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="28"/>
        <source>Watcher Daemon</source>
        <comment>Help Index</comment>
        <translation>Oppsynsnisse</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="29"/>
        <source>How it works</source>
        <comment>Help Index</comment>
        <translation>Hvordan det virker</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="30"/>
        <source>Limitations , Tips etc.</source>
        <comment>Help Index</comment>
        <translation>Begrensninger, tips osv.</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="31"/>
        <source>Credits</source>
        <comment>Help Index</comment>
        <translation>Bidragsytere</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="40"/>
        <source>Handbook</source>
        <comment>Help Index</comment>
        <extracomment>User Manual, Application Manual, Handbook, Howto</extracomment>
        <translation>Håndbok</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="46"/>
        <source>Chapter %1:</source>
        <comment>Help Index Chapter Prefix</comment>
        <extracomment>chapter in the handbook. argument is a number</extracomment>
        <translation>Kapittel %1:</translation>
    </message>
</context>
<context>
    <name>HelpTips</name>
    <message>
        <source>Tips and Caveats</source>
        <translation type="vanished">Tips og triks</translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpTips.qml" line="6"/>
        <source>Tips and Caveats</source>
        <comment>Help Section</comment>
        <translation>Tips og triks</translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpTips.qml" line="8"/>
        <source>
&lt;p&gt;
The app frequently gets confused about colors when switching editing modes, applying colors to system, or taking palettes from the Cupboard. If that happens, try reloading from the system, that helps in most cases.&lt;br /&gt;
&lt;/p&gt;
&lt;p&gt;
It is possible to create color schemes which make parts of the UI unreadable. Check especially non-obvious areas such as the virtual Keyboard.&lt;br /&gt;
It&apos;s a good idea to store a known-good color scheme in the Cupboard so you can go restore easily.
&lt;/p&gt;
&lt;p&gt;
If you have messed up the colors somehow, use the PullUp menu option to reset everything, or from command line issue:&lt;br /&gt;
&lt;pre&gt;dconf dump /desktop/jolla/theme/color/&lt;/pre&gt;
&lt;pre&gt;dconf reset /desktop/jolla/theme/color/highlightBackground&lt;/pre&gt;
and repeat for all the other colors stored there. &lt;br /&gt;
Changing the Ambience from the System Settings may also help.
&lt;/p&gt;
</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>HelpUIMain</name>
    <message>
        <source>The Showroom</source>
        <translation type="vanished">Visningsrommet</translation>
    </message>
    <message>
        <source>The Laboratory</source>
        <translation type="vanished">Laboratoriet</translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpUIMain.qml" line="7"/>
        <source>The main UI sections</source>
        <comment>Help Section</comment>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpUIMain.qml" line="9"/>
        <source>&lt;p&gt;The main UI consists of the following:
       &lt;ul&gt;
       &lt;li&gt;The Showroom&lt;/li&gt;
       &lt;li&gt;The Laboratory&lt;/li&gt;
       &lt;li&gt;The Cupboards&lt;/li&gt;
       &lt;li&gt;PullDown Menu&lt;/li&gt;
       &lt;/ul&gt;&lt;/p&gt;</source>
        <translation>&lt;p&gt;Hovedgrensesnittet består av følgende:
       &lt;ul&gt;
       &lt;li&gt;Visningsrommet&lt;/li&gt;
       &lt;li&gt;Laboratoriet&lt;/li&gt;
       &lt;li&gt;Skapene&lt;/li&gt;
       &lt;li&gt;Nedtrekksmenyen&lt;/li&gt;
       &lt;/ul&gt;&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpUIMain.qml" line="17"/>
        <source>The Showroom</source>
        <comment>Help Section</comment>
        <translation>Visningsrommet</translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpUIMain.qml" line="26"/>
        <source>&lt;p&gt;The top area on the first page (%1) shows the colors that are selected currently.
This is so you can preview and check how your creations will look.&lt;/p&gt;
&lt;p&gt;
There are two parts: the top one showing text in various combinations of colors, and the bottom one for the UI elements like buttons etc.
Tapping on either of those parts will hide it. When both are hidden, a miniature version will appear.&lt;br /&gt;
To unhide all again tap the section header.
&lt;/p&gt;</source>
        <extracomment>argument is the translation of &quot;Showroom&quot;</extracomment>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpUIMain.qml" line="41"/>
        <source>The Laboratory</source>
        <comment>Help Section</comment>
        <translation>Laboratoriet</translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpUIMain.qml" line="44"/>
        <source>&lt;p&gt;The next component is the %1. This is the where you edit the various colors.&lt;/p&gt;
&lt;p&gt;
On the top you find the input mode selector.
There are several ways to do this editing, called Input Modes, explained below.
The selector switches between them.
&lt;ul&gt;</source>
        <extracomment>argument is the translation of &quot;Laboratory&quot;</extracomment>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpUIMain.qml" line="50"/>
        <source>&lt;li&gt;&lt;strong&gt;Slider&lt;/strong&gt; input mode is the main editing mode.&lt;br /&gt;
There is a set of sliders for each color you can edit. Use them to adjust the colors using their red, green, blue (RGB) channels. 
Two slim indicators on top of each slider section shows the current, and the edited color, and the hex value will be shown in the area below them. If you tap that area, the sliders will reset to the value of the unedited color.&lt;br /&gt;
The button with the keyboard symbol opens a dialog where you can put in the value directly.&lt;br /&gt;
The button on the left side opens the Color Selector dialog from Sailfish Silica, in case you want to select a color from there.&lt;br /&gt;
At the bottom below the RGB sliders you will find the Glow editor. This works a little different than the others, allowing you to select the hue of any UI elements that have a glow effect. Below the slider there is a set of buttons that apply either black, white, or the four default colors to the Glow color.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpUIMain.qml" line="58"/>
        <source>&lt;li&gt;&lt;strong&gt;Generators:&lt;/strong&gt; this is a collection of various functions which manipulate all colors of the theme. Some are more useful than others, some are just for fun.
&lt;ul&gt;
&lt;li&gt;&lt;i&gt;Randomizer&lt;/i&gt; shuffles the color values to generate a random theme&lt;/li&gt;
&lt;li&gt;&lt;i&gt;Filters&lt;/i&gt; will change the existing theme colors slightly, e.g. darken or brighten them&lt;/li&gt;
&lt;li&gt;&lt;i&gt;Scheme Generators&lt;/i&gt; is a collection of things which will create a full theme&lt;/li&gt;
&lt;/ul&gt;
&lt;/li&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpUIMain.qml" line="67"/>
        <source>&lt;li&gt;&lt;strong&gt;Swapper/Copier&lt;/strong&gt; is a helper mode which lets you copy the value of one color to another, or swap two values.&lt;/li&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpUIMain.qml" line="68"/>
        <source>&lt;li&gt;&lt;strong&gt;Jolla Original&lt;/strong&gt; you already know, a remake of the element used in the Ambience Settings&lt;/li&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpUIMain.qml" line="83"/>
        <source>The Cupboard</source>
        <comment>Help Section</comment>
        <translation>Skapet</translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpUIMain.qml" line="86"/>
        <source>&lt;p&gt;
Cupboards are found to the right of the main page. These allow you to store your created palettes for re-use later.&lt;br /&gt;
The first page is the global Cupboard, where you can store any palette.
The second page is the Cupboard specific for the current Ambience, and its contents will change when the Ambience changes.
&lt;/p&gt;
&lt;p&gt;
Note that only Ambiences installed from a package can have a name, those created from Gallery will show as anonymous (for now)
&lt;/p&gt;
&lt;p&gt;
Per default the shelves are empty (showing all color pots as gray), but tapping the &lt;i&gt;%1&lt;/i&gt; button will save your current palette to that shelf, overwriting any values that may have been there.
The &lt;i&gt;%2&lt;/i&gt; button will load the stored palette and switch back to the main page.
&lt;/p&gt;
</source>
        <extracomment>arguments are the names of the menu entries &quot;Put on Shelf&quot; and &quot;Take to Lab&quot;</extracomment>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpUIMain.qml" line="99"/>
        <source>Put on Shelf</source>
        <translation>Putt på hyllen</translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpUIMain.qml" line="99"/>
        <source>Take to Lab</source>
        <translation>Ta til laboratoriet</translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpUIMain.qml" line="107"/>
        <source>Menus</source>
        <comment>Help Section</comment>
        <translation>Menyer</translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpUIMain.qml" line="115"/>
        <source>&lt;p&gt;
       The PullDown menu contains the most used commands.
       &lt;p&gt;
       &lt;ul&gt;
       &lt;li&gt;Apply: The central command of the app. This will apply the current color theme to the system.&lt;/li&gt;
       &lt;li&gt;Reload: Loads the theme that is currently in use in the system.&lt;/li&gt;
       &lt;li&gt;Export: opens the Export Ambience page. See the &lt;b&gt;Export&lt;/b&gt; chapter for more.&lt;/li&gt;
       &lt;li&gt;Advanced: takes you to a page containing various lesser used, experimental or destructive commands.&lt;/li&gt;
       &lt;/ul&gt;
       &lt;/p&gt;</source>
        <translation>&lt;p&gt;
       Nedtrekksmenyen inneholder hyppigst brukte kommandoer.
       &lt;p&gt;
       &lt;ul&gt;
       &lt;li&gt;Bruk: Den sentrale kommandoen i programmet. Dette ifører fargedrakten for systemet.&lt;/li&gt;
       &lt;li&gt;Gjeninnlast. Laster inn iført drakt igjen.&lt;/li&gt;
       &lt;li&gt;Eksporter: Åpner «Eksporter omgivelse»-siden. Sjekk &lt;b&gt;Eksport&lt;/b&gt;-kapittelet for mer info.&lt;/li&gt;
       &lt;li&gt;Avansert. Tar deg til en side som inneholder mindre brukte, eksperimentelle eller destruktive kommandoer.&lt;/li&gt;
       &lt;/ul&gt;
       &lt;/p&gt;</translation>
    </message>
    <message>
        <source>Menus</source>
        <translation type="vanished">Menyer</translation>
    </message>
    <message>
        <source>The Cupboard</source>
        <translation type="vanished">Skapet</translation>
    </message>
</context>
<context>
    <name>LoadAmbience</name>
    <message>
        <location filename="../qml/pages/advanced/LoadAmbience.qml" line="32"/>
        <source>Load</source>
        <translation>Last inn</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/LoadAmbience.qml" line="93"/>
        <source>Load Theme</source>
        <translation>Last Tema</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/LoadAmbience.qml" line="93"/>
        <source>Take to Lab</source>
        <translation>Ta til laboratoriet</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/LoadAmbience.qml" line="111"/>
        <source>Here you can load a .ambience (JSON) file.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/LoadAmbience.qml" line="115"/>
        <source>current</source>
        <translation>gjeldende</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/LoadAmbience.qml" line="121"/>
        <source>file data</source>
        <translation>Fildata</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/LoadAmbience.qml" line="127"/>
        <source>File name </source>
        <translation>Filnavn </translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/LoadAmbience.qml" line="129"/>
        <source>Click to select</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/LoadAmbience.qml" line="136"/>
        <source>File content</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/LoadAmbience.qml" line="145"/>
        <source>No file loaded</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>LootBoxItem</name>
    <message>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="23"/>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="28"/>
        <source>ThemeColor</source>
        <translation>DraktFarge</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="27"/>
        <source>A Lootbox was delivered!</source>
        <translation>En utbyttekasse ble levert.</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="28"/>
        <source>now has more shelves!</source>
        <translation>har nå flere hyller.</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="28"/>
        <source>Your persistence has been rewarded.</source>
        <translation>Din standhaftighet har blitt belønnet.</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="29"/>
        <source>Your persistence has been rewarded!</source>
        <translation>Din standhaftighet har blitt belønnet!</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="39"/>
        <source>Purchase Options</source>
        <translation>Kjøpsalternativer</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="48"/>
        <source>Payment* received!</source>
        <translation>Betaling* mottatt,</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="48"/>
        <source>Buy more shelves</source>
        <translation>Kjøp flere hyller</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="53"/>
        <source>Using Jolla Shop credentials to buy Storage Lootbox</source>
        <translation>Bruk identitetsdetaljene fra Jolla-butikken for å kjøpe lagringsutbyttekasse</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="70"/>
        <source>Thank you for your purchase!&lt;br /&gt;Your extra shelves will be delivered in the next update!</source>
        <translation>Takk for kjøpet.&lt;br /&gt;Dine ekstra hyller vil bli levert i neste oppdatering.</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="84"/>
        <source>*) No seriously there are no in-app-purchases in this app or Lootboxes in the Jolla Shop. That would be ridiculous.&lt;br /&gt;No funds were transfered.&lt;br /&gt; In fact, nothing really happened just now.&lt;br /&gt;…or did it?</source>
        <translation>*) Neida, det er ingen kjøpselementer i dette programmet eller utbyttekasser i Jolla-butikken. Det skulle tatt seg ut.&lt;br /&gt;Ingen penger ble oveført.&lt;br /&gt;Det skjedde faktisk ingenting nå.&lt;br /&gt;…eller?</translation>
    </message>
    <message>
        <source>*) No seriously there are no in-app-purchases in this app or Lootboxes in the Jolla Shop. That would be ridiculous.&lt;br /&gt;No funds were transfered.&lt;br /&gt; In fact, nothing really happened just now.&lt;br /&gt;...or did it?</source>
        <translation type="vanished">*) No seriously there are no in-app-purchases in this app or Lootboxes in the Jolla Shop. That would be ridiculous.&lt;br /&gt;No funds were transfered.&lt;br /&gt; In fact, nothing really happened just now.&lt;br /&gt;...or did it?</translation>
    </message>
</context>
<context>
    <name>SaveAmbience</name>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="47"/>
        <source>Export Functions</source>
        <translation>Eksportfunksjoner</translation>
    </message>
    <message>
        <source>Here you can export your creation to a .ambience (json) file.&lt;br /&gt;
                      This file can be picked up by the ThemeColor® RPM Builder™ companion tool which will package the whole thing as an RPM which you can then install.
                     </source>
        <translation type="vanished">Her kan du eksportere din kreasjon til en .ambience (JSON)-fil.&lt;br /&gt;
                      Denne filen vil bli plukket opp av ThemeColor® RPM-bygger™-tilleggsverktøyet, som vil pakke det hele som en RPM-pakke du kan installere.
                     </translation>
    </message>
    <message>
        <source>Here you can export your creation to an .ambience (JSON) file.&lt;br /&gt;
                      This file can be picked up by the ThemeColor® RPM Builder™ companion tool, which will package the whole thing as an RPM you can then install.
                     </source>
        <translation type="vanished">Her kan du eksportere din kreasjon til en .ambience (JSON)-fil.&lt;br /&gt;
                      Denne filen vil bli plukket opp av ThemeColor® RPM-bygger™-tilleggsverktøyet, som vil pakke det hele som en RPM-pakke du kan installere.
                     </translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="61"/>
        <source>Here you can export your creation to a .ambience (JSON) file, create an installable package from it, and optionally install that on your device.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="63"/>
        <source>To do that, first enter a name for your %1, then tap the &amp;quot;%2&amp;quot; button. After this, you can either edit the file, or generate a package from it.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="63"/>
        <source>Ambience</source>
        <translation>Omgivelse</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="83"/>
        <source>Export to File</source>
        <translation>Eksporter til fil</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="88"/>
        <source>Ambience Name</source>
        <translation>Ambience-navn</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="90"/>
        <source>A cool Ambience Name</source>
        <translation>Et snedig Ambience-navn</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="109"/>
        <source>Click to prepare</source>
        <translation></translation>
    </message>
    <message>
        <source>Click to export</source>
        <translation type="vanished">Klikk for å eksportere</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="110"/>
        <source>File Name</source>
        <translation>Filnavn</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="196"/>
        <source>Open File</source>
        <translation>Åpne fil</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="197"/>
        <source>Launch Builder</source>
        <translation>Start byggherre</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="68"/>
        <source>Disclaimer</source>
        <translation>Ansvarsfraskrivelse</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="35"/>
        <source>Ambience Package</source>
        <translation type="unfinished">Atmosfærepakke</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="65"/>
        <source>Both the .ambience file as well as any built packages will be stored in the %1 folder. (%2)</source>
        <comment>first argument is the short name of the locatin, second argument is the full path</comment>
        <translation>Både .ambience.filen og alle innebygde pakker lagres i %1-mappen. (%2)</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="65"/>
        <source>Documents</source>
        <translation>Dokumenter</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="80"/>
        <source>If you decide to share or otherwise distribute the package make sure you respect copy- and other rights for any content included.</source>
        <translation></translation>
    </message>
    <message>
        <source>Help</source>
        <translation type="obsolete">Hjelp</translation>
    </message>
</context>
<context>
    <name>SaveSlot</name>
    <message>
        <location filename="../qml/components/saver/SaveSlot.qml" line="40"/>
        <location filename="../qml/components/saver/SaveSlot.qml" line="41"/>
        <location filename="../qml/components/saver/SaveSlot.qml" line="42"/>
        <source>Ambience</source>
        <translation>Omgivelse</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/SaveSlot.qml" line="40"/>
        <location filename="../qml/components/saver/SaveSlot.qml" line="41"/>
        <location filename="../qml/components/saver/SaveSlot.qml" line="42"/>
        <location filename="../qml/components/saver/SaveSlot.qml" line="44"/>
        <source>Shelf</source>
        <translation>Hylle</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/SaveSlot.qml" line="40"/>
        <source>default</source>
        <translation type="unfinished">standard</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/SaveSlot.qml" line="41"/>
        <source>night mode</source>
        <translation>Nattmodus</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/SaveSlot.qml" line="71"/>
        <source>Take to Lab</source>
        <translation>Ta til laboratoriet</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/SaveSlot.qml" line="72"/>
        <source>Put on Shelf</source>
        <translation>Putt på hyllen</translation>
    </message>
</context>
<context>
    <name>Saver</name>
    <message>
        <location filename="../qml/pages/Saver.qml" line="99"/>
        <source>Global Cupboard</source>
        <translation>Skap for hele systemet</translation>
    </message>
    <message>
        <location filename="../qml/pages/Saver.qml" line="129"/>
        <source>Clean out this cupboard</source>
        <translation>Tøm dette skapet</translation>
    </message>
    <message>
        <location filename="../qml/pages/Saver.qml" line="129"/>
        <source>Spring Clean</source>
        <translation>Vårrengjøring</translation>
    </message>
</context>
<context>
    <name>SaverPlus</name>
    <message>
        <source>anonymous</source>
        <translation type="vanished">anonym</translation>
    </message>
    <message>
        <location filename="../qml/pages/SaverPlus.qml" line="14"/>
        <source>User Ambience</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SaverPlus.qml" line="103"/>
        <source>System Ambience</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SaverPlus.qml" line="115"/>
        <source>Ambience Cupboard</source>
        <translation>Omgivelsesskap</translation>
    </message>
    <message>
        <location filename="../qml/pages/SaverPlus.qml" line="169"/>
        <source>Clean out this cupboard</source>
        <translation>Tøm dette skapet</translation>
    </message>
    <message>
        <location filename="../qml/pages/SaverPlus.qml" line="169"/>
        <source>Spring Clean</source>
        <translation>Vårrengjøring</translation>
    </message>
</context>
<context>
    <name>ShowRoom</name>
    <message>
        <source>A very long line showing Text in </source>
        <translation type="vanished">En veldig lang tekstlinje som viser tekst i </translation>
    </message>
    <message>
        <source>Color</source>
        <translation type="vanished">Colour</translation>
    </message>
    <message>
        <source>Primary Color</source>
        <translation type="vanished">Primærfarge</translation>
    </message>
    <message>
        <source>Secondary Color</source>
        <translation type="vanished">Sekundærfarge</translation>
    </message>
    <message>
        <source>Highlight Color</source>
        <translation type="vanished">Framhevelsesfarge</translation>
    </message>
    <message>
        <source>Secondary Highlight Color</source>
        <translation type="vanished">Sekundær framhevelsesfarge</translation>
    </message>
    <message>
        <source>Error Color</source>
        <translation type="vanished">Feilfarge</translation>
    </message>
    <message>
        <source>Background Color</source>
        <translation type="vanished">Bakgrunnsfarge</translation>
    </message>
    <message>
        <source>four kinds of background overlay opacities and colors</source>
        <translation type="vanished">four kinds of background overlay opacities and colours</translation>
    </message>
    <message>
        <source>Text</source>
        <translation type="vanished">Tekst</translation>
    </message>
    <message>
        <source>Highlight Background Color</source>
        <translation type="vanished">Framhevelsesbakgrunnsfarge</translation>
    </message>
    <message>
        <source>Progress Bar Demo</source>
        <translation type="vanished">Framdriftsbjelke-demo</translation>
    </message>
    <message>
        <source>Tap to restart Demos</source>
        <translation type="vanished">Trykk for å tilbakestille demoer</translation>
    </message>
    <message>
        <source>Remorse Item Demo</source>
        <translation type="vanished">Angringselement-demo</translation>
    </message>
    <message>
        <source>MenuItem</source>
        <translation type="vanished">menyelement</translation>
    </message>
    <message>
        <source>selected</source>
        <translation type="vanished">valgt</translation>
    </message>
    <message>
        <source>disabled</source>
        <translation type="vanished">avskrudd</translation>
    </message>
    <message>
        <source>Button</source>
        <translation type="vanished">Knapp</translation>
    </message>
    <message>
        <location filename="../qml/components/ShowRoom.qml" line="16"/>
        <source>Text Elements</source>
        <translation>Tekstelementer</translation>
    </message>
    <message>
        <location filename="../qml/components/ShowRoom.qml" line="24"/>
        <source>UI Elements</source>
        <translation>Grensesnittselementer</translation>
    </message>
</context>
<context>
    <name>ShowRoomMini</name>
    <message>
        <location filename="../qml/components/ShowRoomMini.qml" line="21"/>
        <source>Mini</source>
        <comment>small showroom</comment>
        <translation>Mini</translation>
    </message>
    <message>
        <location filename="../qml/components/ShowRoomMini.qml" line="21"/>
        <source>Showroom</source>
        <translation>Visningsrom</translation>
    </message>
</context>
<context>
    <name>ShowRoomText</name>
    <message>
        <source>A very long line showing Text in </source>
        <translation type="vanished">En veldig lang tekstlinje som viser tekst i </translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="17"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="29"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="38"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="63"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="72"/>
        <source>Primary Color</source>
        <translation>Primærfarge</translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="17"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="18"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="19"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="20"/>
        <source>A very long line showing Text in</source>
        <translation>En veldig lang tekstlinje som viser tekst i</translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="18"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="30"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="39"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="64"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="73"/>
        <source>Secondary Color</source>
        <translation>Sekundærfarge</translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="19"/>
        <source>Highlight Color</source>
        <translation>Framhevingsfarge</translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="20"/>
        <source>Secondary Highlight Color</source>
        <translation>Sekundær framhevingsfarge</translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="63"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="64"/>
        <source>Cover Background Color</source>
        <translation type="unfinished">Omslagsbakgrunnsfarge</translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="72"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="73"/>
        <source>Wallpaper Overlay Color</source>
        <translation></translation>
    </message>
    <message>
        <source>Error Color</source>
        <translation type="vanished">Feilfarge</translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="29"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="30"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="38"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="39"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="63"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="64"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="72"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="73"/>
        <source>Text</source>
        <translation>Tekst</translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="29"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="30"/>
        <source>Highlight Background Color</source>
        <translation>Framhevelsesbakgrunnsfarge</translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="38"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="39"/>
        <source>Overlay Background Color</source>
        <translation>Overleggs-bakgrunnsfarge</translation>
    </message>
    <message>
        <source>Dim Highlight Color</source>
        <translation type="vanished">Diffus framhevelsesfarge</translation>
    </message>
</context>
<context>
    <name>ShowRoomUI</name>
    <message>
        <location filename="../qml/components/showroom/ShowRoomUI.qml" line="32"/>
        <source>Progress Bar Demo</source>
        <translation>Framdriftsbjelke-demo</translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomUI.qml" line="35"/>
        <source>Remorse Item Demo</source>
        <translation>Angringselement-demo</translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomUI.qml" line="90"/>
        <location filename="../qml/components/showroom/ShowRoomUI.qml" line="91"/>
        <location filename="../qml/components/showroom/ShowRoomUI.qml" line="92"/>
        <source>MenuItem</source>
        <translation>menyelement</translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomUI.qml" line="92"/>
        <source>selected</source>
        <translation>valgt</translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomUI.qml" line="91"/>
        <source>disabled</source>
        <translation>avskrudd</translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomUI.qml" line="124"/>
        <location filename="../qml/components/showroom/ShowRoomUI.qml" line="125"/>
        <source>Button</source>
        <translation>Knapp</translation>
    </message>
</context>
<context>
    <name>TransparencyEditor</name>
    <message>
        <location filename="../qml/pages/advanced/TransparencyEditor.qml" line="41"/>
        <source>Edit Transparency</source>
        <translation>Rediger gjennomsiktighet</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/TransparencyEditor.qml" line="49"/>
        <source>This will edit the alpha channel of the color. Note that in the Sailfish UI many elements use their own transparency values and are not affected by the alpha channel of the color. Remorse Timers are one such example.</source>
        <translation>Dette redigerer alfakanalen tilhørende fargen. Merk at i Sailfish-grensesnittet bruker mange elementer sin egne gjennomsiktighetsverdier, og påvirkes ikke av alfakanalen til fargen. Avglemmingstidsur er ett eksempel.</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/TransparencyEditor.qml" line="78"/>
        <source>Highlight Background Opacity</source>
        <translation>Dekkevne for bakgrunnsframhevelse</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/TransparencyEditor.qml" line="79"/>
        <source>This is used e.g. for Pulley Menu background.</source>
        <translation>Dette brukes f.eks. for nedtrekksmenyens bakgrunn.</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/TransparencyEditor.qml" line="81"/>
        <source>Wallpaper Overlay Opacity</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/TransparencyEditor.qml" line="83"/>
        <source>Cover Overlay Opacity</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/TransparencyEditor.qml" line="84"/>
        <source>Transparency of application covers.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/TransparencyEditor.qml" line="90"/>
        <source>Color Alpha Channel</source>
        <translation>Alfakanal for farge</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/TransparencyEditor.qml" line="91"/>
        <source>Here you can edit Alpha channels for colors that have one.</source>
        <translation>Her kan du redigere alfakanaler for farger som har det.</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/TransparencyEditor.qml" line="92"/>
        <source>Secondary Color</source>
        <translation>Sekundærfarge</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/TransparencyEditor.qml" line="94"/>
        <source>Secondary Highlight Color</source>
        <translation>Sekundær framhevelsesfarge</translation>
    </message>
    <message>
        <source>Highlight Background Color</source>
        <translation type="obsolete">Framhevelsesbakgrunnsfarge</translation>
    </message>
</context>
</TS>
