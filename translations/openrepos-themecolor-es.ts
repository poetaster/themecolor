<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="es">
<context>
    <name>AdvancedPage</name>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="17"/>
        <source>Advanced Options</source>
        <translation>Opciones avanzadas</translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="26"/>
        <source>System Functions</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="29"/>
        <location filename="../qml/pages/AdvancedPage.qml" line="35"/>
        <source>Restart %1</source>
        <comment>restart an application, as in &apos;restart service foo&apos;</comment>
        <translation type="unfinished">Reiniciar %1</translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="30"/>
        <location filename="../qml/pages/AdvancedPage.qml" line="57"/>
        <source>Lipstick</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="30"/>
        <source>Home Screen</source>
        <translation>Pantalla de inicio</translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="30"/>
        <source>Restart the %1 service. This will close all apps and relaunch the %2</source>
        <comment>arguments are &apos;Lipstick&apos; and &apos;Home Screen&apos;, one id the application name, one is the commonly used name for the same.</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="31"/>
        <location filename="../qml/pages/AdvancedPage.qml" line="37"/>
        <source>Restarting</source>
        <translation>Reiniciando</translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="36"/>
        <source>Restart the %1 service. This sometimes fixes color problems</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="36"/>
        <location filename="../qml/pages/AdvancedPage.qml" line="78"/>
        <source>Ambience</source>
        <translation>Ambiente</translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="49"/>
        <source>Config Management</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="51"/>
        <source>Reset Config</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="52"/>
        <source>Reset all color-related configuration values</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="53"/>
        <location filename="../qml/pages/AdvancedPage.qml" line="58"/>
        <source>Resetting</source>
        <translation>Restableciendo</translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="56"/>
        <source>Reset Config and Restart</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="57"/>
        <source>Reset all color-related configuration values and restart %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="75"/>
        <source>Background Service</source>
        <translation>Servicio en segundo plano</translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="77"/>
        <source>Set up daemon</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="78"/>
        <source>Background service which watches for %1 changes and applies actions.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="81"/>
        <source>Import/Export</source>
        <translation>Importar/Exportar</translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="84"/>
        <source>Load Ambience File</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="85"/>
        <source>Load a color scheme from a .ambience file.</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>AppInfoSingleton</name>
    <message>
        <source>ThemeColor</source>
        <translation type="vanished">ColorDelTema</translation>
    </message>
    <message>
        <location filename="../qml/components/AppInfoSingleton.qml" line="21"/>
        <source>ThemeColor</source>
        <comment>this will also be used for the app icon</comment>
        <translation>ColorDelTema</translation>
    </message>
    <message>
        <location filename="../qml/components/AppInfoSingleton.qml" line="22"/>
        <source>ThemeColor® RPM Builder™</source>
        <comment>this will also be used for the app icon</comment>
        <translation>ThemeColor® RPM Constructor™</translation>
    </message>
    <message>
        <location filename="../qml/components/AppInfoSingleton.qml" line="34"/>
        <source>English</source>
        <comment>Language name in local language. So, if local language is French, &apos;German&apos; should be &apos;allemande&apos;, not &apos;Deutsch&apos;.</comment>
        <translation>Inglés</translation>
    </message>
    <message>
        <location filename="../qml/components/AppInfoSingleton.qml" line="31"/>
        <source>German</source>
        <comment>Language name in local language. So, if local language is French, &apos;German&apos; should be &apos;allemande&apos;, not &apos;Deutsch&apos;.</comment>
        <translation>Alemán</translation>
    </message>
    <message>
        <location filename="../qml/components/AppInfoSingleton.qml" line="44"/>
        <source>Norwegian Bokmål</source>
        <comment>Language name in local language. So, if local language is French, &apos;German&apos; should be &apos;allemande&apos;, not &apos;Deutsch&apos;.</comment>
        <translation>Bokmål Noruego</translation>
    </message>
    <message>
        <location filename="../qml/components/AppInfoSingleton.qml" line="37"/>
        <source>Spanish</source>
        <comment>Language name in local language. So, if local language is French, &apos;German&apos; should be &apos;allemande&apos;, not &apos;Deutsch&apos;.</comment>
        <translation>Español</translation>
    </message>
    <message>
        <location filename="../qml/components/AppInfoSingleton.qml" line="41"/>
        <source>French</source>
        <comment>Language name in local language. So, if local language is French, &apos;German&apos; should be &apos;allemande&apos;, not &apos;Deutsch&apos;.</comment>
        <translation>Francés</translation>
    </message>
    <message>
        <location filename="../qml/components/AppInfoSingleton.qml" line="47"/>
        <source>Swedish</source>
        <comment>Language name in local language. So, if local language is French, &apos;German&apos; should be &apos;allemande&apos;, not &apos;Deutsch&apos;.</comment>
        <translation>Sueco</translation>
    </message>
    <message>
        <location filename="../qml/components/AppInfoSingleton.qml" line="50"/>
        <source>Simplified Chinese</source>
        <comment>Language name in local language. So, if local language is French, &apos;German&apos; should be &apos;allemande&apos;, not &apos;Deutsch&apos;.</comment>
        <translation>Chino simplificado</translation>
    </message>
</context>
<context>
    <name>ColorField</name>
    <message>
        <source>specify RGB or aRGB value, e.g.</source>
        <translation type="vanished">especifica valor RGB o aRGB, p.e.</translation>
    </message>
    <message>
        <location filename="../qml/components/controls/ColorField.qml" line="9"/>
        <source>an %1 value</source>
        <comment>parameter is either RGB or aRGB</comment>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/components/controls/ColorField.qml" line="10"/>
        <source>(the # is optional)</source>
        <translation>(# es opcional)</translation>
    </message>
    <message>
        <location filename="../qml/components/controls/ColorField.qml" line="10"/>
        <source>e.g.</source>
        <translation type="unfinished">p.ej.</translation>
    </message>
</context>
<context>
    <name>ColorFilters</name>
    <message>
        <source>Tint</source>
        <translation type="vanished">Teñir</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorFilters.qml" line="17"/>
        <source>Red</source>
        <translation>Rojo</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorFilters.qml" line="17"/>
        <source>Tint %1</source>
        <translation>Tinte %1</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorFilters.qml" line="25"/>
        <source>Darken</source>
        <translation>Oscurecer</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorFilters.qml" line="33"/>
        <source>Brighten</source>
        <translation>Aclarar</translation>
    </message>
</context>
<context>
    <name>ColorGenerators</name>
    <message>
        <location filename="../qml/components/ColorGenerators.qml" line="10"/>
        <source>Randomizer</source>
        <translation>Aleatorizar</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorGenerators.qml" line="12"/>
        <source>Filters</source>
        <translation>Filtros</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorGenerators.qml" line="14"/>
        <source>Scheme Generators</source>
        <translation>Generadores de tema</translation>
    </message>
</context>
<context>
    <name>ColorRandomizer</name>
    <message>
        <location filename="../qml/components/generators/ColorRandomizer.qml" line="16"/>
        <source>Random</source>
        <translation>Aleatorio</translation>
    </message>
    <message>
        <source>Colors</source>
        <translation type="vanished">Colores</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorRandomizer.qml" line="21"/>
        <location filename="../qml/components/generators/ColorRandomizer.qml" line="35"/>
        <location filename="../qml/components/generators/ColorRandomizer.qml" line="45"/>
        <source>Generated</source>
        <translation>Generados</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorRandomizer.qml" line="26"/>
        <source>Bright</source>
        <translation>Brillantes</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorRandomizer.qml" line="26"/>
        <source>Dark</source>
        <translation>Oscuros</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorRandomizer.qml" line="40"/>
        <source>Gray</source>
        <translation>Grises</translation>
    </message>
</context>
<context>
    <name>ColorSchemeGenerator</name>
    <message>
        <source>Solarize</source>
        <translation type="vanished">Solarizar</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="13"/>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="28"/>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="35"/>
        <source>%1 Theme</source>
        <comment>parameter is name for a theme, e.g. night, day, random... you might just use the parameter and not translate theme at all.</comment>
        <translation>Tema %1</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="13"/>
        <source>Solarize</source>
        <comment>as in &apos;solarized&apos; color scheme</comment>
        <translation>Solarizado</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="20"/>
        <source>Generate</source>
        <translation>Generar</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="20"/>
        <source>from</source>
        <translation>de</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="20"/>
        <source>Highlight</source>
        <translation>Resaltar</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="28"/>
        <source>Night</source>
        <translation>Nocturno</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="48"/>
        <source>%1 Color Blindness</source>
        <comment>%1 is replaced with the variant of color blindness, as in &apos;red-green color-blindness&apos;</comment>
        <translation>Acromatopsia %1</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="48"/>
        <source>R/G</source>
        <comment>abbreviation for red-green color blindness</comment>
        <translation>R/V</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="48"/>
        <source>#%1/%2</source>
        <comment>for lists, as in &apos;number one of four&apos; is #1/4</comment>
        <translation>#%1/%2</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="35"/>
        <source>Summer</source>
        <translation>Verano</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="16"/>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="23"/>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="31"/>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="38"/>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="54"/>
        <source>Applying</source>
        <translation>Aplicando</translation>
    </message>
</context>
<context>
    <name>ColorSelectors</name>
    <message>
        <source>Color</source>
        <translation type="vanished">Colour</translation>
    </message>
    <message>
        <source>Primary Color</source>
        <translation type="vanished">Primary Colour</translation>
    </message>
    <message>
        <source>Secondary Color</source>
        <translation type="vanished">Secondary Colour</translation>
    </message>
    <message>
        <source>Highlight Color</source>
        <translation type="vanished">Highlight Colour</translation>
    </message>
    <message>
        <source>Secondary Highlight Color</source>
        <translation type="vanished">Secondary Highlight Colour</translation>
    </message>
</context>
<context>
    <name>ColorSlider</name>
    <message>
        <location filename="../qml/components/controls/ColorSlider.qml" line="58"/>
        <source>Adjust sliders, tap to reset</source>
        <translation>Ajusta controles deslizantes, toca para resetear</translation>
    </message>
</context>
<context>
    <name>ColorSliders</name>
    <message>
        <source>Color</source>
        <translation type="vanished">Colour</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSliders.qml" line="10"/>
        <source>Primary Color</source>
        <translation>Color principal</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSliders.qml" line="15"/>
        <source>Secondary Color</source>
        <translation>Color secundario</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSliders.qml" line="20"/>
        <source>Highlight Color</source>
        <translation>Color de realce</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSliders.qml" line="25"/>
        <source>Secondary Highlight Color</source>
        <translation>Color secundario de realce</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSliders.qml" line="30"/>
        <source>Background Highlight Color</source>
        <translation>Color de fondo de realce</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSliders.qml" line="35"/>
        <source>Dim Highlight Color</source>
        <translation>Color oscuro de realce</translation>
    </message>
    <message>
        <source>Highlight Background Color</source>
        <translation type="vanished">Color de fondo de realce</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSliders.qml" line="40"/>
        <source>Background Glow Color</source>
        <translation>Color de punto luminoso</translation>
    </message>
</context>
<context>
    <name>ColorSwapper</name>
    <message>
        <location filename="../qml/components/ColorSwapper.qml" line="74"/>
        <source>Copy</source>
        <translation>Copiar</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSwapper.qml" line="74"/>
        <source>Swap</source>
        <translation>Intercambiar</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSwapper.qml" line="95"/>
        <location filename="../qml/components/ColorSwapper.qml" line="122"/>
        <source>Primary Color</source>
        <translation>Color principal</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSwapper.qml" line="96"/>
        <location filename="../qml/components/ColorSwapper.qml" line="123"/>
        <source>Secondary Color</source>
        <translation>Color secundario</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSwapper.qml" line="97"/>
        <location filename="../qml/components/ColorSwapper.qml" line="124"/>
        <source>Highlight Color</source>
        <translation>Color de realce</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSwapper.qml" line="98"/>
        <location filename="../qml/components/ColorSwapper.qml" line="125"/>
        <source>Secondary Highlight Color</source>
        <translation>Color secundario de realce</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSwapper.qml" line="99"/>
        <location filename="../qml/components/ColorSwapper.qml" line="126"/>
        <source>Background Highlight Color</source>
        <translation>Color de fondo de realce</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSwapper.qml" line="100"/>
        <location filename="../qml/components/ColorSwapper.qml" line="127"/>
        <source>Dim Highlight Color</source>
        <translation>Color oscuro de realce</translation>
    </message>
    <message>
        <source>Highlight Background Color</source>
        <translation type="vanished">Color de fondo de realce</translation>
    </message>
    <message>
        <source>Color</source>
        <translation type="obsolete">Colour</translation>
    </message>
</context>
<context>
    <name>ColorTextInput</name>
    <message>
        <source>Color input</source>
        <translation type="vanished">Entrada de color</translation>
    </message>
    <message>
        <source>Input value, tap to reset</source>
        <translation type="vanished">Valor de entrada, toca para resetear</translation>
    </message>
</context>
<context>
    <name>ColorTextInputDialog</name>
    <message>
        <location filename="../qml/components/controls/ColorTextInputDialog.qml" line="11"/>
        <location filename="../qml/components/controls/ColorTextInputDialog.qml" line="24"/>
        <source>Color input</source>
        <translation>Entrada de color</translation>
    </message>
</context>
<context>
    <name>ColorTextInputs</name>
    <message>
        <source>Primary Color</source>
        <translation type="vanished">Color principal</translation>
    </message>
    <message>
        <source>Secondary Color</source>
        <translation type="vanished">Color secundario</translation>
    </message>
    <message>
        <source>Highlight Color</source>
        <translation type="vanished">Color de realce</translation>
    </message>
    <message>
        <source>Secondary Highlight Color</source>
        <translation type="vanished">Color secundario de realce</translation>
    </message>
    <message>
        <source>Background Highlight Color</source>
        <translation type="vanished">Color de fondo de realce</translation>
    </message>
    <message>
        <source>Dim Highlight Color</source>
        <translation type="vanished">Color oscuro de realce</translation>
    </message>
    <message>
        <source>Highlight Background Color</source>
        <translation type="vanished">Color de fondo de realce</translation>
    </message>
    <message>
        <source>Background Glow Color</source>
        <translation type="vanished">Color de punto luminoso</translation>
    </message>
</context>
<context>
    <name>CoverPage</name>
    <message>
        <source>ThemeColor</source>
        <translation type="vanished">ColorDelTema</translation>
    </message>
</context>
<context>
    <name>DaemonSettings</name>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="105"/>
        <source>Daemon Settings</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="115"/>
        <source>Enable Watcher daemon</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="110"/>
        <source>Watches for %1 changes and applies actions defined below.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="116"/>
        <source>Daemon is </source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="116"/>
        <source>active</source>
        <translation>activo</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="116"/>
        <source>inactive</source>
        <translation>inactivas</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="134"/>
        <source>Actions</source>
        <translation>Acciones</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="140"/>
        <source>Select what happens when an %1 change is detected</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="145"/>
        <source>launch %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="146"/>
        <source>just opens the app</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="156"/>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="158"/>
        <source>Warning:</source>
        <translation>Atención:</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="157"/>
        <source>The features below are experimental and do not work reliably.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="165"/>
        <source>apply Top theme</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="166"/>
        <source>applies theme from the top %1 shelf</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="173"/>
        <source>apply Night theme</source>
        <translation></translation>
    </message>
    <message>
        <source>ThemeColor</source>
        <translation type="obsolete">ColorDelTema</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="110"/>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="140"/>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="166"/>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="174"/>
        <source>Ambience</source>
        <translation>Ambiente</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="174"/>
        <source>taken from the second %1 shelf</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="187"/>
        <source>Night begins:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="201"/>
        <source>Night ends:</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>FirstPage</name>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="29"/>
        <source>Adjust Theme Colors</source>
        <translation>Ajustar colores del tema</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="47"/>
        <source>Showroom</source>
        <translation>Sala de exposiciones</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="98"/>
        <source>Laboratory</source>
        <translation>Laboratorio</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="112"/>
        <source>Input Mode:</source>
        <translation>Modo de entrada:</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="113"/>
        <source>Tap to switch</source>
        <translation>Toca para cambiar</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="122"/>
        <source>Swapper/Copier</source>
        <translation>Intercambiador/Copiadora</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="176"/>
        <source>Advanced…</source>
        <translation>Avanzado…</translation>
    </message>
    <message>
        <source>Compute all Colors from Highlight</source>
        <translation type="vanished">Calcular todos los colores de realce</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="183"/>
        <source>Applying</source>
        <translation>Aplicando</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="182"/>
        <source>Apply Colors to System</source>
        <translation>Aplicar colores al sistema</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="185"/>
        <source>Reload Colors from System</source>
        <translation>Reload Colours from System</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="123"/>
        <source>Generators</source>
        <translation>Generadores</translation>
    </message>
    <message>
        <source>Reload Colors from current Theme</source>
        <translation type="vanished">Volver a cargar colores del tema actual</translation>
    </message>
    <message>
        <source>Reload Colors from System Config</source>
        <translation type="vanished">Volver a cargar colores del sistema</translation>
    </message>
    <message>
        <source>Experimental or dangerous actions</source>
        <translation type="vanished">Acciones experimentales o peligrosas</translation>
    </message>
    <message>
        <source>Export to Ambience file</source>
        <translation type="vanished">Exportar a archivo de ambiente</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="179"/>
        <source>Export to Ambience package</source>
        <translation>Exportar a paquete de Ambiente</translation>
    </message>
    <message>
        <source>(not implemented)</source>
        <translation type="vanished">(no implementado)</translation>
    </message>
    <message>
        <source>Save Theme to current Ambience</source>
        <translation type="vanished">Guardar tema a ambiente actual</translation>
    </message>
    <message>
        <source>Saving</source>
        <translation type="vanished">Guardando</translation>
    </message>
    <message>
        <source>Resetting</source>
        <translation type="vanished">Restableciendo</translation>
    </message>
    <message>
        <source>Restart %1</source>
        <comment>restart an application, as in &apos;restart service foo&apos;</comment>
        <translation type="vanished">Reiniciar %1</translation>
    </message>
    <message>
        <source>Restarting</source>
        <translation type="vanished">Reiniciando</translation>
    </message>
    <message>
        <source>Reset all values and restart</source>
        <translation type="vanished">Restaurar todos los valores y reiniciar</translation>
    </message>
    <message>
        <source>Reset nonstandard values</source>
        <translation type="vanished">Restaurar a valores no estándar</translation>
    </message>
    <message>
        <source>Restart Lipstick</source>
        <translation type="vanished">Reiniciar Lipstick</translation>
    </message>
    <message>
        <source>Help</source>
        <translation type="vanished">Ayuda</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="121"/>
        <source>Sliders</source>
        <translation>Controles deslizantes</translation>
    </message>
    <message>
        <source>Text</source>
        <translation type="vanished">Texto</translation>
    </message>
    <message>
        <source>Swapper</source>
        <translation type="vanished">Swapper</translation>
    </message>
    <message>
        <source>Randomizer</source>
        <translation type="vanished">Aleatorizar</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="124"/>
        <source>Jolla Original</source>
        <translation>Original de Jolla</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="173"/>
        <source>Handbook</source>
        <comment>User Manual, Application Manual, Handbook, Howto</comment>
        <translation>Manual</translation>
    </message>
    <message>
        <source>Open Ambience Settings</source>
        <translation type="vanished">Abrir ajustes de ambiente</translation>
    </message>
</context>
<context>
    <name>HelpColors</name>
    <message>
        <source>The four basic Colors</source>
        <translation type="vanished">Los cuatro colores básicos</translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpColors.qml" line="6"/>
        <source>The four basic Colors</source>
        <comment>Help Section</comment>
        <translation>Los cuatro colores básicos</translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpColors.qml" line="8"/>
        <source>&lt;p&gt;Jolla Ambiences define four colors which are used for the various text elements around the UI.:&lt;br /&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpColors.qml" line="10"/>
        <source>primary, for most interactive elements (it is black or white depending on Ambience Scheme)</source>
        <translation>primario, para la mayoría de los elementos interactivos (es blanco o negro según el esquema de ambiente)</translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpColors.qml" line="11"/>
        <source>secondary, per default derived from primary by making it a little less opaque, is used for inactive elements, and most text</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpColors.qml" line="12"/>
        <source>highlight, which is what you selected in the Ambience Settings, colors active or activated elements</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpColors.qml" line="13"/>
        <source>secondary highlight, derived through some magic from the highlight color.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpColors.qml" line="16"/>
        <source>Additional Colors</source>
        <comment>Help Section</comment>
        <translation>Colores adicionales</translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpColors.qml" line="19"/>
        <source>
&lt;p&gt;
Other colors used:&lt;br /&gt;
&lt;ul&gt;
&lt;li&gt;Background and HighlightBackground style the background of Menus, Buttons and the like.&lt;/li&gt;
&lt;li&gt;DimmerHighlightBackground does the same, but for darker areas or high contrast elements.&lt;/li&gt;
&lt;li&gt;GlowColor is used for glassy things like Switches, forward/back indicators etc.&lt;/li&gt;
&lt;li&gt;CoverOverlayColor is/was used for Application Covers, but see note below.&lt;/li&gt;
&lt;/ul&gt;
&lt;/p&gt;
&lt;p&gt;
For more information about colors and other aspects of the GUI, see the official &lt;a href=&quot;https://sailfishos.org/develop/docs/silica/qml-sailfishsilica-sailfish-silica-theme.html&quot;&gt;Theme Documentation&lt;/a&gt;
&lt;/p&gt;
&lt;p&gt;
&lt;i&gt;
It must be said that while all those &lt;b&gt;Theme&lt;/b&gt; colors are still defined, as SailfishOS progresses, with UI developers continue to use QML&apos;s&lt;b&gt;palette&lt;/b&gt; property directly.
As we only deal with Theme colors, this unfortunately means more and more UI components stop reacting to those changes.
&lt;/i&gt;
&lt;/p&gt;
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Additional Colors</source>
        <translation type="vanished">Colores adicionales</translation>
    </message>
</context>
<context>
    <name>HelpCredits</name>
    <message>
        <source>About</source>
        <translation type="vanished">Acerca de</translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpCredits.qml" line="6"/>
        <source>About</source>
        <comment>Help Section</comment>
        <translation>Acerca de</translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpCredits.qml" line="7"/>
        <source>Version:</source>
        <translation>Versión:</translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpCredits.qml" line="8"/>
        <source>Copyright:</source>
        <translation>Derechos de autor:</translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpCredits.qml" line="9"/>
        <source>License:</source>
        <translation>Licencia:</translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpCredits.qml" line="10"/>
        <source>Source Code:</source>
        <translation>Código fuente:</translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpCredits.qml" line="11"/>
        <source>Translations</source>
        <translation>Traducciones</translation>
    </message>
</context>
<context>
    <name>HelpDaemon</name>
    <message>
        <location filename="../qml/pages/help/HelpDaemon.qml" line="7"/>
        <source>The Watcher Daemon</source>
        <comment>Help Section</comment>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpDaemon.qml" line="9"/>
        <source>Ambience</source>
        <translation>Ambiente</translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpDaemon.qml" line="9"/>
        <source>&lt;p&gt;The Watcher Daemon is a background service which can take care of some tasks while %1 is not running. Its main purpose is to apply color schemes after the %2 has changed.&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpDaemon.qml" line="10"/>
        <source>&lt;p&gt;It can be configured by tapping the &lt;i&gt;Daemon Settings&lt;/i&gt; button on the Advanced page.&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpDaemon.qml" line="11"/>
        <source>&lt;p&gt;This is an ever-evolving (and experimental) feature with more functions added each release.&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpDaemon.qml" line="12"/>
        <source>&lt;p&gt;One upcoming feature is to apply &amp;quot;Night Mode&amp;quot; color schemes at the appropriate time.&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>HelpExport</name>
    <message>
        <source>Exporting Ambiences</source>
        <translation type="vanished">Exportación de ambientes</translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpExport.qml" line="7"/>
        <source>Exporting Ambiences</source>
        <comment>Help Section</comment>
        <translation>Exportación de ambientes</translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpExport.qml" line="10"/>
        <source>&lt;p&gt;To export an Ambience, after having edited the colors, select the &quot;%1&quot; menu entry to bring up the Export page.&lt;/p&gt;</source>
        <extracomment>argument is the name of the menu entry &apos;Export to Ambience Package&apos;</extracomment>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpExport.qml" line="11"/>
        <source>Export to Ambience package</source>
        <translation>Exportar a paquete de Ambiente</translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpExport.qml" line="22"/>
        <source>&lt;p&gt;Enter a name for your Ambience there and click the Ambience Name box to save.
This will do two things: write a .ambience file to your Documents folder, and save some settings for the %1 companion tool (see below).&lt;br /&gt;
If you like you can now edit the .ambience file using a text editor, e.g. for adding ringtone and sound information.
&lt;/p&gt;</source>
        <extracomment>the argument is also a translated string, the name of the builder tool</extracomment>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpExport.qml" line="28"/>
        <source>The Builder</source>
        <comment>Help Section</comment>
        <translation>El Constructor</translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpExport.qml" line="31"/>
        <source>&lt;p&gt;
When ready, launch the %1.
The Builder picks up the information from dconf and the .ambience file to create an RPM file, and prompt to install it if successful.
The package will also be saved in the Documents folder so you can share it with your friends.
&lt;/p&gt;</source>
        <extracomment>the argument is also a translated string, the name of the builder tool</extracomment>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpExport.qml" line="39"/>
        <source>Notes</source>
        <comment>Help Section</comment>
        <extracomment>as in something to add, not a small piece of paper</extracomment>
        <translation>Notas</translation>
    </message>
    <message>
        <source>The Builder</source>
        <translation type="vanished">El Constructor</translation>
    </message>
    <message>
        <source>Notes</source>
        <comment>as in something to add, not a small piece of paper </comment>
        <translation type="vanished">Notas</translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpExport.qml" line="41"/>
        <source>&lt;ul&gt;
&lt;li&gt;The Builder is non-graphical and non-interactive. It needs to be configured through the app as described or it will not work.&lt;/li&gt;
&lt;li&gt;Installing an Ambience will switch it from Anonymous to a named Ambience. This means it will get a new Ambience shelf, which will be empty. Therefore it is a good idea to save the Scheme to the General Shelf.&lt;/li&gt;
&lt;li&gt;While both the .ambience file and the RPM package include most color values, not all are picked up by lipstick when switching Ambiences.  So, after having switched, open the App again and use the Ambience Shelf to load and apply your favourite scheme.&lt;/li&gt;
&lt;/ul&gt;
</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>HelpGeneral</name>
    <message>
        <source>General</source>
        <translation type="vanished">General</translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpGeneral.qml" line="6"/>
        <source>General</source>
        <comment>Help Section</comment>
        <translation>General</translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpGeneral.qml" line="8"/>
        <source>&lt;p&gt;
This application allows you to modify the current color scheme of Lipstick (a.k.a. Home Screen).
&lt;/p&gt;
&lt;p&gt;
It can not (yet) however, change saved Ambiences directly. Applied changes will not survive an Ambience change, Lipstick restart, or device reboot.&lt;br/&gt;
However, using the Shelves feature you can restore schemes you previously saved and re-apply them afterwards.
Also, by exporting changes to an installable package, changes can be made permanent.
&lt;/p&gt;
&lt;p&gt;
Currently only some colors can be edited. There are others in use by the system which are usually autocomputed from the basic four and can not be modified.&lt;br/&gt;
&lt;/p&gt;
&lt;p&gt;
We are working on overcoming some of these limitations, but some things would require patching the system UI elements which is beyond the scope of this app.
&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpGeneral.qml" line="24"/>
        <source>Basic Workflow</source>
        <comment>Help Section</comment>
        <translation>Flujo de trabajo básico</translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpGeneral.qml" line="26"/>
        <source>
&lt;p&gt;The basic workflow for re-coloring an existing Ambience or creating a new one would be something like this:&lt;/p&gt;

&lt;ol&gt;
&lt;li&gt;switch to an Ambience of your liking using the usual methods&lt;/li&gt;
&lt;li&gt;launch ThemeColor&lt;/li&gt;
&lt;li&gt;using the Sliders editing mode, adjust the four basic colors (primary, secondary, highlight and secondary highlight) to your liking.&lt;/li&gt;
&lt;li&gt;use the PullDown menu &lt;i&gt;Apply Colors to System&lt;/i&gt; to apply to the current theme&lt;/li&gt;
&lt;li&gt;explore the Sailfish UI a bit to make sure all looks right. Always check the VKB,  that can easily become unreadable&lt;/li&gt;
&lt;li&gt;go back to ThemeColor, and use the Shelves to store your colors for later reuse from within the app.&lt;/li&gt;
&lt;li&gt;adjust if necessary and repeat the last two steps above until everything looks pretty&lt;/li&gt;
&lt;li&gt;(optional:) to export and create an ambience RPM, and optionally install it, use the menu item &lt;i&gt;Export Ambience&lt;/i&gt;&lt;/li&gt;
&lt;/ol&gt;

&lt;p&gt; … and done! Your new Ambience should now show up in the usual place in Settings to select.&lt;/p&gt;

&lt;p&gt;If it does not (sometimes it happens) that means that ambienced has crashed.
In that case you can try to use the two &lt;i&gt;Restart&lt;/i&gt; functions on the Advanced page
to restart either Lipstick (the Home Screen), or just ambienced (the Theme engine)).
&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>HelpHow</name>
    <message>
        <location filename="../qml/pages/help/HelpHow.qml" line="6"/>
        <source>Loading and Applying Colors</source>
        <comment>Help Section</comment>
        <translation>Cargar y aplicar colores</translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpHow.qml" line="8"/>
        <source>
&lt;p&gt;
        Ambience Colors are stored in several places: the .ambience file for installed Ambiences, a user-local database for user-created Ambiences, and &lt;i&gt;dconf&lt;/i&gt;.
        When an Ambience is loaded, its color values are written to a DConf location, and lipstick reads the values from there and uses it for its Theme.&lt;br /&gt;
&lt;/p&gt;
&lt;p&gt;
        We use that last part in this application, in that when we &quot;Apply&quot; colors to the system, we simply write to that DConf location, and lipstick notices and adjusts it theme.
        The good thing is, while official Ambiences only define four colors (two of which are always the same in practice), writing additional values to that location works, if they are defined as changable by the Sailfish Theme.
&lt;/p&gt;
&lt;p&gt;
        The dconf path in question is &lt;pre&gt;/desktop/jolla/theme/color&lt;/pre&gt; and can be read from command line using &lt;pre&gt;dconf dump&lt;/pre&gt;
&lt;/p&gt;
</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpHow.qml" line="22"/>
        <source>App Configuration and State</source>
        <comment>Help Section</comment>
        <translation>Configuración y estado de la aplicación</translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpHow.qml" line="24"/>
        <source>
&lt;p&gt;
        Speaking of dconf, the application uses it in its own location to store things like Shelves and information for the builder.&lt;br /&gt;
        Unfortuately, if the user creates and deletes Ambiences a lot, and uses the Ambience Shelf, there might be a lot of stale setting in there which may warrant cleaning out from time to time.
&lt;/p&gt;
&lt;p&gt;
        The location for this is &lt;pre&gt;/org/nephros/openrepos-themecolor/storage&lt;/pre&gt;
&lt;/p&gt;
</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpHow.qml" line="34"/>
        <source>The Builder</source>
        <comment>Help Section</comment>
        <translation>El Constructor</translation>
    </message>
    <message>
        <source>The Builder</source>
        <translation type="vanished">El Constructor</translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpHow.qml" line="36"/>
        <source>
&lt;p&gt;
        The Ambience builder is a shell script launched from a oneshot systemd service with the following mode of operation:
&lt;/p&gt;
&lt;p&gt;
        First it reads DConf values prepared by the App to figure out things like Ambience name and location of files.&lt;br /&gt;
        It then puts them in a temporary directory together with a .spec file. It calls the &lt;pre&gt;rpmbuild&lt;/pre&gt; command which produces (hopfully) a package.&lt;br /&gt;
        If successful, that package is opened, prompting the user to install it.
&lt;/p&gt;
</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>HelpPage</name>
    <message>
        <source>How to Use</source>
        <translation type="vanished">Forma de utilización</translation>
    </message>
    <message>
        <source>General</source>
        <translation type="vanished">General</translation>
    </message>
    <message>
        <source>This application allows you to modify the current color scheme of Lipstick. It will not (yet) however, change or create new ambiences,           nor will your changes survive an Ambience change, Lipstick restart, or device reboot.&lt;br /&gt;
          Currently, only four colors can be edited: primaryColor, secondaryColor, highlightColor, and secondaryHighlightColor, as these are the ones saved by the system.          There are other colors in use by the system, which are autocomputed from these basic four, and can not be modified at the moment.&lt;br/&gt;&lt;br/&gt;
          We are working on overcoming some of these limitations.</source>
        <translation type="vanished">This application allows you to modify the current colour scheme of Lipstick. It will not (yet) however, change or create new ambiences,           nor will your changes survive an Ambience change, Lipstick restart, or device reboot.&lt;br /&gt;
          Currently, only four colours can be edited: primaryColor, secondaryColor, highlightColor, and secondaryHighlightColor (sic!), as these are the ones saved by the system.          There are other colours in use by the system, which are autocomputed from these basic four, and can not be modified at the moment.&lt;br/&gt;&lt;br/&gt;
          We are working on overcoming some of these limitations.</translation>
    </message>
    <message>
        <source>This application allows you to modify the current color scheme of Lipstick. It will not (yet) however, change or create new ambiences,           nor will your changes survive an Ambience change, Lipstick restart, or device reboot.&lt;br /&gt;
          Currently, only some colors can be edited.          There are other colors in use by the system, which are autocomputed from the basic four, and can not be modified at the moment.&lt;br/&gt;&lt;br/&gt;
          We are working on overcoming some of these limitations.</source>
        <translation type="vanished">This application allows you to modify the current colour scheme of Lipstick. It will not (yet) however, change or create new ambiences, nor will your changes survive an Ambience change, Lipstick restart, or device reboot.&lt;br /&gt;
Currently, only some colours can be edited. There are other colours in use by the system, which are autocomputed from the basic four, and can not be modified at the moment.&lt;br/&gt;&lt;br/&gt;
We are working on overcoming some of these limitations.</translation>
    </message>
    <message>
        <source>The Showroom</source>
        <translation type="vanished">La sala de exposiciones</translation>
    </message>
    <message>
        <source>The Laboratory</source>
        <translation type="vanished">El laboratorio</translation>
    </message>
    <message>
        <source>The Cupboard</source>
        <translation type="vanished">El armario</translation>
    </message>
    <message>
        <source>Tips and Caveats</source>
        <translation type="vanished">Consejos y advertencias</translation>
    </message>
    <message>
        <source>It is possible to create color schemes which make parts of the UI unreadable. Check especially non-obvious areas such as the virtual Keyboard.&lt;br /&gt;
          It&apos;s a good idea to store a known-good color sceme in the Cupboard so you can go restore easily.&lt;br /&gt;          &lt;br /&gt;          If you have messed up the colours completely, use the PullUp menu option to reset everything, or from command line issue:&lt;br /&gt;          &lt;pre&gt;dconf dump /desktop/jolla/theme/color/&lt;/pre&gt;          &lt;pre&gt;dconf reset /desktop/jolla/theme/color/highlightBackground&lt;/pre&gt;          and repeat for all the other colors stored there. &lt;br /&gt;          Changing the Ambience from the System Settings may also help.</source>
        <translation type="vanished">It is possible to create colour schemes which make parts of the UI unreadable. Check especially non-obvious areas such as the virtual Keyboard.&lt;br /&gt;
It&apos;s a good idea to store a known-good colour scheme in the Cupboard so you can go restore easily.&lt;br /&gt;&lt;br /&gt;If you have messed up the colours completely, use the PullUp menu option to reset everything, or from command line issue:&lt;br /&gt;&lt;pre&gt;dconf dump /desktop/jolla/theme/color/&lt;/pre&gt;&lt;pre&gt;dconf reset /desktop/jolla/theme/color/highlightBackground&lt;/pre&gt;and repeat for all the other colours stored there. &lt;br /&gt;Changing the Ambience from the System Settings may also help.</translation>
    </message>
    <message>
        <source>This application allows you to modify the current color scheme of Lipstick. It will not (yet) however, change or create new Ambiences, 
nor will your changes survive an Ambience change, Lipstick restart, or device reboot.&lt;br /&gt;
Currently, only some colors can be edited.
There are other colors in use by the system, which are autocomputed from the basic four, and can not be modified.&lt;br/&gt;&lt;br/&gt;
We are working on overcoming some of these limitations.</source>
        <translation type="vanished">This application allows you to modify the current colour scheme of Lipstick. It will not (yet) however, change or create new Ambiences, nor will your changes survive an Ambience change, Lipstick restart, or device reboot.&lt;br /&gt;
Currently, only some colours can be edited.
There are other colours in use by the system, which are autocomputed from the basic four, and can not be modified.&lt;br/&gt;&lt;br/&gt;
We are working on overcoming some of these limitations.</translation>
    </message>
    <message>
        <source>The top area on the first page (&quot;Showroom&quot;) is non-interactive and just shows the colors that are selected currently.&lt;br /&gt;Here you can preview your creation.</source>
        <translation type="vanished">El área superior de la primera página (&quot;Sala de exposiciones&quot;) no es interactiva y sólo muestra los colores que actualmente se han seleccionado.&lt;br/&gt;Aquí puedes obtener una vista previa de tu creación.</translation>
    </message>
    <message>
        <source>In Slider input mode, use the Sliders in the lower section to adjust the colors.  In Text input mode, you can enter color values directly. Randomizer
does what it sais, and Jolla Original you already know.  Swapper lets you change color definitions.&lt;br /&gt; Check what your theme will look like in the
Showroom display.&lt;br /&gt;
&lt;br /&gt;
When you&apos;re done, use the PullDown Menu to apply the colors to the curent session.</source>
        <translation type="vanished">In Slider input mode, use the Sliders in the lower section to adjust the colours. In Text input mode, you can enter colour values directly. Randomizer
does what it sais, and Jolla Original you already know. Swapper lets you change colour definitions.&lt;br /&gt; Check what your theme will look like in the
Showroom display.&lt;br /&gt;
&lt;br /&gt;
When you&apos;re done, use the PullDown Menu to apply the colours to the current session.</translation>
    </message>
    <message>
        <source>This area allows you to store your created palettes for re-use later.  There is one global Cupboard, and one specific for the current Ambience.&lt;br /&gt;
Note that only system-wide Ambiences have a name, custom ones will show as anonymous (for now)</source>
        <translation type="vanished">This area allows you to store your created palettes for re-use later.  There is one global Cupboard, and one specific for the current Ambience.&lt;br /&gt;
Note that only system-wide Ambiences have a name, custom ones will show as anonymous (for now)</translation>
    </message>
    <message>
        <source>Jolla Ambiences only define four colors, primary (which is black or white depending on Ambience Scheme), secondary which is a little less opaque primary with some added, 
plus highlight and secondary highlight which are those selected in the Ambience Settings.&lt;br /&gt;
The App lets you edit other colors than these four, but they will not be affected by Ambience changes or Lipstick restarts as they are stored in the dconf database and stay there. 
This means once applied through the app, they will always stay the same until you change them again in the App. 
You can use the reset option documented below to get rid of them if necessary.
&lt;br /&gt;
The app frequently gets confused about colors when switching editing modes, applying colors to system, or taking palettes from the Cupboard.
If that happens, try reloading the colors from the system, that helps in most cases.&lt;br /&gt;
&lt;br /&gt;
It is possible to create color schemes which make parts of the UI unreadable. Check especially non-obvious areas such as the virtual Keyboard.&lt;br /&gt;
It&apos;s a good idea to store a known-good color scheme in the Cupboard so you can go restore easily.&lt;br /&gt;
&lt;br /&gt;
If you have messed up the colors somehow, use the PullUp menu option to reset everything, or from command line issue:&lt;br /&gt;
&lt;pre&gt;dconf dump /desktop/jolla/theme/color/&lt;/pre&gt;
&lt;pre&gt;dconf reset /desktop/jolla/theme/color/highlightBackground&lt;/pre&gt;
and repeat for all the other colors stored there. &lt;br /&gt;
Changing the Ambience from the System Settings may also help.</source>
        <translation type="vanished">Jolla Ambiences only define four colours, primary (which is black or white depending on Ambience Scheme), secondary which is a little less opaque primary with some added,
plus highlight and secondary highlight which are those selected in the Ambience Settings.&lt;br /&gt;
The App lets you edit other colours than these four, but they will not be affected by Ambience changes or Lipstick restarts as they are stored in the dconf database and stay there.
This means once applied through the app, they will always stay the same until you change them again in the App.
You can use the reset option documented below to get rid of them if necessary.
&lt;br /&gt;
The app frequently gets confused about colours when switching editing modes, applying colours to system, or taking palettes from the Cupboard.
If that happens, try reloading the colours from the system, that helps in most cases.&lt;br /&gt;
&lt;br /&gt;
It is possible to create colour schemes which make parts of the UI unreadable. Check especially non-obvious areas such as the virtual Keyboard.&lt;br /&gt;
It&apos;s a good idea to store a known-good colour scheme in the Cupboard so you can go restore easily.&lt;br /&gt;
&lt;br /&gt;
If you have messed up the colours somehow, use the PullUp menu option to reset everything, or from command line issue:&lt;br /&gt;
&lt;pre&gt;dconf dump /desktop/jolla/theme/color/&lt;/pre&gt;
&lt;pre&gt;dconf reset /desktop/jolla/theme/color/highlightBackground&lt;/pre&gt;
and repeat for all the other colours stored there. &lt;br /&gt;
Changing the Ambience from the System Settings may also help.</translation>
    </message>
    <message>
        <source>This application allows you to modify the current color scheme of Lipstick. It will not (yet) however, change or create new Ambiences, nor will your changes survive an Ambience change, Lipstick restart, or device reboot.&lt;br /&gt;
Currently, only some colors can be edited. There are other colors in use by the system, which are autocomputed from the basic four, and can not be modified.&lt;br/&gt;
&lt;br/&gt;
We are working on overcoming some of these limitations.</source>
        <translation type="vanished">Con esta aplicación puedes modificar el esquema de color actual de Lipstick. Sin embargo, esta aplicación no cambiará ni creará ambientes nuevos (todavía), ni los cambios realizados continuarán cuando cambies de ambiente, reinicies Lipstick o reinicies el dispositivo.&lt;br/&gt;
Actualmente sólo pueden editarse algunos colores. Hay otros colores que usa el sistema, que se calculan automáticamente a partir de los cuatro colores básicos y no se pueden modificar.&lt;br/&gt;
&lt;br/&gt;
Estamos trabajando para superar algunas de estas limitaciones.</translation>
    </message>
    <message>
        <source>The top area on the first page (&quot;Showroom&quot;) shows the colors that are selected currently using various UI elements.&lt;br /&gt;Here you can preview your creation.&lt;br /&gt;Tapping on either area will hide it. To unhide, tap the title header.</source>
        <translation type="obsolete">El área superior de la primera página (&quot;Sala de exposiciones&quot;) no es interactiva y sólo muestra los colores que actualmente se han seleccionado.&lt;br/&gt;Aquí puedes obtener una vista previa de tu creación.</translation>
    </message>
    <message>
        <source>In Slider input mode, use the Sliders in the lower section to adjust the colors. In Text input mode, you can enter color values directly. Randomizer does what it sais, and Jolla Original you already know. Swapper lets you change color definitions.&lt;br /&gt;
Check what your theme will look like in the Showroom display.&lt;br /&gt;
&lt;br /&gt;
When you&apos;re done, use the PullDown Menu to apply the colors to the curent session.</source>
        <translation type="vanished">En el modo de entrada &quot;Control deslizante&quot;, usa los controles deslizantes de la sección de abajo para ajustar los colores. En el modo de entrada &quot;Texto&quot;, puedes introducir directamente los valores del color. El modo &quot;Aleatorizador&quot; hace lo que dice, y &quot;Original de Jolla&quot;, ya lo sabes. Con el modo &quot;Intercambiador&quot; puedes cambiar las definiciones del color.&lt;br/&gt;
Comprueba cómo se verá tu tema en la sala de exposiciones.&lt;br/&gt;
&lt;br/&gt;
Cuando hayas terminado, usa el menú deslizante para aplicar los colores a la sesión actual.</translation>
    </message>
    <message>
        <source>This area allows you to store your created palettes for re-use later. There is one global Cupboard, and one specific for the current Ambience.&lt;br /&gt;
Note that only system-wide Ambiences have a name, custom ones will show as anonymous (for now)</source>
        <translation type="vanished">Este área te permite almacenar las paletas que has creado para un uso posterior. Hay un armario global y uno específico para el ambiente actual.&lt;br/&gt;
Ten en cuenta que sólo los ambientes de todo el sistema tienen un nombre, los personalizados se mostrarán como anónimos (por ahora)</translation>
    </message>
    <message>
        <source>Jolla Ambiences only define four colors, primary (which is black or white depending on Ambience Scheme), secondary which is a little less opaque primary, plus highlight and secondary highlight which are those selected in the Ambience Settings.&lt;br /&gt;
The App lets you edit other colors than these four, but they will not be affected by Ambience changes or Lipstick restarts as they are stored in the dconf database and stay there. This means once applied through the app, they will always stay the same until you change them again in the App. You can use the reset option documented below to get rid of them if necessary.
&lt;br /&gt;
The app frequently gets confused about colors when switching editing modes, applying colors to system, or taking palettes from the Cupboard. If that happens, try reloading from the system, that helps in most cases.&lt;br /&gt;
&lt;br /&gt;
It is possible to create color schemes which make parts of the UI unreadable. Check especially non-obvious areas such as the virtual Keyboard.&lt;br /&gt;
It&apos;s a good idea to store a known-good color scheme in the Cupboard so you can go restore easily.&lt;br /&gt;
&lt;br /&gt;
If you have messed up the colors somehow, use the PullUp menu option to reset everything, or from command line issue:&lt;br /&gt;
&lt;pre&gt;dconf dump /desktop/jolla/theme/color/&lt;/pre&gt;
&lt;pre&gt;dconf reset /desktop/jolla/theme/color/highlightBackground&lt;/pre&gt;
and repeat for all the other colors stored there. &lt;br /&gt;
Changing the Ambience from the System Settings may also help.</source>
        <translation type="vanished">En los ambientes de Jolla sólo se definen cuatro colores, principal (que es negro o blanco según el esquema del ambiente), secundario, que es como el principal pero un poco menos opaco, de realce y de realce secundario que son los colores que se seleccionan en la configuración del ambiente.&lt;br/&gt;
Esta aplicación te permite editar otros colores además de estos cuatro, pero no se verán afectados ante cambios de ambiente o reinicio de lipstick, ya que se almacenan en la base de datos dconf y permanecen allí. Esto significa que una vez aplicados a través de la aplicación, siempre permanecerán igual hasta que los cambies nuevamente en la aplicación. Puedes usar la opción de reinicio documentada a continuación para deshacerte de ellos si es necesario.
&lt;br/&gt;
La aplicación se confunde con frecuencia con los colores al cambiar de modo de edición, aplicar colores al sistema o tomar paletas del armario. Si eso sucede, intenta volver a cargar desde el sistema, eso ayuda en la mayoría de los casos.&lt;br/&gt;
&lt;br/&gt;
Es posible crear esquemas de color que hagan que partes de la interfaz de usuario sean ilegibles. Verifica las áreas especialmente no obvias como el teclado virtual.&lt;br/&gt;
Es una buena idea almacenar una combinación de colores buena en el armario para que puedas restaurarla fácilmente.&lt;br/&gt;
&lt;br/&gt;
Si de alguna manera has estropeado los colores, usa la opción del menú superior para restablecer todo, o desde la línea de comandos: &lt;br/&gt;
&lt;pre&gt;dconf dump /desktop/jolla/theme/color/&lt;/pre&gt;
&lt;pre&gt;dconf reset /desktop/jolla/theme/color/highlightBackground&lt;/pre&gt;
y repite ésto para todos los demás colores almacenados allí.&lt;br/&gt;
También ayuda si cambias de ambiente desde la configuración del sistema.</translation>
    </message>
    <message>
        <source>Version:</source>
        <translation type="vanished">Versión:</translation>
    </message>
    <message>
        <source>Copyright:</source>
        <translation type="vanished">Derechos de autor:</translation>
    </message>
    <message>
        <source>License:</source>
        <translation type="vanished">Licencia:</translation>
    </message>
    <message>
        <source>Source Code:</source>
        <translation type="vanished">Código fuente:</translation>
    </message>
    <message>
        <source>Translations:</source>
        <translation type="vanished">Traducciones:</translation>
    </message>
    <message>
        <source>Jolla Ambiences only define four colors, primary (which is black or white depending on Ambience Scheme), secondary which is a little less opaque primary with some added, plus highlight and secondary highlight which are those selected in the Ambience Settings.&lt;br /&gt;
The App lets you edit other colors than these four, but they will not be affected by Ambience changes or Lipstick restarts as they are stored in the dconf database and stay there. This means once applied through the app, they will always stay the same until you change them again in the App.  You can use the reset option documented below to get rid of them if necessary.
&lt;br /&gt;
The app frequently gets confused about colors when switching editing modes, applying colors to system, or taking palettes from the Cupboard. If that happens, try reloading the colors from the system, that helps in most cases.&lt;br /&gt;
&lt;br /&gt;
It is possible to create color schemes which make parts of the UI unreadable. Check especially non-obvious areas such as the virtual Keyboard.&lt;br /&gt;
It&apos;s a good idea to store a known-good color scheme in the Cupboard so you can go restore easily.&lt;br /&gt;
&lt;br /&gt;
If you have messed up the colors somehow, use the PullUp menu option to reset everything, or from command line issue:&lt;br /&gt;
&lt;pre&gt;dconf dump /desktop/jolla/theme/color/&lt;/pre&gt;
&lt;pre&gt;dconf reset /desktop/jolla/theme/color/highlightBackground&lt;/pre&gt;
and repeat for all the other colors stored there. &lt;br /&gt;
Changing the Ambience from the System Settings may also help.</source>
        <translation type="vanished">Jolla Ambiences only define four colours, primary (which is black or white depending on Ambience Scheme), secondary which is a little less opaque primary with some added, plus highlight and secondary highlight which are those selected in the Ambience Settings.&lt;br /&gt;
The App lets you edit other colours than these four, but they will not be affected by Ambience changes or Lipstick restarts as they are stored in the dconf database and stay there. This means once applied through the app, they will always stay the same until you change them again in the App. You can use the reset option documented below to get rid of them if necessary.
&lt;br /&gt;
The app frequently gets confused about colours when switching editing modes, applying colours to system, or taking palettes from the Cupboard. If that happens, try reloading the colours from the system, that helps in most cases.&lt;br /&gt;
&lt;br /&gt;
It is possible to create colour schemes which make parts of the UI unreadable. Check especially non-obvious areas such as the virtual Keyboard.&lt;br /&gt;
It&apos;s a good idea to store a known-good colour scheme in the Cupboard so you can go restore easily.&lt;br /&gt;
&lt;br /&gt;
If you have messed up the colours somehow, use the PullUp menu option to reset everything, or from command line issue:&lt;br /&gt;
&lt;pre&gt;dconf dump /desktop/jolla/theme/color/&lt;/pre&gt;
&lt;pre&gt;dconf reset /desktop/jolla/theme/color/highlightBackground&lt;/pre&gt;
and repeat for all the other colours stored there. &lt;br /&gt;
Changing the Ambience from the System Settings may also help.</translation>
    </message>
    <message>
        <source>About</source>
        <translation type="vanished">Acerca de</translation>
    </message>
    <message>
        <source>Version: </source>
        <translation type="vanished">Version: </translation>
    </message>
    <message>
        <source>Copyright: </source>
        <translation type="vanished">Copyright: </translation>
    </message>
    <message>
        <source>License: </source>
        <translation type="vanished">Licence: </translation>
    </message>
    <message>
        <source>Source Code: </source>
        <translation type="vanished">Source Code: </translation>
    </message>
    <message>
        <source>General Information</source>
        <translation type="vanished">Información general</translation>
    </message>
    <message>
        <source>About the UI</source>
        <translation type="vanished">Sobre el UI</translation>
    </message>
    <message>
        <source>About Colors</source>
        <translation type="vanished">Sobre Colores</translation>
    </message>
    <message>
        <source>Exporting Ambiences</source>
        <translation type="vanished">Exportación de Ambientes</translation>
    </message>
    <message>
        <source>How it works</source>
        <translation type="vanished">Cómo funciona</translation>
    </message>
    <message>
        <source>Limitations, Tips etc.</source>
        <translation type="vanished">Limitaciones, Vierte etc.</translation>
    </message>
    <message>
        <source>Credits</source>
        <translation type="vanished">Créditos</translation>
    </message>
    <message>
        <source>Handbook</source>
        <comment>User Manual, Application Manual, Handbook, Howto</comment>
        <translation type="vanished">Manual</translation>
    </message>
    <message>
        <source>Chapter %1:</source>
        <comment>chapter in the handbook. argument is a number</comment>
        <translation type="vanished">Capítulo %1:</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="24"/>
        <source>General Information</source>
        <comment>Help Index</comment>
        <translation>Información general</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="25"/>
        <source>About the UI</source>
        <comment>Help Index</comment>
        <translation>Sobre el UI</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="26"/>
        <source>About Colors</source>
        <comment>Help Index</comment>
        <translation>Sobre Colores</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="27"/>
        <source>Exporting Ambiences</source>
        <comment>Help Index</comment>
        <translation>Exportación de ambientes</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="28"/>
        <source>Watcher Daemon</source>
        <comment>Help Index</comment>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="29"/>
        <source>How it works</source>
        <comment>Help Index</comment>
        <translation>Cómo funciona</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="30"/>
        <source>Limitations , Tips etc.</source>
        <comment>Help Index</comment>
        <translation>Limitaciones, consejos, etc.</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="31"/>
        <source>Credits</source>
        <comment>Help Index</comment>
        <translation>Créditos</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="40"/>
        <source>Handbook</source>
        <comment>Help Index</comment>
        <extracomment>User Manual, Application Manual, Handbook, Howto</extracomment>
        <translation>Manual</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="46"/>
        <source>Chapter %1:</source>
        <comment>Help Index Chapter Prefix</comment>
        <extracomment>chapter in the handbook. argument is a number</extracomment>
        <translation>Capítulo %1:</translation>
    </message>
</context>
<context>
    <name>HelpTips</name>
    <message>
        <source>Tips and Caveats</source>
        <translation type="vanished">Consejos y advertencias</translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpTips.qml" line="6"/>
        <source>Tips and Caveats</source>
        <comment>Help Section</comment>
        <translation>Consejos y advertencias</translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpTips.qml" line="8"/>
        <source>
&lt;p&gt;
The app frequently gets confused about colors when switching editing modes, applying colors to system, or taking palettes from the Cupboard. If that happens, try reloading from the system, that helps in most cases.&lt;br /&gt;
&lt;/p&gt;
&lt;p&gt;
It is possible to create color schemes which make parts of the UI unreadable. Check especially non-obvious areas such as the virtual Keyboard.&lt;br /&gt;
It&apos;s a good idea to store a known-good color scheme in the Cupboard so you can go restore easily.
&lt;/p&gt;
&lt;p&gt;
If you have messed up the colors somehow, use the PullUp menu option to reset everything, or from command line issue:&lt;br /&gt;
&lt;pre&gt;dconf dump /desktop/jolla/theme/color/&lt;/pre&gt;
&lt;pre&gt;dconf reset /desktop/jolla/theme/color/highlightBackground&lt;/pre&gt;
and repeat for all the other colors stored there. &lt;br /&gt;
Changing the Ambience from the System Settings may also help.
&lt;/p&gt;
</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>HelpUIMain</name>
    <message>
        <source>The main UI sections</source>
        <translation type="vanished">Las secciones principales de la IGU</translation>
    </message>
    <message>
        <source>&lt;p&gt;The main UI consists of the following:
       &lt;ul&gt;
       &lt;li&gt;The Showroom&lt;/li&gt;
       &lt;li&gt;The Laboratory&lt;/li&gt;
       &lt;li&gt;The Cupboards&lt;/li&gt;
       &lt;li&gt;PullDown and PushUp Menus&lt;/li&gt;
       &lt;/ul&gt;&lt;/p&gt;</source>
        <translation type="vanished">&lt;p&gt;La interfaz de usuario principal consiste en lo siguiente:
       &lt;ul&gt;
       &lt;li&gt;La sala de exposiciones&lt;/li&gt;
       &lt;li&gt;El Laboratorio&lt;/li&gt;
       &lt;li&gt;Los armarios&lt;/li&gt;
       &lt;li&gt;Menús PullDown y PushUp&lt;/li&gt;
       &lt;/ul&gt;&lt;/p&gt;</translation>
    </message>
    <message>
        <source>The Showroom</source>
        <translation type="vanished">La sala de exposiciones</translation>
    </message>
    <message>
        <source>The Laboratory</source>
        <translation type="vanished">El laboratorio</translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpUIMain.qml" line="7"/>
        <source>The main UI sections</source>
        <comment>Help Section</comment>
        <translation>Las secciones principales de la IGU</translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpUIMain.qml" line="9"/>
        <source>&lt;p&gt;The main UI consists of the following:
       &lt;ul&gt;
       &lt;li&gt;The Showroom&lt;/li&gt;
       &lt;li&gt;The Laboratory&lt;/li&gt;
       &lt;li&gt;The Cupboards&lt;/li&gt;
       &lt;li&gt;PullDown Menu&lt;/li&gt;
       &lt;/ul&gt;&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpUIMain.qml" line="17"/>
        <source>The Showroom</source>
        <comment>Help Section</comment>
        <translation>La sala de exposiciones</translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpUIMain.qml" line="26"/>
        <source>&lt;p&gt;The top area on the first page (%1) shows the colors that are selected currently.
This is so you can preview and check how your creations will look.&lt;/p&gt;
&lt;p&gt;
There are two parts: the top one showing text in various combinations of colors, and the bottom one for the UI elements like buttons etc.
Tapping on either of those parts will hide it. When both are hidden, a miniature version will appear.&lt;br /&gt;
To unhide all again tap the section header.
&lt;/p&gt;</source>
        <extracomment>argument is the translation of &quot;Showroom&quot;</extracomment>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpUIMain.qml" line="41"/>
        <source>The Laboratory</source>
        <comment>Help Section</comment>
        <translation>El laboratorio</translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpUIMain.qml" line="44"/>
        <source>&lt;p&gt;The next component is the %1. This is the where you edit the various colors.&lt;/p&gt;
&lt;p&gt;
On the top you find the input mode selector.
There are several ways to do this editing, called Input Modes, explained below.
The selector switches between them.
&lt;ul&gt;</source>
        <extracomment>argument is the translation of &quot;Laboratory&quot;</extracomment>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpUIMain.qml" line="50"/>
        <source>&lt;li&gt;&lt;strong&gt;Slider&lt;/strong&gt; input mode is the main editing mode.&lt;br /&gt;
There is a set of sliders for each color you can edit. Use them to adjust the colors using their red, green, blue (RGB) channels. 
Two slim indicators on top of each slider section shows the current, and the edited color, and the hex value will be shown in the area below them. If you tap that area, the sliders will reset to the value of the unedited color.&lt;br /&gt;
The button with the keyboard symbol opens a dialog where you can put in the value directly.&lt;br /&gt;
The button on the left side opens the Color Selector dialog from Sailfish Silica, in case you want to select a color from there.&lt;br /&gt;
At the bottom below the RGB sliders you will find the Glow editor. This works a little different than the others, allowing you to select the hue of any UI elements that have a glow effect. Below the slider there is a set of buttons that apply either black, white, or the four default colors to the Glow color.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpUIMain.qml" line="58"/>
        <source>&lt;li&gt;&lt;strong&gt;Generators:&lt;/strong&gt; this is a collection of various functions which manipulate all colors of the theme. Some are more useful than others, some are just for fun.
&lt;ul&gt;
&lt;li&gt;&lt;i&gt;Randomizer&lt;/i&gt; shuffles the color values to generate a random theme&lt;/li&gt;
&lt;li&gt;&lt;i&gt;Filters&lt;/i&gt; will change the existing theme colors slightly, e.g. darken or brighten them&lt;/li&gt;
&lt;li&gt;&lt;i&gt;Scheme Generators&lt;/i&gt; is a collection of things which will create a full theme&lt;/li&gt;
&lt;/ul&gt;
&lt;/li&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpUIMain.qml" line="67"/>
        <source>&lt;li&gt;&lt;strong&gt;Swapper/Copier&lt;/strong&gt; is a helper mode which lets you copy the value of one color to another, or swap two values.&lt;/li&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpUIMain.qml" line="68"/>
        <source>&lt;li&gt;&lt;strong&gt;Jolla Original&lt;/strong&gt; you already know, a remake of the element used in the Ambience Settings&lt;/li&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpUIMain.qml" line="83"/>
        <source>The Cupboard</source>
        <comment>Help Section</comment>
        <translation>El armario</translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpUIMain.qml" line="86"/>
        <source>&lt;p&gt;
Cupboards are found to the right of the main page. These allow you to store your created palettes for re-use later.&lt;br /&gt;
The first page is the global Cupboard, where you can store any palette.
The second page is the Cupboard specific for the current Ambience, and its contents will change when the Ambience changes.
&lt;/p&gt;
&lt;p&gt;
Note that only Ambiences installed from a package can have a name, those created from Gallery will show as anonymous (for now)
&lt;/p&gt;
&lt;p&gt;
Per default the shelves are empty (showing all color pots as gray), but tapping the &lt;i&gt;%1&lt;/i&gt; button will save your current palette to that shelf, overwriting any values that may have been there.
The &lt;i&gt;%2&lt;/i&gt; button will load the stored palette and switch back to the main page.
&lt;/p&gt;
</source>
        <extracomment>arguments are the names of the menu entries &quot;Put on Shelf&quot; and &quot;Take to Lab&quot;</extracomment>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpUIMain.qml" line="99"/>
        <source>Put on Shelf</source>
        <translation>Poner en estante</translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpUIMain.qml" line="99"/>
        <source>Take to Lab</source>
        <translation>Llevar al lab</translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpUIMain.qml" line="107"/>
        <source>Menus</source>
        <comment>Help Section</comment>
        <translation>Menús</translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpUIMain.qml" line="115"/>
        <source>&lt;p&gt;
       The PullDown menu contains the most used commands.
       &lt;p&gt;
       &lt;ul&gt;
       &lt;li&gt;Apply: The central command of the app. This will apply the current color theme to the system.&lt;/li&gt;
       &lt;li&gt;Reload: Loads the theme that is currently in use in the system.&lt;/li&gt;
       &lt;li&gt;Export: opens the Export Ambience page. See the &lt;b&gt;Export&lt;/b&gt; chapter for more.&lt;/li&gt;
       &lt;li&gt;Advanced: takes you to a page containing various lesser used, experimental or destructive commands.&lt;/li&gt;
       &lt;/ul&gt;
       &lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Menus</source>
        <translation type="vanished">Menús</translation>
    </message>
    <message>
        <source>The Cupboard</source>
        <translation type="vanished">El armario</translation>
    </message>
</context>
<context>
    <name>LoadAmbience</name>
    <message>
        <location filename="../qml/pages/advanced/LoadAmbience.qml" line="32"/>
        <source>Load</source>
        <translation>Cargar</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/LoadAmbience.qml" line="93"/>
        <source>Load Theme</source>
        <translation>Cargar Tema</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/LoadAmbience.qml" line="93"/>
        <source>Take to Lab</source>
        <translation>Llevar al lab</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/LoadAmbience.qml" line="111"/>
        <source>Here you can load a .ambience (JSON) file.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/LoadAmbience.qml" line="115"/>
        <source>current</source>
        <translation>actual</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/LoadAmbience.qml" line="121"/>
        <source>file data</source>
        <translation>Datos de archivo</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/LoadAmbience.qml" line="127"/>
        <source>File name </source>
        <translation>Nombre del archivo: </translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/LoadAmbience.qml" line="129"/>
        <source>Click to select</source>
        <translation>Pulse para seleccionar</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/LoadAmbience.qml" line="136"/>
        <source>File content</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/LoadAmbience.qml" line="145"/>
        <source>No file loaded</source>
        <translation>¡No se cargó nada!</translation>
    </message>
</context>
<context>
    <name>LootBoxItem</name>
    <message>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="23"/>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="28"/>
        <source>ThemeColor</source>
        <translation>ColorDelTema</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="27"/>
        <source>A Lootbox was delivered!</source>
        <translation>¡Se ha entregado una caja de recompensa!</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="28"/>
        <source>now has more shelves!</source>
        <translation>¡Ahora tienes más estantes!</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="28"/>
        <source>Your persistence has been rewarded.</source>
        <translation>Se ha premiado tu perseverancia.</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="29"/>
        <source>Your persistence has been rewarded!</source>
        <translation>¡Se ha premiado tu perseverancia!</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="39"/>
        <source>Purchase Options</source>
        <translation>Opciones de compra</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="48"/>
        <source>Payment* received!</source>
        <translation>¡Pago* recibido!</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="48"/>
        <source>Buy more shelves</source>
        <translation>Comprar más estantes</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="53"/>
        <source>Using Jolla Shop credentials to buy Storage Lootbox</source>
        <translation>Usando credenciales de la tienda de Jolla para comprar caja de recompensa</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="70"/>
        <source>Thank you for your purchase!&lt;br /&gt;Your extra shelves will be delivered in the next update!</source>
        <translation>¡Gracias por la compra!&lt;br /&gt;¡Tus estantes extra se entregarán en la siguiente actualización!</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="84"/>
        <source>*) No seriously there are no in-app-purchases in this app or Lootboxes in the Jolla Shop. That would be ridiculous.&lt;br /&gt;No funds were transfered.&lt;br /&gt; In fact, nothing really happened just now.&lt;br /&gt;…or did it?</source>
        <translation>*) No, en serio, no hay compras internas en esta aplicación o cajas de recompensa en la Tienda Jolla. Eso sería ridículo. &lt;br/&gt;No se han transferido fondos.&lt;br/&gt;De hecho, realmente no sucedió nada en este momento. &lt;br/&gt;...¿o sí?</translation>
    </message>
    <message>
        <source>*) No seriously there are no in-app-purchases in this app or Lootboxes in the Jolla Shop. That would be ridiculous.&lt;br /&gt;No funds were transfered.&lt;br /&gt; In fact, nothing really happened just now.&lt;br /&gt;...or did it?</source>
        <translation type="vanished">*) No seriously there are no in-app-purchases in this app or Lootboxes in the Jolla Shop. That would be ridiculous.&lt;br /&gt;No funds were transfered.&lt;br /&gt; In fact, nothing really happened just now.&lt;br /&gt;...or did it?</translation>
    </message>
</context>
<context>
    <name>SaveAmbience</name>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="47"/>
        <source>Export Functions</source>
        <translation>Exportar funciones</translation>
    </message>
    <message>
        <source>Here you can export your creation to a .ambience (json) file.&lt;br /&gt;
                      This is not terribly useful at the moment, but you can use that file to build your own ambiences by adding an image, optionally some sounds, and package the whole thing as an RPM.
                     </source>
        <translation type="vanished">Aquí puedes exportar tu creación a un archivo .ambience (json).&lt;br /&gt;
                      Por ahora esto no es muy útil, pero puedes usar el archivo para construir tus propios ambientes añadiendo una imagen, opcionalmente algunos sonidos y empaquetarlo todo en un RPM.
                     </translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="61"/>
        <source>Here you can export your creation to a .ambience (JSON) file, create an installable package from it, and optionally install that on your device.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="63"/>
        <source>To do that, first enter a name for your %1, then tap the &amp;quot;%2&amp;quot; button. After this, you can either edit the file, or generate a package from it.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="63"/>
        <source>Ambience</source>
        <translation>Ambiente</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="83"/>
        <source>Export to File</source>
        <translation>Exportar a archivo</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="88"/>
        <source>Ambience Name</source>
        <translation>Nombre de ambiente</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="90"/>
        <source>A cool Ambience Name</source>
        <translation>Un nombre de ambiente bonito</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="109"/>
        <source>Click to prepare</source>
        <translation></translation>
    </message>
    <message>
        <source>Click to export</source>
        <translation type="vanished">Haz clic para exportar</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="110"/>
        <source>File Name</source>
        <translation>Nombre del archivo</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="196"/>
        <source>Open File</source>
        <translation>Abrir el archivo</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="197"/>
        <source>Launch Builder</source>
        <translation>Comenzar Constructor</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="68"/>
        <source>Disclaimer</source>
        <translation>Descargo de responsabilidad</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="35"/>
        <source>Ambience Package</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="65"/>
        <source>Both the .ambience file as well as any built packages will be stored in the %1 folder. (%2)</source>
        <comment>first argument is the short name of the locatin, second argument is the full path</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="65"/>
        <source>Documents</source>
        <translation>Documentos</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="80"/>
        <source>If you decide to share or otherwise distribute the package make sure you respect copy- and other rights for any content included.</source>
        <translation></translation>
    </message>
    <message>
        <source>Help</source>
        <translation type="obsolete">Ayuda</translation>
    </message>
</context>
<context>
    <name>SaveSlot</name>
    <message>
        <location filename="../qml/components/saver/SaveSlot.qml" line="40"/>
        <location filename="../qml/components/saver/SaveSlot.qml" line="41"/>
        <location filename="../qml/components/saver/SaveSlot.qml" line="42"/>
        <source>Ambience</source>
        <translation>Ambiente</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/SaveSlot.qml" line="40"/>
        <location filename="../qml/components/saver/SaveSlot.qml" line="41"/>
        <location filename="../qml/components/saver/SaveSlot.qml" line="42"/>
        <location filename="../qml/components/saver/SaveSlot.qml" line="44"/>
        <source>Shelf</source>
        <translation>Estante</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/SaveSlot.qml" line="40"/>
        <source>default</source>
        <translation>por defecto</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/SaveSlot.qml" line="41"/>
        <source>night mode</source>
        <translation>Modo nocturno</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/SaveSlot.qml" line="71"/>
        <source>Take to Lab</source>
        <translation>Llevar al lab</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/SaveSlot.qml" line="72"/>
        <source>Put on Shelf</source>
        <translation>Poner en estante</translation>
    </message>
</context>
<context>
    <name>Saver</name>
    <message>
        <location filename="../qml/pages/Saver.qml" line="99"/>
        <source>Global Cupboard</source>
        <translation>Armario global</translation>
    </message>
    <message>
        <location filename="../qml/pages/Saver.qml" line="129"/>
        <source>Clean out this cupboard</source>
        <translation>Limpiar este armario</translation>
    </message>
    <message>
        <location filename="../qml/pages/Saver.qml" line="129"/>
        <source>Spring Clean</source>
        <translation>Limpieza general</translation>
    </message>
</context>
<context>
    <name>SaverPlus</name>
    <message>
        <source>anonymous</source>
        <translation type="vanished">anónimo</translation>
    </message>
    <message>
        <location filename="../qml/pages/SaverPlus.qml" line="14"/>
        <source>User Ambience</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SaverPlus.qml" line="103"/>
        <source>System Ambience</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SaverPlus.qml" line="115"/>
        <source>Ambience Cupboard</source>
        <translation>Armario del ambiente</translation>
    </message>
    <message>
        <location filename="../qml/pages/SaverPlus.qml" line="169"/>
        <source>Clean out this cupboard</source>
        <translation>Limpiar este armario</translation>
    </message>
    <message>
        <location filename="../qml/pages/SaverPlus.qml" line="169"/>
        <source>Spring Clean</source>
        <translation>Limpieza general</translation>
    </message>
</context>
<context>
    <name>ShowRoom</name>
    <message>
        <source>A very long line showing Text in </source>
        <translation type="vanished">Una línea muy larga está mostrando texto en </translation>
    </message>
    <message>
        <source>Color</source>
        <translation type="vanished">Colour</translation>
    </message>
    <message>
        <source>Primary Color</source>
        <translation type="vanished">Color principal</translation>
    </message>
    <message>
        <source>Secondary Color</source>
        <translation type="vanished">Color secundario</translation>
    </message>
    <message>
        <source>Highlight Color</source>
        <translation type="vanished">Color de realce</translation>
    </message>
    <message>
        <source>Secondary Highlight Color</source>
        <translation type="vanished">Color secundario de realce</translation>
    </message>
    <message>
        <source>Error Color</source>
        <translation type="vanished">Color de error</translation>
    </message>
    <message>
        <source>Background Color</source>
        <translation type="vanished">Color de fondo</translation>
    </message>
    <message>
        <source>four kinds of background overlay opacities and colors</source>
        <translation type="vanished">four kinds of background overlay opacities and colours</translation>
    </message>
    <message>
        <source>Text</source>
        <translation type="vanished">Texto</translation>
    </message>
    <message>
        <source>Highlight Background Color</source>
        <translation type="vanished">Color de fondo de realce</translation>
    </message>
    <message>
        <source>Progress Bar Demo</source>
        <translation type="vanished">Demo de barra de progreso</translation>
    </message>
    <message>
        <source>Tap to restart Demos</source>
        <translation type="vanished">Toca para reiniciar Demos</translation>
    </message>
    <message>
        <source>Remorse Item Demo</source>
        <translation type="vanished">Demo de barra de cancelación</translation>
    </message>
    <message>
        <source>MenuItem</source>
        <translation type="vanished">Elemento del menú</translation>
    </message>
    <message>
        <source>selected</source>
        <translation type="vanished">seleccionado</translation>
    </message>
    <message>
        <source>disabled</source>
        <translation type="vanished">desactivado</translation>
    </message>
    <message>
        <source>Button</source>
        <translation type="vanished">Botón</translation>
    </message>
    <message>
        <location filename="../qml/components/ShowRoom.qml" line="16"/>
        <source>Text Elements</source>
        <translation>Elementos de texto</translation>
    </message>
    <message>
        <location filename="../qml/components/ShowRoom.qml" line="24"/>
        <source>UI Elements</source>
        <translation>Elementos de la UI</translation>
    </message>
</context>
<context>
    <name>ShowRoomMini</name>
    <message>
        <location filename="../qml/components/ShowRoomMini.qml" line="21"/>
        <source>Mini</source>
        <comment>small showroom</comment>
        <translation>Mini</translation>
    </message>
    <message>
        <location filename="../qml/components/ShowRoomMini.qml" line="21"/>
        <source>Showroom</source>
        <translation>Sala de exposiciones</translation>
    </message>
</context>
<context>
    <name>ShowRoomText</name>
    <message>
        <source>A very long line showing Text in </source>
        <translation type="vanished">Una línea muy larga está mostrando texto en </translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="17"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="29"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="38"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="63"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="72"/>
        <source>Primary Color</source>
        <translation>Color principal</translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="17"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="18"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="19"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="20"/>
        <source>A very long line showing Text in</source>
        <translation>Una línea muy larga está mostrando texto en</translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="18"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="30"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="39"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="64"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="73"/>
        <source>Secondary Color</source>
        <translation>Color secundario</translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="19"/>
        <source>Highlight Color</source>
        <translation>Color de realce</translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="20"/>
        <source>Secondary Highlight Color</source>
        <translation>Color secundario de realce</translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="63"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="64"/>
        <source>Cover Background Color</source>
        <translation>Color de fondo de la cabecera</translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="72"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="73"/>
        <source>Wallpaper Overlay Color</source>
        <translation></translation>
    </message>
    <message>
        <source>Error Color</source>
        <translation type="vanished">Color de error</translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="29"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="30"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="38"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="39"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="63"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="64"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="72"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="73"/>
        <source>Text</source>
        <translation>Texto</translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="29"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="30"/>
        <source>Highlight Background Color</source>
        <translation>Color de fondo de realce</translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="38"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="39"/>
        <source>Overlay Background Color</source>
        <translation>Color de fondo de la superposición</translation>
    </message>
    <message>
        <source>Dim Highlight Color</source>
        <translation type="vanished">Color oscuro de realce</translation>
    </message>
</context>
<context>
    <name>ShowRoomUI</name>
    <message>
        <location filename="../qml/components/showroom/ShowRoomUI.qml" line="32"/>
        <source>Progress Bar Demo</source>
        <translation>Demo de barra de progreso</translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomUI.qml" line="35"/>
        <source>Remorse Item Demo</source>
        <translation>Demo de barra de cancelación</translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomUI.qml" line="90"/>
        <location filename="../qml/components/showroom/ShowRoomUI.qml" line="91"/>
        <location filename="../qml/components/showroom/ShowRoomUI.qml" line="92"/>
        <source>MenuItem</source>
        <translation>Elemento del menú</translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomUI.qml" line="92"/>
        <source>selected</source>
        <translation>seleccionado</translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomUI.qml" line="91"/>
        <source>disabled</source>
        <translation>desactivado</translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomUI.qml" line="124"/>
        <location filename="../qml/components/showroom/ShowRoomUI.qml" line="125"/>
        <source>Button</source>
        <translation>Botón</translation>
    </message>
</context>
<context>
    <name>TransparencyEditor</name>
    <message>
        <location filename="../qml/pages/advanced/TransparencyEditor.qml" line="41"/>
        <source>Edit Transparency</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/TransparencyEditor.qml" line="49"/>
        <source>This will edit the alpha channel of the color. Note that in the Sailfish UI many elements use their own transparency values and are not affected by the alpha channel of the color. Remorse Timers are one such example.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/TransparencyEditor.qml" line="78"/>
        <source>Highlight Background Opacity</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/TransparencyEditor.qml" line="79"/>
        <source>This is used e.g. for Pulley Menu background.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/TransparencyEditor.qml" line="81"/>
        <source>Wallpaper Overlay Opacity</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/TransparencyEditor.qml" line="83"/>
        <source>Cover Overlay Opacity</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/TransparencyEditor.qml" line="84"/>
        <source>Transparency of application covers.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/TransparencyEditor.qml" line="90"/>
        <source>Color Alpha Channel</source>
        <translation>Canal alfa de color</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/TransparencyEditor.qml" line="91"/>
        <source>Here you can edit Alpha channels for colors that have one.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/TransparencyEditor.qml" line="92"/>
        <source>Secondary Color</source>
        <translation>Color secundario</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/TransparencyEditor.qml" line="94"/>
        <source>Secondary Highlight Color</source>
        <translation>Color secundario de realce</translation>
    </message>
    <message>
        <source>Highlight Background Color</source>
        <translation type="vanished">Color de fondo de realce</translation>
    </message>
</context>
</TS>
