<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr">
<context>
    <name>AdvancedPage</name>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="17"/>
        <source>Advanced Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="26"/>
        <source>System Functions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="29"/>
        <location filename="../qml/pages/AdvancedPage.qml" line="35"/>
        <source>Restart %1</source>
        <comment>restart an application, as in &apos;restart service foo&apos;</comment>
        <translation type="unfinished">Redémarrer %1</translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="30"/>
        <source>Restart the %1 service. This will close all apps and relaunch the %2</source>
        <comment>arguments are &apos;Lipstick&apos; and &apos;Home Screen&apos;, one id the application name, one is the commonly used name for the same.</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="30"/>
        <location filename="../qml/pages/AdvancedPage.qml" line="57"/>
        <source>Lipstick</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="30"/>
        <source>Home Screen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="31"/>
        <location filename="../qml/pages/AdvancedPage.qml" line="37"/>
        <source>Restarting</source>
        <translation type="unfinished">Redémarrage</translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="36"/>
        <source>Restart the %1 service. This sometimes fixes color problems</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="36"/>
        <location filename="../qml/pages/AdvancedPage.qml" line="78"/>
        <source>Ambience</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="49"/>
        <source>Config Management</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="51"/>
        <source>Reset Config</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="52"/>
        <source>Reset all color-related configuration values</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="53"/>
        <location filename="../qml/pages/AdvancedPage.qml" line="58"/>
        <source>Resetting</source>
        <translation type="unfinished">Réinitialisation</translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="56"/>
        <source>Reset Config and Restart</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="57"/>
        <source>Reset all color-related configuration values and restart %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="75"/>
        <source>Background Service</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="77"/>
        <source>Set up daemon</source>
        <translation type="unfinished">Configurer le démon</translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="78"/>
        <source>Background service which watches for %1 changes and applies actions.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="81"/>
        <source>Import/Export</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="84"/>
        <source>Load Ambience File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/AdvancedPage.qml" line="85"/>
        <source>Load a color scheme from a .ambience file.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AppInfoSingleton</name>
    <message>
        <source>ThemeColor</source>
        <translation type="vanished">ThemeColour</translation>
    </message>
    <message>
        <source>ThemeColor® RPM Builder™</source>
        <translation type="vanished">ThemeColour® RPM Builder™</translation>
    </message>
    <message>
        <location filename="../qml/components/AppInfoSingleton.qml" line="21"/>
        <source>ThemeColor</source>
        <comment>this will also be used for the app icon</comment>
        <translation>CouleurDuThème</translation>
    </message>
    <message>
        <location filename="../qml/components/AppInfoSingleton.qml" line="22"/>
        <source>ThemeColor® RPM Builder™</source>
        <comment>this will also be used for the app icon</comment>
        <translation>CouleurDuThème® Constructeur RPM™</translation>
    </message>
    <message>
        <location filename="../qml/components/AppInfoSingleton.qml" line="34"/>
        <source>English</source>
        <comment>Language name in local language. So, if local language is French, &apos;German&apos; should be &apos;allemande&apos;, not &apos;Deutsch&apos;.</comment>
        <translation>anglais</translation>
    </message>
    <message>
        <location filename="../qml/components/AppInfoSingleton.qml" line="31"/>
        <source>German</source>
        <comment>Language name in local language. So, if local language is French, &apos;German&apos; should be &apos;allemande&apos;, not &apos;Deutsch&apos;.</comment>
        <translation>allemand</translation>
    </message>
    <message>
        <location filename="../qml/components/AppInfoSingleton.qml" line="44"/>
        <source>Norwegian Bokmål</source>
        <comment>Language name in local language. So, if local language is French, &apos;German&apos; should be &apos;allemande&apos;, not &apos;Deutsch&apos;.</comment>
        <translation>norvégien (bokmål)</translation>
    </message>
    <message>
        <location filename="../qml/components/AppInfoSingleton.qml" line="37"/>
        <source>Spanish</source>
        <comment>Language name in local language. So, if local language is French, &apos;German&apos; should be &apos;allemande&apos;, not &apos;Deutsch&apos;.</comment>
        <translation>espagnol</translation>
    </message>
    <message>
        <location filename="../qml/components/AppInfoSingleton.qml" line="41"/>
        <source>French</source>
        <comment>Language name in local language. So, if local language is French, &apos;German&apos; should be &apos;allemande&apos;, not &apos;Deutsch&apos;.</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/AppInfoSingleton.qml" line="47"/>
        <source>Swedish</source>
        <comment>Language name in local language. So, if local language is French, &apos;German&apos; should be &apos;allemande&apos;, not &apos;Deutsch&apos;.</comment>
        <translation>suédois</translation>
    </message>
    <message>
        <location filename="../qml/components/AppInfoSingleton.qml" line="50"/>
        <source>Simplified Chinese</source>
        <comment>Language name in local language. So, if local language is French, &apos;German&apos; should be &apos;allemande&apos;, not &apos;Deutsch&apos;.</comment>
        <translation>chinois (simplifié)</translation>
    </message>
</context>
<context>
    <name>ColorField</name>
    <message>
        <source>specify RGB or aRGB value, e.g.</source>
        <translation type="vanished">spécifiez la valeur RVB ou RVBa, par ex.</translation>
    </message>
    <message>
        <location filename="../qml/components/controls/ColorField.qml" line="9"/>
        <source>an %1 value</source>
        <comment>parameter is either RGB or aRGB</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/controls/ColorField.qml" line="10"/>
        <source>(the # is optional)</source>
        <translation>(le # est optionnel)</translation>
    </message>
    <message>
        <location filename="../qml/components/controls/ColorField.qml" line="10"/>
        <source>e.g.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ColorFilters</name>
    <message>
        <source>Tint</source>
        <translation type="vanished">Tint</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorFilters.qml" line="17"/>
        <source>Red</source>
        <translation>Rouge</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorFilters.qml" line="17"/>
        <source>Tint %1</source>
        <translation>Teinte %1</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorFilters.qml" line="25"/>
        <source>Darken</source>
        <translation>Assombrir</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorFilters.qml" line="33"/>
        <source>Brighten</source>
        <translation>Éclaircir</translation>
    </message>
</context>
<context>
    <name>ColorGenerators</name>
    <message>
        <location filename="../qml/components/ColorGenerators.qml" line="10"/>
        <source>Randomizer</source>
        <translation>Randomiseur</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorGenerators.qml" line="12"/>
        <source>Filters</source>
        <translation>Filtres</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorGenerators.qml" line="14"/>
        <source>Scheme Generators</source>
        <translation>Générateurs de schémas</translation>
    </message>
</context>
<context>
    <name>ColorRandomizer</name>
    <message>
        <location filename="../qml/components/generators/ColorRandomizer.qml" line="16"/>
        <source>Random</source>
        <translation>Aléatoire</translation>
    </message>
    <message>
        <source>Colors</source>
        <translation type="vanished">Colours</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorRandomizer.qml" line="21"/>
        <location filename="../qml/components/generators/ColorRandomizer.qml" line="35"/>
        <location filename="../qml/components/generators/ColorRandomizer.qml" line="45"/>
        <source>Generated</source>
        <translation>Généré</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorRandomizer.qml" line="26"/>
        <source>Bright</source>
        <translation>Clair</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorRandomizer.qml" line="26"/>
        <source>Dark</source>
        <translation>Sombre</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorRandomizer.qml" line="40"/>
        <source>Gray</source>
        <translation>Gris</translation>
    </message>
</context>
<context>
    <name>ColorSchemeGenerator</name>
    <message>
        <source>Solarize</source>
        <translation type="vanished">Solarise</translation>
    </message>
    <message>
        <source>%1 Theme</source>
        <comment>parameter is name for a theme, e.g. night, day, random...</comment>
        <translation type="vanished">%1 Theme</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="13"/>
        <source>Solarize</source>
        <comment>as in &apos;solarized&apos; color scheme</comment>
        <translation>Solariser</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="13"/>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="28"/>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="35"/>
        <source>%1 Theme</source>
        <comment>parameter is name for a theme, e.g. night, day, random... you might just use the parameter and not translate theme at all.</comment>
        <translation>%1</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="20"/>
        <source>Generate</source>
        <translation>Générer</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="20"/>
        <source>from</source>
        <translation>depuis</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="20"/>
        <source>Highlight</source>
        <translation>Surbrillance</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="28"/>
        <source>Night</source>
        <translation>Nuit</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="48"/>
        <source>#%1/%2</source>
        <comment>for lists, as in &apos;number one of four&apos; is #1/4</comment>
        <translation>#%1/%2</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation type="vanished">Theme</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="35"/>
        <source>Summer</source>
        <translation>Été</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="48"/>
        <source>%1 Color Blindness</source>
        <comment>%1 is replaced with the variant of color blindness, as in &apos;red-green color-blindness&apos;</comment>
        <translation>Daltonisme %1</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="48"/>
        <source>R/G</source>
        <comment>abbreviation for red-green color blindness</comment>
        <translation>R/V</translation>
    </message>
    <message>
        <source> #%1/%2</source>
        <comment>for lists, as in &apos;number one of four&apos; is #1/4</comment>
        <translation type="vanished"> #%1/%2</translation>
    </message>
    <message>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="16"/>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="23"/>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="31"/>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="38"/>
        <location filename="../qml/components/generators/ColorSchemeGenerator.qml" line="54"/>
        <source>Applying</source>
        <translation>Application</translation>
    </message>
</context>
<context>
    <name>ColorSelectors</name>
    <message>
        <source>Color</source>
        <translation type="vanished">Colour</translation>
    </message>
    <message>
        <source>Primary Color</source>
        <translation type="vanished">Primary Colour</translation>
    </message>
    <message>
        <source>Secondary Color</source>
        <translation type="vanished">Secondary Colour</translation>
    </message>
    <message>
        <source>Highlight Color</source>
        <translation type="vanished">Highlight Colour</translation>
    </message>
    <message>
        <source>Secondary Highlight Color</source>
        <translation type="vanished">Secondary Highlight Colour</translation>
    </message>
</context>
<context>
    <name>ColorSlider</name>
    <message>
        <location filename="../qml/components/controls/ColorSlider.qml" line="58"/>
        <source>Adjust sliders, tap to reset</source>
        <translation>Ajustez les curseurs, touchez pour réinitialiser</translation>
    </message>
</context>
<context>
    <name>ColorSliders</name>
    <message>
        <source>Color</source>
        <translation type="vanished">Colour</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSliders.qml" line="10"/>
        <source>Primary Color</source>
        <translation>Couleur principale</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSliders.qml" line="15"/>
        <source>Secondary Color</source>
        <translation>Couleur secondaire</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSliders.qml" line="20"/>
        <source>Highlight Color</source>
        <translation>Couleur de surbrillance</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSliders.qml" line="25"/>
        <source>Secondary Highlight Color</source>
        <translation>Couleur de surbrillance secondaire</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSliders.qml" line="30"/>
        <source>Background Highlight Color</source>
        <translation>Couleur de surbrillance d’arrière-plan</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSliders.qml" line="35"/>
        <source>Dim Highlight Color</source>
        <translation>Couleur de surbrillance sombre</translation>
    </message>
    <message>
        <source>Highlight Background Color</source>
        <translation type="vanished">Highlight Background Colour</translation>
    </message>
    <message>
        <source>Highlight Dimmer Color</source>
        <translation type="vanished">Highlight Dimmer Colour</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSliders.qml" line="40"/>
        <source>Background Glow Color</source>
        <translation>Couleur de la lueur d’arrière-plan</translation>
    </message>
</context>
<context>
    <name>ColorSwapper</name>
    <message>
        <location filename="../qml/components/ColorSwapper.qml" line="74"/>
        <source>Copy</source>
        <translation>Copier</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSwapper.qml" line="74"/>
        <source>Swap</source>
        <translation>Permuter</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSwapper.qml" line="95"/>
        <location filename="../qml/components/ColorSwapper.qml" line="122"/>
        <source>Primary Color</source>
        <translation>Couleur principale</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSwapper.qml" line="96"/>
        <location filename="../qml/components/ColorSwapper.qml" line="123"/>
        <source>Secondary Color</source>
        <translation>Couleur secondaire</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSwapper.qml" line="97"/>
        <location filename="../qml/components/ColorSwapper.qml" line="124"/>
        <source>Highlight Color</source>
        <translation>Couleur de surbrillance</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSwapper.qml" line="98"/>
        <location filename="../qml/components/ColorSwapper.qml" line="125"/>
        <source>Secondary Highlight Color</source>
        <translation>Couleur de surbrillance secondaire</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSwapper.qml" line="99"/>
        <location filename="../qml/components/ColorSwapper.qml" line="126"/>
        <source>Background Highlight Color</source>
        <translation>Couleur de surbrillance d’arrière-plan</translation>
    </message>
    <message>
        <location filename="../qml/components/ColorSwapper.qml" line="100"/>
        <location filename="../qml/components/ColorSwapper.qml" line="127"/>
        <source>Dim Highlight Color</source>
        <translation>Couleur de surbrillance sombre</translation>
    </message>
    <message>
        <source>Highlight Background Color</source>
        <translation type="vanished">Highlight Background Colour</translation>
    </message>
    <message>
        <source>Highlight Dimmer Color</source>
        <translation type="vanished">Highlight Dimmer Colour</translation>
    </message>
    <message>
        <source>Color</source>
        <translation type="obsolete">Colour</translation>
    </message>
</context>
<context>
    <name>ColorTextInput</name>
    <message>
        <source>Color input</source>
        <translation type="vanished">Saisie de couleur</translation>
    </message>
    <message>
        <source>Input value, tap to reset</source>
        <translation type="vanished">Saisir la valeur, effleurer pour réinitialiser</translation>
    </message>
</context>
<context>
    <name>ColorTextInputDialog</name>
    <message>
        <location filename="../qml/components/controls/ColorTextInputDialog.qml" line="11"/>
        <location filename="../qml/components/controls/ColorTextInputDialog.qml" line="24"/>
        <source>Color input</source>
        <translation type="unfinished">Saisie de couleur</translation>
    </message>
</context>
<context>
    <name>ColorTextInputs</name>
    <message>
        <source>Primary Color</source>
        <translation type="vanished">Couleur principale</translation>
    </message>
    <message>
        <source>Secondary Color</source>
        <translation type="vanished">Couleur secondaire</translation>
    </message>
    <message>
        <source>Highlight Color</source>
        <translation type="vanished">Couleur de surbrillance</translation>
    </message>
    <message>
        <source>Secondary Highlight Color</source>
        <translation type="vanished">Couleur de surbrillance secondaire</translation>
    </message>
    <message>
        <source>Background Highlight Color</source>
        <translation type="vanished">Couleur de surbrillance d’arrière-plan</translation>
    </message>
    <message>
        <source>Dim Highlight Color</source>
        <translation type="vanished">Couleur de surbrillance sombre</translation>
    </message>
    <message>
        <source>Highlight Background Color</source>
        <translation type="vanished">Highlight Background Colour</translation>
    </message>
    <message>
        <source>Highlight Dimmer Color</source>
        <translation type="vanished">Highlight Dimmer Colour</translation>
    </message>
    <message>
        <source>Background Glow Color</source>
        <translation type="vanished">Couleur de la lueur d’arrière-plan</translation>
    </message>
</context>
<context>
    <name>CoverPage</name>
    <message>
        <source>ThemeColor</source>
        <translation type="vanished">CouleurDuThème</translation>
    </message>
</context>
<context>
    <name>DaemonSettings</name>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="105"/>
        <source>Daemon Settings</source>
        <translation>Paramètres du démon</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="115"/>
        <source>Enable Watcher daemon</source>
        <translation>Activer le démon Watcher</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="110"/>
        <source>Watches for %1 changes and applies actions defined below.</source>
        <translation>Surveille les changements de %1 et applique les actions définies ci-dessous.</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="116"/>
        <source>Daemon is </source>
        <translation>Le démon est </translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="116"/>
        <source>active</source>
        <translation>actif</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="116"/>
        <source>inactive</source>
        <translation>inactif</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="134"/>
        <source>Actions</source>
        <translation>Actions</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="140"/>
        <source>Select what happens when an %1 change is detected</source>
        <translation>Sélectionnez ce qui se passe quand un changement %1 est détecté</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="145"/>
        <source>launch %1</source>
        <translation>lancer %1</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="146"/>
        <source>just opens the app</source>
        <translation>ouvre simplement l’application</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="156"/>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="158"/>
        <source>Warning:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="157"/>
        <source>The features below are experimental and do not work reliably.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="165"/>
        <source>apply Top theme</source>
        <translation>appliquer le thème par défaut</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="166"/>
        <source>applies theme from the top %1 shelf</source>
        <translation>applique le thème de l’étagère supérieure %1</translation>
    </message>
    <message>
        <source>Define what happens when an %1 change is detected</source>
        <translation type="vanished">Define what happens when an %1 change is detected</translation>
    </message>
    <message>
        <source>launch Application</source>
        <translation type="vanished">launch Application</translation>
    </message>
    <message>
        <source>just opens %1</source>
        <translation type="vanished">just opens %1</translation>
    </message>
    <message>
        <source>apply Custom theme</source>
        <translation type="vanished">apply Custom theme</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="173"/>
        <source>apply Night theme</source>
        <translation>appliquer le thème de nuit</translation>
    </message>
    <message>
        <source>ThemeColor</source>
        <translation type="vanished">ThemeColor</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="110"/>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="140"/>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="166"/>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="174"/>
        <source>Ambience</source>
        <translation>Ambiance</translation>
    </message>
    <message>
        <source>taken from the top %1 shelf</source>
        <translation type="vanished">taken from the top %1 shelf</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="174"/>
        <source>taken from the second %1 shelf</source>
        <translation>provenant de la deuxième étagère %1</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="187"/>
        <source>Night begins:</source>
        <translation>La nuit commence :</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/DaemonSettings.qml" line="201"/>
        <source>Night ends:</source>
        <translation>La nuit se termine :</translation>
    </message>
</context>
<context>
    <name>FakeVKB</name>
    <message>
        <source>QWErty0</source>
        <comment>characters shown on keyboard simulator. translate if you want non-ascii characters to show</comment>
        <translation type="vanished">QWErty0</translation>
    </message>
    <message>
        <source>@#$%^&amp;9</source>
        <comment>characters shown on keyboard simulator. translate if you want non-ascii characters to show</comment>
        <translation type="vanished">@#$%^&amp;9</translation>
    </message>
</context>
<context>
    <name>FirstPage</name>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="29"/>
        <source>Adjust Theme Colors</source>
        <translation>Ajuster les couleurs du thème</translation>
    </message>
    <message>
        <source>Current Color Model:</source>
        <translation type="vanished">Current Colour Model:</translation>
    </message>
    <message>
        <source>Primary Color</source>
        <translation type="vanished">Primary Colour</translation>
    </message>
    <message>
        <source>Secondary Color</source>
        <translation type="vanished">Secondary Colour</translation>
    </message>
    <message>
        <source>Highlight Color</source>
        <translation type="vanished">Highlight Colour</translation>
    </message>
    <message>
        <source>Secondary Highlight Color</source>
        <translation type="vanished">Secondary Highlight Colour</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="47"/>
        <source>Showroom</source>
        <translation>Salle d’exposition</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="98"/>
        <source>Laboratory</source>
        <translation>Laboratoire</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="112"/>
        <source>Input Mode:</source>
        <translation>Mode de saisie :</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="113"/>
        <source>Tap to switch</source>
        <translation>Appuyer pour changer</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="176"/>
        <source>Advanced…</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>gray</source>
        <translation type="vanished">grey</translation>
    </message>
    <message>
        <source>Apply colors to system</source>
        <translation type="vanished">Apply colours to system</translation>
    </message>
    <message>
        <source>Reload colors from system</source>
        <translation type="vanished">Reload colors from system</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="122"/>
        <source>Swapper/Copier</source>
        <translation>Permuteur/copieur</translation>
    </message>
    <message>
        <source>Compute all Colors from Highlight</source>
        <translation type="vanished">Compute all Colors from Highlight</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="183"/>
        <source>Applying</source>
        <translation>Application</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="182"/>
        <source>Apply Colors to System</source>
        <translation>Appliquer les couleurs au système</translation>
    </message>
    <message>
        <source>Set up daemon</source>
        <translation type="vanished">Configurer le démon</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="185"/>
        <source>Reload Colors from System</source>
        <translation>Recharger les couleurs du système</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="123"/>
        <source>Generators</source>
        <translation>Générateurs</translation>
    </message>
    <message>
        <source>User Manual</source>
        <translation type="vanished">Handbook</translation>
    </message>
    <message>
        <source>Reload Colors from current Theme</source>
        <translation type="vanished">Reload Colours from current Theme</translation>
    </message>
    <message>
        <source>Reload Colors from System Config</source>
        <translation type="vanished">Reload Colours from System Config</translation>
    </message>
    <message>
        <source>Experimental or dangerous actions</source>
        <translation type="vanished">Actions expérimentales ou dangereuses</translation>
    </message>
    <message>
        <source>Export to Ambience file</source>
        <translation type="vanished">Export to Ambience file</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="179"/>
        <source>Export to Ambience package</source>
        <translation>Exporter vers le paquet Ambiance</translation>
    </message>
    <message>
        <source>Edit Transparency</source>
        <translation type="vanished">Edit Transparency</translation>
    </message>
    <message>
        <source>(not implemented)</source>
        <translation type="vanished">(not implemented)</translation>
    </message>
    <message>
        <source>Save Theme to current Ambience</source>
        <translation type="vanished">Save Theme to current Ambience</translation>
    </message>
    <message>
        <source>Saving</source>
        <translation type="vanished">Saving</translation>
    </message>
    <message>
        <source>Resetting</source>
        <translation type="vanished">Réinitialisation</translation>
    </message>
    <message>
        <source>Restart %1</source>
        <comment>restart an application, as in &apos;restart service foo&apos;</comment>
        <translation type="vanished">Redémarrer %1</translation>
    </message>
    <message>
        <source>Restarting</source>
        <translation type="vanished">Redémarrage</translation>
    </message>
    <message>
        <source>Saving...</source>
        <translation type="vanished">Saving…</translation>
    </message>
    <message>
        <source>Reset all values and restart</source>
        <translation type="vanished">Réinitialiser toutes les valeurs et redémarrer</translation>
    </message>
    <message>
        <source>Reset nonstandard values</source>
        <translation type="vanished">Remise à zéro des valeurs non standard</translation>
    </message>
    <message>
        <source>Restart</source>
        <translation type="vanished">Restart</translation>
    </message>
    <message>
        <source>Restart Lipstick</source>
        <translation type="vanished">Restart Lipstick</translation>
    </message>
    <message>
        <source>Help</source>
        <translation type="vanished">Help</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="121"/>
        <source>Sliders</source>
        <translation>Curseurs</translation>
    </message>
    <message>
        <source>Text</source>
        <translation type="vanished">Texte</translation>
    </message>
    <message>
        <source>Swapper</source>
        <translation type="vanished">Swapper</translation>
    </message>
    <message>
        <source>Randomizer</source>
        <translation type="vanished">Randomizer</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="124"/>
        <source>Jolla Original</source>
        <translation>Original Jolla</translation>
    </message>
    <message>
        <location filename="../qml/pages/FirstPage.qml" line="173"/>
        <source>Handbook</source>
        <comment>User Manual, Application Manual, Handbook, Howto</comment>
        <translation>Guide d’utilisation</translation>
    </message>
    <message>
        <source>Open Ambience Settings</source>
        <translation type="vanished">Open Ambience Settings</translation>
    </message>
    <message>
        <source>Apply Colors to current Theme</source>
        <translation type="vanished">Apply colours to current Theme</translation>
    </message>
    <message>
        <source>Reload Colors</source>
        <translation type="vanished">Reload Colours</translation>
    </message>
    <message>
        <source>Color</source>
        <translation type="vanished">Colour</translation>
    </message>
    <message>
        <source>Reset Colors</source>
        <translation type="vanished">Reset Colours</translation>
    </message>
</context>
<context>
    <name>HelpColors</name>
    <message>
        <source>The four basic Colors</source>
        <translation type="vanished">The four basic Colours</translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpColors.qml" line="6"/>
        <source>The four basic Colors</source>
        <comment>Help Section</comment>
        <translation>Les quatre couleurs de base</translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpColors.qml" line="8"/>
        <source>&lt;p&gt;Jolla Ambiences define four colors which are used for the various text elements around the UI.:&lt;br /&gt;</source>
        <translation>&lt;p&gt;Les ambiances Jolla définissent quatre couleurs qui sont utilisées pour les différents éléments de texte autour de l’interface utilisateur :&lt;br /&gt;</translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpColors.qml" line="10"/>
        <source>primary, for most interactive elements (it is black or white depending on Ambience Scheme)</source>
        <translation>primaire, pour la plupart des éléments interactifs (c’est noir ou blanc selon le schéma d’ambiance)</translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpColors.qml" line="11"/>
        <source>secondary, per default derived from primary by making it a little less opaque, is used for inactive elements, and most text</source>
        <translation>secondaire, par défaut dérivé de la primaire en la rendant un peu moins opaque, est utilisée pour les éléments inactifs et la plupart des textes</translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpColors.qml" line="12"/>
        <source>highlight, which is what you selected in the Ambience Settings, colors active or activated elements</source>
        <translation>la surbrillance, qui est ce que vous avez sélectionné dans les paramètres d’ambiance, des couleurs des éléments actifs ou activés</translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpColors.qml" line="13"/>
        <source>secondary highlight, derived through some magic from the highlight color.</source>
        <translation>la surbrillance secondaire, dérivée par magie de la couleur de surbrillance.</translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpColors.qml" line="16"/>
        <source>Additional Colors</source>
        <comment>Help Section</comment>
        <translation>Couleurs supplémentaires</translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpColors.qml" line="19"/>
        <source>
&lt;p&gt;
Other colors used:&lt;br /&gt;
&lt;ul&gt;
&lt;li&gt;Background and HighlightBackground style the background of Menus, Buttons and the like.&lt;/li&gt;
&lt;li&gt;DimmerHighlightBackground does the same, but for darker areas or high contrast elements.&lt;/li&gt;
&lt;li&gt;GlowColor is used for glassy things like Switches, forward/back indicators etc.&lt;/li&gt;
&lt;li&gt;CoverOverlayColor is/was used for Application Covers, but see note below.&lt;/li&gt;
&lt;/ul&gt;
&lt;/p&gt;
&lt;p&gt;
For more information about colors and other aspects of the GUI, see the official &lt;a href=&quot;https://sailfishos.org/develop/docs/silica/qml-sailfishsilica-sailfish-silica-theme.html&quot;&gt;Theme Documentation&lt;/a&gt;
&lt;/p&gt;
&lt;p&gt;
&lt;i&gt;
It must be said that while all those &lt;b&gt;Theme&lt;/b&gt; colors are still defined, as SailfishOS progresses, with UI developers continue to use QML&apos;s&lt;b&gt;palette&lt;/b&gt; property directly.
As we only deal with Theme colors, this unfortunately means more and more UI components stop reacting to those changes.
&lt;/i&gt;
&lt;/p&gt;
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Additional Colors</source>
        <translation type="vanished">Additional Colours</translation>
    </message>
    <message>
        <source>
&lt;p&gt;
Other colors used:&lt;br /&gt;
&lt;ul&gt;
&lt;li&gt;Background and HighlightBackground style the background of Menus, Buttons and the like.&lt;/li&gt;
&lt;li&gt;DimmerHighlightBackground does the same, but for darker areas or high contrast elements.&lt;/li&gt;
&lt;li&gt;GlowColor is used for glassy things like Switches, forward/back indicators etc.&lt;/li&gt;
&lt;/ul&gt;
&lt;/p&gt;
&lt;p&gt;
For more information about colors and other aspects of the GUI, see the official &lt;a href=&quot;https://sailfishos.org/develop/docs/silica/qml-sailfishsilica-sailfish-silica-theme.html&quot;&gt;Theme Documentation&lt;/a&gt;
&lt;/p&gt;
</source>
        <translation type="vanished">
&lt;p&gt;
Autres couleurs utilisées : &lt;br /&gt;
&lt;ul&gt;
&lt;li&gt;L’arrière-plan (Background) et la Surbrillance d’arrière-plan (HighlightBackground) donnent un style à l’arrière-plan des menus, des boutons et autres.&lt;/li&gt;
&lt;li&gt;La Surbrillance d’arrière-olan sombre (DimmerHighlightBackground) fait de même, mais pour les zones plus sombres ou les éléments à fort contraste&lt;/li&gt;.
&lt;li&gt;La Couleur de la lueur (GlowColor) est utilisé pour les éléments vitreux comme les interrupteurs, les indicateurs de marche avant/arrière, etc.&lt;/li&gt;
&lt;/ul&gt;
 &lt;/p&gt;
&lt;p&gt;
Pour plus d’informations sur les couleurs et d’autres aspects de l’interface graphique, consultez la &lt;a href=&quot;https://sailfishos.org/develop/docs/silica/qml-sailfishsilica-sailfish-silica-theme.html&quot;&gt;Documentation officielle du thème&lt;/a&gt;.
&lt;/p&gt;
</translation>
    </message>
</context>
<context>
    <name>HelpCredits</name>
    <message>
        <source>About</source>
        <translation type="vanished">About</translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpCredits.qml" line="6"/>
        <source>About</source>
        <comment>Help Section</comment>
        <translation>À propos</translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpCredits.qml" line="7"/>
        <source>Version:</source>
        <translation>Version :</translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpCredits.qml" line="8"/>
        <source>Copyright:</source>
        <translation>Droits d’auteur :</translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpCredits.qml" line="9"/>
        <source>License:</source>
        <translation>Licence :</translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpCredits.qml" line="10"/>
        <source>Source Code:</source>
        <translation>Code source :</translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpCredits.qml" line="11"/>
        <source>Translations</source>
        <translation>Traductions</translation>
    </message>
</context>
<context>
    <name>HelpDaemon</name>
    <message>
        <location filename="../qml/pages/help/HelpDaemon.qml" line="7"/>
        <source>The Watcher Daemon</source>
        <comment>Help Section</comment>
        <translation>Le Démon de surveillance</translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpDaemon.qml" line="9"/>
        <source>&lt;p&gt;The Watcher Daemon is a background service which can take care of some tasks while %1 is not running. Its main purpose is to apply color schemes after the %2 has changed.&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpDaemon.qml" line="9"/>
        <source>Ambience</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpDaemon.qml" line="10"/>
        <source>&lt;p&gt;It can be configured by tapping the &lt;i&gt;Daemon Settings&lt;/i&gt; button on the Advanced page.&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpDaemon.qml" line="11"/>
        <source>&lt;p&gt;This is an ever-evolving (and experimental) feature with more functions added each release.&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpDaemon.qml" line="12"/>
        <source>&lt;p&gt;One upcoming feature is to apply &amp;quot;Night Mode&amp;quot; color schemes at the appropriate time.&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;p&gt;The Watcher Daemon is a background service which can take care of some tasks while %1 is not running.&lt;/p&gt;</source>
        <translation type="vanished">&lt;p&gt;Le Démon de surveillance est un service d’arrière-plan qui peut prendre en charge certaines tâches lorsque %1 n’est pas en cours d’exécution.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;The Watcher Daemon is a background service which can take care of some tasks while %1 is not running automatically.&lt;/p&gt;</source>
        <translation type="vanished">&lt;p&gt;The Watcher Daemon is a background service which can take care of some tasks while %1 is not running automatically.&lt;/p&gt;</translation>
    </message>
</context>
<context>
    <name>HelpExport</name>
    <message>
        <source>Exporting Ambiences</source>
        <translation type="vanished">Exporting Ambiences</translation>
    </message>
    <message>
        <source>&lt;p&gt;To export an Ambience, after having edited the colors, select the &quot;%1&quot; menu entry to bring up the Export page.&lt;/p&gt;</source>
        <comment>argument is the name of the menu entry &apos;Export to Ambience Package&apos;</comment>
        <translation type="vanished">&lt;p&gt;To export an Ambience, after having edited the colours, select the &quot;%1&quot; menu entry to bring up the Export page.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpExport.qml" line="7"/>
        <source>Exporting Ambiences</source>
        <comment>Help Section</comment>
        <translation>Exportation d’ambiances</translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpExport.qml" line="10"/>
        <source>&lt;p&gt;To export an Ambience, after having edited the colors, select the &quot;%1&quot; menu entry to bring up the Export page.&lt;/p&gt;</source>
        <extracomment>argument is the name of the menu entry &apos;Export to Ambience Package&apos;</extracomment>
        <translation>&lt;p&gt;Pour exporter une Ambiance, après avoir modifié les couleurs, sélectionnez l’entrée de menu « %1 » pour faire apparaître la page d’exportation &lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpExport.qml" line="11"/>
        <source>Export to Ambience package</source>
        <translation>Exporer vers le paquet Ambiance</translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpExport.qml" line="22"/>
        <source>&lt;p&gt;Enter a name for your Ambience there and click the Ambience Name box to save.
This will do two things: write a .ambience file to your Documents folder, and save some settings for the %1 companion tool (see below).&lt;br /&gt;
If you like you can now edit the .ambience file using a text editor, e.g. for adding ringtone and sound information.
&lt;/p&gt;</source>
        <extracomment>the argument is also a translated string, the name of the builder tool</extracomment>
        <translation>&lt;p&gt; Entrez un nom pour votre Ambiance et cliquez sur la case Nom de l’Ambiance pour sauvegarder.
Ceci fera deux choses : écrire un fichier .ambience dans votre dossier Documents et sauvegarder certains paramètres pour l’outil compagnon %1 (voir ci-dessous) &lt;br /&gt;.
Si vous le souhaitez, vous pouvez maintenant modifier le fichier .ambience en utilisant un éditeur de texte, par exemple pour ajouter des sonneries et des informations sonores.
&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpExport.qml" line="28"/>
        <source>The Builder</source>
        <comment>Help Section</comment>
        <translation>Le Constructeur</translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpExport.qml" line="31"/>
        <source>&lt;p&gt;
When ready, launch the %1.
The Builder picks up the information from dconf and the .ambience file to create an RPM file, and prompt to install it if successful.
The package will also be saved in the Documents folder so you can share it with your friends.
&lt;/p&gt;</source>
        <extracomment>the argument is also a translated string, the name of the builder tool</extracomment>
        <translation>&lt;p&gt;
Une fois prêt, lancez le %1.
Le Constructeur récupère les informations depuis dconf et du fichier .ambience pour créer un fichier RPM, et invite à l’installer s’il réussit.
Le paquet sera également enregistré dans le dossier Documents afin que vous puissiez le partager avec vos amis.
&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpExport.qml" line="39"/>
        <source>Notes</source>
        <comment>Help Section</comment>
        <extracomment>as in something to add, not a small piece of paper</extracomment>
        <translation>Notes</translation>
    </message>
    <message>
        <source>&lt;p&gt;Enter a name for your Ambience there and click the Ambience Name box to save.
This will do two things: write a .ambience file to your Documents folder, and save some settings for the %1 companion tool (see below).&lt;br /&gt;
If you like you can now edit the .ambience file using a text editor, e.g. for adding ringtone and sound information.
&lt;/p&gt;</source>
        <comment>the argument is also a translated string, the name of the builder tool</comment>
        <translation type="vanished">&lt;p&gt;Enter a name for your Ambience there and click the Ambience Name box to save.
This will do two things: write a .ambience file to your Documents folder, and save some settings for the %1 companion tool (see below).&lt;br /&gt;
If you like you can now edit the .ambience file using a text editor, e.g. for adding ringtone and sound information.
&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;
When ready, launch the %1.
The Builder picks up the information from dconf and the .ambience file to create an RPM file, and prompt to install it if successful.
The package will also be saved in the Documents folder so you can share it with your friends.
&lt;/p&gt;</source>
        <comment>the argument is also a translated string, the name of the builder tool</comment>
        <translation type="vanished">&lt;p&gt;
When ready, launch the %1.
The Builder picks up the information from dconf and the .ambience file to create an RPM file, and prompt to install it if successful.
The package will also be saved in the Documents folder so you can share it with your friends.
&lt;/p&gt;</translation>
    </message>
    <message>
        <source>The Builder</source>
        <translation type="vanished">The ThemeColour® RPM Builder™</translation>
    </message>
    <message>
        <source>Notes</source>
        <comment>as in something to add, not a small piece of paper </comment>
        <translation type="vanished">Notes</translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpExport.qml" line="41"/>
        <source>&lt;ul&gt;
&lt;li&gt;The Builder is non-graphical and non-interactive. It needs to be configured through the app as described or it will not work.&lt;/li&gt;
&lt;li&gt;Installing an Ambience will switch it from Anonymous to a named Ambience. This means it will get a new Ambience shelf, which will be empty. Therefore it is a good idea to save the Scheme to the General Shelf.&lt;/li&gt;
&lt;li&gt;While both the .ambience file and the RPM package include most color values, not all are picked up by lipstick when switching Ambiences.  So, after having switched, open the App again and use the Ambience Shelf to load and apply your favourite scheme.&lt;/li&gt;
&lt;/ul&gt;
</source>
        <translation>&lt;ul&gt;
&lt;li&gt;Le Constructeur est non graphique et non interactif. Il doit être configuré par le biais de l’application comme décrit, sinon il ne fonctionnera pas.&lt;/li&gt;
&lt;li&gt;L’installation d’une Ambiance la fera passer d’Anonyme à Ambiance nommée. Ceci signifie qu’elle obtiendra une nouvelle étagère Ambiance, qui sera vide. C’est donc une bonne idée de sauvegarder le schéma sur l’étagère Général &lt;/li&gt;.
&lt;li&gt; Bien que le fichier .ambience et le paquet RPM incluent la plupart des valeurs de couleur, toutes ne sont pas prises en compte par lipstick lors du changement d’Ambiance.  Donc, après avoir changé, ouvrez à nouveau l’application et utilisez l’étagère d’ambiances pour charger et appliquer votre schéma préféré &lt;/li&gt;.
&lt;/ul&gt;
</translation>
    </message>
    <message>
        <source>&lt;ul&gt;
&lt;li&gt;The Builder is non-graphical and non-interactive. It needs to be configured through the app as described or it will not work.&lt;/li&gt;
&lt;li&gt;Installing an Ambience will switch it from Anonymous to a named Ambience. This means it will get a new Ambience shelf, which will be empty.&lt;/li&gt;
&lt;li&gt;While both the .ambience file and the RPM package include most color values, not all are picked up by lipstick when switching Ambiences. Use the Shelf to store your favourite scheme and apply it from there after switching.&lt;/li&gt;
&lt;/ul&gt;
</source>
        <translation type="vanished">&lt;ul&gt;
&lt;li&gt;The Builder is non-graphical and non-interactive. It needs to be configured through the app as described or it will not work.&lt;/li&gt;
&lt;li&gt;Installing an Ambience will switch it from Anonymous to a named Ambience. This means it will get a new Ambience shelf, which will be empty.&lt;/li&gt;
&lt;li&gt;While both the .ambience file and the RPM package include most colour values, not all are picked up by lipstick when switching Ambiences. Use the Shelf to store your favourite scheme and apply it from there after switching.&lt;/li&gt;
&lt;/ul&gt;
</translation>
    </message>
</context>
<context>
    <name>HelpGeneral</name>
    <message>
        <source>General</source>
        <translation type="vanished">General</translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpGeneral.qml" line="6"/>
        <source>General</source>
        <comment>Help Section</comment>
        <translation>Général</translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpGeneral.qml" line="8"/>
        <source>&lt;p&gt;
This application allows you to modify the current color scheme of Lipstick (a.k.a. Home Screen).
&lt;/p&gt;
&lt;p&gt;
It can not (yet) however, change saved Ambiences directly. Applied changes will not survive an Ambience change, Lipstick restart, or device reboot.&lt;br/&gt;
However, using the Shelves feature you can restore schemes you previously saved and re-apply them afterwards.
Also, by exporting changes to an installable package, changes can be made permanent.
&lt;/p&gt;
&lt;p&gt;
Currently only some colors can be edited. There are others in use by the system which are usually autocomputed from the basic four and can not be modified.&lt;br/&gt;
&lt;/p&gt;
&lt;p&gt;
We are working on overcoming some of these limitations, but some things would require patching the system UI elements which is beyond the scope of this app.
&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpGeneral.qml" line="26"/>
        <source>
&lt;p&gt;The basic workflow for re-coloring an existing Ambience or creating a new one would be something like this:&lt;/p&gt;

&lt;ol&gt;
&lt;li&gt;switch to an Ambience of your liking using the usual methods&lt;/li&gt;
&lt;li&gt;launch ThemeColor&lt;/li&gt;
&lt;li&gt;using the Sliders editing mode, adjust the four basic colors (primary, secondary, highlight and secondary highlight) to your liking.&lt;/li&gt;
&lt;li&gt;use the PullDown menu &lt;i&gt;Apply Colors to System&lt;/i&gt; to apply to the current theme&lt;/li&gt;
&lt;li&gt;explore the Sailfish UI a bit to make sure all looks right. Always check the VKB,  that can easily become unreadable&lt;/li&gt;
&lt;li&gt;go back to ThemeColor, and use the Shelves to store your colors for later reuse from within the app.&lt;/li&gt;
&lt;li&gt;adjust if necessary and repeat the last two steps above until everything looks pretty&lt;/li&gt;
&lt;li&gt;(optional:) to export and create an ambience RPM, and optionally install it, use the menu item &lt;i&gt;Export Ambience&lt;/i&gt;&lt;/li&gt;
&lt;/ol&gt;

&lt;p&gt; … and done! Your new Ambience should now show up in the usual place in Settings to select.&lt;/p&gt;

&lt;p&gt;If it does not (sometimes it happens) that means that ambienced has crashed.
In that case you can try to use the two &lt;i&gt;Restart&lt;/i&gt; functions on the Advanced page
to restart either Lipstick (the Home Screen), or just ambienced (the Theme engine)).
&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;p&gt;
This application allows you to modify the current color scheme of Lipstick (a.k.a. Home Screen).
&lt;/p&gt;
&lt;p&gt;
It can not (yet) however, change saved Ambiences directly. Applied changes will not survive an Ambience change, Lipstick restart, or device reboot.&lt;br/&gt;
However, using the Shelves feature you can restore schemes you previously saved and re-apply them afterwards.  &lt;/p&gt;
Also, by exporting changes to an installable package, changes can be made permanent.
&lt;/p&gt;
&lt;p&gt;
Currently only some colors can be edited. There are others in use by the system which are usually autocomputed from the basic four and can not be modified.&lt;br/&gt;
&lt;/p&gt;
&lt;p&gt;
We are working on overcoming some of these limitations, but some things would require patching the system UI elements which is beyond the scope of this app.
&lt;/p&gt;</source>
        <translation type="vanished">&lt;p&gt;
Cette application vous permet de modifier la palette de couleurs actuelle de Lipstick (alias l’écran d’accueil).
&lt;/p&gt;
&lt;p&gt;
Elle ne peut cependant pas (encore) modifier directement les Ambiances enregistrées. Les changements appliqués ne survivront pas à un changement d’Ambiance, à un redémarrage de Lipstick ou à un redémarrage de l’appareil.&lt;br/&gt;
Cependant, en utilisant la fonction Étagères, vous pouvez restaurer les ambiances que vous avez précédemment enregistrées et les réappliquer par la suite.  &lt;/p&gt;
De plus, en exportant les changements vers un paquet installable, les changements peuvent être rendus permanents.
&lt;/p&gt;
&lt;p&gt;
Actuellement, seules certaines couleurs peuvent être modifiées. Il y en a d’autres utilisées par le système qui sont généralement calculées automatiquement à partir des quatre de base et ne peuvent pas être modifiées.&lt;br/&gt;
&lt;/p&gt;
&lt;p&gt;
Nous travaillons à surmonter certaines de ces limitations, mais certaines choses nécessiteraient de patcher les éléments de l&apos;interface utilisateur du système, ce qui est au-delà de la portée de cette application.
&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpGeneral.qml" line="24"/>
        <source>Basic Workflow</source>
        <comment>Help Section</comment>
        <translation>Flux de travail de base</translation>
    </message>
    <message>
        <source>
&lt;p&gt;The basic workflow for re-coloring an existing Ambience or creating a new one would be something like this:&lt;/p&gt;

&lt;ol&gt;
&lt;li&gt;switch to an Ambience of your liking using the usual methods&lt;/li&gt;
&lt;li&gt;launch ThemeColor&lt;/li&gt;
&lt;li&gt;using the Sliders editing mode, adjust the four basic colors (primary, secondary, highlight and secondary highlight) to your liking.&lt;/li&gt;
&lt;li&gt;use the PullDown menu &lt;i&gt;Apply Colors to System&lt;/i&gt; to apply to the current theme&lt;/li&gt;
&lt;li&gt;explore the Sailfish UI a bit to make sure all looks right. Always check the VKB,  that can easily become unreadable&lt;/li&gt;
&lt;li&gt;go back to ThemeColor, and use the Shelves to store your colors for later reuse from within the app.&lt;/li&gt;
&lt;li&gt;adjust if necessary and repeat the last two steps above until eveything looks pretty&lt;/li&gt;
&lt;li&gt;(optional:) to export and create an ambience RPM, use the menu function &lt;i&gt;Export Ambience&lt;/i&gt;&lt;/li&gt;
&lt;li&gt;(optional:) type in a name for your new Ambience, and tap to store&lt;/li&gt;
&lt;li&gt;(optional:) tap the Builder button&lt;/li&gt;
&lt;li&gt;(optional:) that should prompt you to install the new Ambience RPM&lt;/li&gt;
&lt;/ol&gt;

&lt;p&gt; ... and done! Your new Ambience should now show up in the usual place in Settings to select.&lt;/p&gt;

&lt;p&gt;If it does not (sometimes it happens) that means that ambienced has crashed.
In that case you can try to use the two &lt;i&gt;Restart&lt;/i&gt; functions in the Pullup menu
from the app to restart either Lipstick (the Home Screen), or just
ambienced (the Theme engine)).
&lt;/p&gt;</source>
        <translation type="vanished">
&lt;p&gt;The basic workflow for re-colouring an existing Ambience or creating a new one would be something like this:&lt;/p&gt;

&lt;ol&gt;
&lt;li&gt;switch to an Ambience of your liking using the usual methods&lt;/li&gt;
&lt;li&gt;launch ThemeColour&lt;/li&gt;
&lt;li&gt;using the Sliders editing mode, adjust the four basic colours (primary, secondary, highlight and secondary highlight) to your liking.&lt;/li&gt;
&lt;li&gt;use the PullDown menu &lt;i&gt;Apply Colours to System&lt;/i&gt; to apply to the current theme&lt;/li&gt;
&lt;li&gt;explore the Sailfish UI a bit to make sure all looks right. Always check the VKB,  that can easily become unreadable&lt;/li&gt;
&lt;li&gt;go back to ThemeColour, and use the Shelves to store your colors for later reuse from within the app.&lt;/li&gt;
&lt;li&gt;adjust if necessary and repeat the last two steps above until everything looks pretty&lt;/li&gt;
&lt;li&gt;(optional:) to export and create an Ambience RPM, use the menu function &lt;i&gt;Export Ambience&lt;/i&gt;&lt;/li&gt;
&lt;li&gt;(optional:) type in a name for your new Ambience, and tap to store&lt;/li&gt;
&lt;li&gt;(optional:) tap the Builder button&lt;/li&gt;
&lt;li&gt;(optional:) that should prompt you to install the new Ambience RPM&lt;/li&gt;
&lt;/ol&gt;

&lt;p&gt; ... and done! Your new Ambience should now show up in the usual place in Settings to select.&lt;/p&gt;

&lt;p&gt;If it does not (sometimes it happens) that means that ambienced has crashed.
In that case you can try to use the two &lt;i&gt;Restart&lt;/i&gt; functions in the Pullup menu
from the app to restart either Lipstick (the Home Screen), or just
ambienced (the Theme engine)).
&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;
This application allows you to modify the current color scheme of Lipstick (a.k.a. Home Screen).
&lt;/p&gt;
&lt;p&gt;
It can not (yet) however, change or create new Ambiences directly.  Changes will not survive an Ambience change, Lipstick restart, or device reboot.&lt;br/&gt;
However, using the Shelves feature you can restore schemes you previously saved and re-apply when any of the above happens.  &lt;/p&gt;
&lt;p&gt;
Currently only some colors can be edited. There are others in use by the system which are usually autocomputed from the basic four and can not be modified.&lt;br/&gt;
&lt;/p&gt;
We are working on overcoming some of these limitations, but some things would require patching the system UI elements which is beyond the scope of this app.</source>
        <translation type="vanished">&lt;p&gt;
This application allows you to modify the current colour scheme of Lipstick (a.k.a. Home Screen).
&lt;/p&gt;
&lt;p&gt;
It can not (yet) however, change or create new Ambiences directly.  Changes will not survive an Ambience change, Lipstick restart, or device reboot.&lt;br/&gt;
However, using the Shelves feature you can restore schemes you previously saved and re-apply when any of the above happens.  &lt;/p&gt;
&lt;p&gt;
Currently only some colours can be edited. There are others in use by the system which are usually autocomputed from the basic four and can not be modified.&lt;br/&gt;
&lt;/p&gt;
We are working on overcoming some of these limitations, but some things would require patching the system UI elements which is beyond the scope of this app.</translation>
    </message>
    <message>
        <source>Basic Workflow</source>
        <translation type="vanished">Basic Workflow</translation>
    </message>
    <message>
        <source>
&lt;p&gt;The basic workflow for re-coloring an existing Ambience or creating a new one would be something like this:&lt;/p&gt;

&lt;ol&gt;
    &lt;li&gt;Create an Ambience using the usual method (select picture from Gallery, tab Ambience button, edit Name etc. and save)&lt;/li&gt;
    &lt;li&gt;Switch to that Ambience&lt;/li&gt;
    &lt;li&gt;Launch ThemeColor&lt;/li&gt;
    &lt;li&gt;using the Sliders editing mode, adjust the four basic colors (primary, secondary, highlight and secondary highlight) to your liking.&lt;/li&gt;
    &lt;li&gt;Use the PullDown menu &quot;Apply Colors to System&quot; to apply to the current theme&lt;/li&gt;
    &lt;li&gt;Explore the Sailfish UI a bit to make sure all looks right. Always check the VKB,  that can easily become unreadable&lt;/li&gt;
    &lt;li&gt;Go back to ThemeColor, and use the Shelves to store your colors for later reuse from within the app.&lt;/li&gt;
    &lt;li&gt;Adjust if necessary and repeat the last two steps above until you&apos;re satisfied&lt;/li&gt;
    &lt;li&gt;(Optional:) To export and create an ambience RPM, use the PushUp menu function &quot;Export Ambience&quot;&lt;/li&gt;
    &lt;li&gt;Type in a name for your new Ambience, and tap to store&lt;/li&gt;
    &lt;li&gt;Go to the app launcher (home) screen and launch the RPM Builder companion application&lt;/li&gt;
    &lt;li&gt;That should prompt you to install the new Ambience RPM&lt;/li&gt;
&lt;/ol&gt;

&lt;p&gt; ... and done! Your new ambience should now show up in the usual place in Settings to select.&lt;/p&gt;

&lt;p&gt;If it doesn&apos;t (sometimes it happens) that means that ambienced has crashed.
In that case you can try to use the two &quot;Restart&quot; functions in the Pullup menu
from the app to restart either Lipstick (the &quot;Home Screen&quot;, or just
ambienced).&lt;/p&gt;

</source>
        <translation type="vanished">
&lt;p&gt;The basic workflow for re-colouring an existing Ambience or creating a new one would be something like this:&lt;/p&gt;

&lt;ol&gt;
    &lt;li&gt;Create an Ambience using the usual method (select picture from Gallery, tab Ambience button, edit Name etc. and save)&lt;/li&gt;
    &lt;li&gt;Switch to that Ambience&lt;/li&gt;
    &lt;li&gt;Launch ThemeColour&lt;/li&gt;
    &lt;li&gt;using the Sliders editing mode, adjust the four basic colours (primary, secondary, highlight and secondary highlight) to your liking.&lt;/li&gt;
    &lt;li&gt;Use the PullDown menu &quot;Apply Colours to System&quot; to apply to the current theme&lt;/li&gt;
    &lt;li&gt;Explore the Sailfish UI a bit to make sure all looks right. Always check the VKB,  that can easily become unreadable&lt;/li&gt;
    &lt;li&gt;Go back to ThemeColour, and use the Shelves to store your colours for later reuse from within the app.&lt;/li&gt;
    &lt;li&gt;Adjust if necessary and repeat the last two steps above until you&apos;re satisfied&lt;/li&gt;
    &lt;li&gt;(Optional:) To export and create an ambience RPM, use the PushUp menu function &quot;Export Ambience&quot;&lt;/li&gt;
    &lt;li&gt;Type in a name for your new Ambience, and tap to store&lt;/li&gt;
    &lt;li&gt;Go to the app launcher (home) screen and launch the RPM Builder companion application&lt;/li&gt;
    &lt;li&gt;That should prompt you to install the new Ambience RPM&lt;/li&gt;
&lt;/ol&gt;

&lt;p&gt; ... and done! Your new ambience should now show up in the usual place in Settings to select.&lt;/p&gt;

&lt;p&gt;If it doesn&apos;t (sometimes it happens) that means that ambienced has crashed.
In that case you can try to use the two &quot;Restart&quot; functions in the Pullup menu
from the app to restart either Lipstick (the &quot;Home Screen&quot;, or just
ambienced).&lt;/p&gt;

</translation>
    </message>
</context>
<context>
    <name>HelpHow</name>
    <message>
        <source>Loading and Applying Colors</source>
        <translation type="vanished">Loading and Applying Colours</translation>
    </message>
    <message>
        <source>
&lt;p&gt;
        Ambience Colors are stored in several places: the .ambience file for installed Ambiences, a user-local database for user-created Ambiences, and &lt;i&gt;dconf&lt;/i&gt;.
        When an Ambience is loaded, its color values are written to a DConf location, and lipstick reads the values from there and uses it for its Theme.&lt;br /&gt;
&lt;/p&gt;
&lt;p&gt;
        We use that last part in this application, in that when we &quot;Apply&quot; colors to the system, we simply write to that DConf location, and lipstick notices and adjusts it theme.
        The good thing is, while official Ambiences only define four colors (two of which are always the same in practice), writing additional values to that location works, if they are defined as changable by the Sailfish Theme.
&lt;/p&gt;
&lt;p&gt;
        The location in question is &lt;pre&gt;/desktop/jolla/theme/color&lt;/pre&gt; and can be read from command line using &lt;pre&gt;dconf dump&lt;/pre&gt;
&lt;/p&gt;
</source>
        <translation type="vanished">
&lt;p&gt;
        Ambience Colours are stored in several places: the .ambience file for installed Ambiences, a user-local database for user-created Ambiences, and &lt;i&gt;dconf&lt;/i&gt;.
        When an Ambience is loaded, its colour values are written to a DConf location, and lipstick reads the values from there and uses it for its Theme.&lt;br /&gt;
&lt;/p&gt;
&lt;p&gt;
        We use that last part in this application, in that when we &quot;Apply&quot; colours to the system, we simply write to that DConf location, and lipstick notices and adjusts it theme.
        The good thing is, while official Ambiences only define four colours (two of which are always the same in practice), writing additional values to that location works, if they are defined as changable by the Sailfish Theme.
&lt;/p&gt;
&lt;p&gt;
        The location in question is &lt;pre&gt;/desktop/jolla/theme/color&lt;/pre&gt; and can be read from command line using &lt;pre&gt;dconf dump&lt;/pre&gt;
&lt;/p&gt;
</translation>
    </message>
    <message>
        <source>App Configuration and State</source>
        <translation type="vanished">App Configuration and State</translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpHow.qml" line="6"/>
        <source>Loading and Applying Colors</source>
        <comment>Help Section</comment>
        <translation>Chargement et application des couleurs</translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpHow.qml" line="8"/>
        <source>
&lt;p&gt;
        Ambience Colors are stored in several places: the .ambience file for installed Ambiences, a user-local database for user-created Ambiences, and &lt;i&gt;dconf&lt;/i&gt;.
        When an Ambience is loaded, its color values are written to a DConf location, and lipstick reads the values from there and uses it for its Theme.&lt;br /&gt;
&lt;/p&gt;
&lt;p&gt;
        We use that last part in this application, in that when we &quot;Apply&quot; colors to the system, we simply write to that DConf location, and lipstick notices and adjusts it theme.
        The good thing is, while official Ambiences only define four colors (two of which are always the same in practice), writing additional values to that location works, if they are defined as changable by the Sailfish Theme.
&lt;/p&gt;
&lt;p&gt;
        The dconf path in question is &lt;pre&gt;/desktop/jolla/theme/color&lt;/pre&gt; and can be read from command line using &lt;pre&gt;dconf dump&lt;/pre&gt;
&lt;/p&gt;
</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpHow.qml" line="22"/>
        <source>App Configuration and State</source>
        <comment>Help Section</comment>
        <translation>Configuration et état de l’application</translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpHow.qml" line="24"/>
        <source>
&lt;p&gt;
        Speaking of dconf, the application uses it in its own location to store things like Shelves and information for the builder.&lt;br /&gt;
        Unfortuately, if the user creates and deletes Ambiences a lot, and uses the Ambience Shelf, there might be a lot of stale setting in there which may warrant cleaning out from time to time.
&lt;/p&gt;
&lt;p&gt;
        The location for this is &lt;pre&gt;/org/nephros/openrepos-themecolor/storage&lt;/pre&gt;
&lt;/p&gt;
</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpHow.qml" line="34"/>
        <source>The Builder</source>
        <comment>Help Section</comment>
        <translation>Le Constructeur</translation>
    </message>
    <message>
        <source>The Builder</source>
        <translation type="vanished">The ThemeColour® RPM Builder™</translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpHow.qml" line="36"/>
        <source>
&lt;p&gt;
        The Ambience builder is a shell script launched from a oneshot systemd service with the following mode of operation:
&lt;/p&gt;
&lt;p&gt;
        First it reads DConf values prepared by the App to figure out things like Ambience name and location of files.&lt;br /&gt;
        It then puts them in a temporary directory together with a .spec file. It calls the &lt;pre&gt;rpmbuild&lt;/pre&gt; command which produces (hopfully) a package.&lt;br /&gt;
        If successful, that package is opened, prompting the user to install it.
&lt;/p&gt;
</source>
        <translation>
&lt;p&gt;
        Le constructeur d&apos;Ambiance est un script shell lancé à partir d&apos;un service systemd oneshot avec le mode de fonctionnement suivant :
&lt;/p&gt;
&lt;p&gt;
        Il lit d&apos;abord les valeurs DConf préparées par l&apos;application pour déterminer des choses comme le nom d&apos;Ambiance et l&apos;emplacement des fichiers &lt;br /&gt;.
        Il les place ensuite dans un répertoire temporaire avec un fichier .spec. Elle appelle la commande &lt;pre&gt;rpmbuild&lt;/pre&gt; qui produit (avec un peu de chance) un paquet&lt;br /&gt;.
        En cas de succès, ce paquet est ouvert et l&apos;utilisateur est invité à l&apos;installer.
&lt;/p&gt;
</translation>
    </message>
</context>
<context>
    <name>HelpPage</name>
    <message>
        <source>Use the Sliders in the lower section to adjust the colors.&lt;br /&gt;            When satisfied, tap the area above the slider to set the color.            Check what your theme will look like in the Showroom display.&lt;br /&gt;            &lt;br /&gt;            When you&apos;re done, use the PullUp Manu or the PushUp Menu to apply your creation.</source>
        <translation type="vanished">Use the Sliders in the lower section to adjust the colours.&lt;br /&gt;            When satisfied, tap the area above the slider to set the colour.            Check what your theme will look like in the Showroom display.&lt;br /&gt;            &lt;br /&gt;            When you&apos;re done, use the PullUp Manu or the PushUp Menu to apply your creation.</translation>
    </message>
    <message>
        <source>How to Use</source>
        <translation type="vanished">How to Use</translation>
    </message>
    <message>
        <source>General</source>
        <translation type="vanished">General</translation>
    </message>
    <message>
        <source>This application allows you to modify the current color scheme of Lipstick. It will not (yet) however, change or create new ambiences,           nor will your changes survive an Ambience change, Lipstick restart, or device reboot.&lt;br /&gt;
          Currently, only four colors can be edited: primaryColor, secondaryColor, highlightColor, and secondaryHighlightColor, as these are the ones saved by the system.          There are other colors in use by the system, which are autocomputed from these basic four, and can not be modified at the moment.&lt;br/&gt;&lt;br/&gt;
          We are working on overcoming some of these limitations.</source>
        <translation type="vanished">This application allows you to modify the current colour scheme of Lipstick. It will not (yet) however, change or create new ambiences,           nor will your changes survive an Ambience change, Lipstick restart, or device reboot.&lt;br /&gt;
          Currently, only four colours can be edited: primaryColor, secondaryColor, highlightColor, and secondaryHighlightColor (sic!), as these are the ones saved by the system.          There are other colours in use by the system, which are autocomputed from these basic four, and can not be modified at the moment.&lt;br/&gt;&lt;br/&gt;
          We are working on overcoming some of these limitations.</translation>
    </message>
    <message>
        <source>This application allows you to modify the current color scheme of Lipstick. It will not (yet) however, change or create new ambiences,           nor will your changes survive an Ambience change, Lipstick restart, or device reboot.&lt;br /&gt;
          Currently, only some colors can be edited.          There are other colors in use by the system, which are autocomputed from the basic four, and can not be modified at the moment.&lt;br/&gt;&lt;br/&gt;
          We are working on overcoming some of these limitations.</source>
        <translation type="vanished">This application allows you to modify the current colour scheme of Lipstick. It will not (yet) however, change or create new ambiences, nor will your changes survive an Ambience change, Lipstick restart, or device reboot.&lt;br /&gt;
Currently, only some colours can be edited. There are other colours in use by the system, which are autocomputed from the basic four, and can not be modified at the moment.&lt;br/&gt;&lt;br/&gt;
We are working on overcoming some of these limitations.</translation>
    </message>
    <message>
        <source>The Showroom</source>
        <translation type="vanished">The Showroom</translation>
    </message>
    <message>
        <source>The Top area on the first page (&quot;Showroom&quot;) is non-interactive          and just shows the colors that are selected currently.&lt;br /&gt;          Here you can preview your creation.          </source>
        <translation type="vanished">The Top area on the first page (&quot;Showroom&quot;) is non-interactive          and just shows the colours that are selected currently.&lt;br /&gt;          Here you can preview your creation.          </translation>
    </message>
    <message>
        <source>The Laboratory</source>
        <translation type="vanished">The Laboratory</translation>
    </message>
    <message>
        <source>In Slider Input Mode, use the Sliders in the lower section to adjust the colors.&lt;br /&gt;            In Text Input Mode, you can enter color values directly.&lt;br /&gt;
            Check what your theme will look like in the Showroom display.&lt;br /&gt;            &lt;br /&gt;            When you&apos;re done, use the PullUp Menu or the PushUp Menu to apply your creation to the Session.
          </source>
        <translation type="vanished">In Slider Input Mode, use the Sliders in the lower section to adjust the colours.&lt;br /&gt;            In Text Input Mode, you can enter colour values directly.&lt;br /&gt;
            Check what your theme will look like in the Showroom display.&lt;br /&gt;            &lt;br /&gt;            When you&apos;re done, use the PullUp Menu or the PushUp Menu to apply your creation to the Session.
          </translation>
    </message>
    <message>
        <source>In Slider input mode, use the Sliders in the lower section to adjust the colors.            In Text input mode, you can enter color values directly. Randomizer does what it sais, and Jolla Original you already know.&lt;br /&gt;             Check what your theme will look like in the Showroom display.&lt;br /&gt;            &lt;br /&gt;            When you&apos;re done, use the PullDown Menu.
          </source>
        <translation type="vanished">In Slider input mode, use the Sliders in the lower section to adjust the colours. In Text input mode, you can enter colour values directly. Randomizer does what it sais, and Jolla Original you already know.&lt;br /&gt;Check what your theme will look like in the Showroom display.&lt;br /&gt; &lt;br /&gt; When you&apos;re done, use the PullDown Menu.
          </translation>
    </message>
    <message>
        <source>The Cupboard</source>
        <translation type="vanished">The Cupboard</translation>
    </message>
    <message>
        <source>This area allows you to store your created palettes for re-use later.             There is one global Cupboard, and one specific for the current ambience.&lt;br /&gt;             Note that only system-wide Ambiences have a name, custon ones will show as anonymous (for now)</source>
        <translation type="vanished">This area allows you to store your created palettes for re-use later. There is one global Cupboard, and one specific for the current Ambience.&lt;br /&gt; Note that only system-wide Ambiences have a name, custom ones will show as anonymous (for now)</translation>
    </message>
    <message>
        <source>Tips and Caveats</source>
        <translation type="vanished">Tips and Caveats</translation>
    </message>
    <message>
        <source>It is possible to create color schemes which make parts of the UI unreadable. Check especially non-obvious areas such as the virtual Keyboard.&lt;br /&gt;
          It&apos;s a good idea to store a known-good color sceme in the Cupboard so you can go restore easily.&lt;br /&gt;          &lt;br /&gt;          If you have messed up the colours completely, use the PullUp menu option to reset everything, or from command line issue:&lt;br /&gt;          &lt;pre&gt;dconf dump /desktop/jolla/theme/color/&lt;/pre&gt;          &lt;pre&gt;dconf reset /desktop/jolla/theme/color/highlightBackground&lt;/pre&gt;          and repeat for all the other colors stored there. &lt;br /&gt;          Changing the Ambience from the System Settings may also help.</source>
        <translation type="vanished">It is possible to create colour schemes which make parts of the UI unreadable. Check especially non-obvious areas such as the virtual Keyboard.&lt;br /&gt;
It&apos;s a good idea to store a known-good colour scheme in the Cupboard so you can go restore easily.&lt;br /&gt;&lt;br /&gt;If you have messed up the colours completely, use the PullUp menu option to reset everything, or from command line issue:&lt;br /&gt;&lt;pre&gt;dconf dump /desktop/jolla/theme/color/&lt;/pre&gt;&lt;pre&gt;dconf reset /desktop/jolla/theme/color/highlightBackground&lt;/pre&gt;and repeat for all the other colours stored there. &lt;br /&gt;Changing the Ambience from the System Settings may also help.</translation>
    </message>
    <message>
        <source>This application allows you to modify the current color scheme of Lipstick. It will not (yet) however, change or create new Ambiences, 
nor will your changes survive an Ambience change, Lipstick restart, or device reboot.&lt;br /&gt;
Currently, only some colors can be edited.
There are other colors in use by the system, which are autocomputed from the basic four, and can not be modified.&lt;br/&gt;&lt;br/&gt;
We are working on overcoming some of these limitations.</source>
        <translation type="vanished">This application allows you to modify the current colour scheme of Lipstick. It will not (yet) however, change or create new Ambiences, nor will your changes survive an Ambience change, Lipstick restart, or device reboot.&lt;br /&gt;
Currently, only some colours can be edited.
There are other colours in use by the system, which are autocomputed from the basic four, and can not be modified.&lt;br/&gt;&lt;br/&gt;
We are working on overcoming some of these limitations.</translation>
    </message>
    <message>
        <source>The top area on the first page (&quot;Showroom&quot;) is non-interactive and just shows the colors that are selected currently.&lt;br /&gt;Here you can preview your creation.</source>
        <translation type="vanished">The top area on the first page (&quot;Showroom&quot;) is non-interactive and just shows the colours that are selected currently.&lt;br /&gt;Here you can preview your creation.</translation>
    </message>
    <message>
        <source>In Slider input mode, use the Sliders in the lower section to adjust the colors.  In Text input mode, you can enter color values directly. Randomizer
does what it sais, and Jolla Original you already know.  Swapper lets you change color definitions.&lt;br /&gt; Check what your theme will look like in the
Showroom display.&lt;br /&gt;
&lt;br /&gt;
When you&apos;re done, use the PullDown Menu to apply the colors to the curent session.</source>
        <translation type="vanished">In Slider input mode, use the Sliders in the lower section to adjust the colours. In Text input mode, you can enter colour values directly. Randomizer
does what it sais, and Jolla Original you already know. Swapper lets you change colour definitions.&lt;br /&gt; Check what your theme will look like in the
Showroom display.&lt;br /&gt;
&lt;br /&gt;
When you&apos;re done, use the PullDown Menu to apply the colours to the current session.</translation>
    </message>
    <message>
        <source>This area allows you to store your created palettes for re-use later.  There is one global Cupboard, and one specific for the current Ambience.&lt;br /&gt;
Note that only system-wide Ambiences have a name, custom ones will show as anonymous (for now)</source>
        <translation type="vanished">This area allows you to store your created palettes for re-use later.  There is one global Cupboard, and one specific for the current Ambience.&lt;br /&gt;
Note that only system-wide Ambiences have a name, custom ones will show as anonymous (for now)</translation>
    </message>
    <message>
        <source>Jolla Ambiences only define four colors, primary (which is black or white depending on Ambience Scheme), secondary which is a little less opaque primary with some added, 
plus highlight and secondary highlight which are those selected in the Ambience Settings.&lt;br /&gt;
The App lets you edit other colors than these four, but they will not be affected by Ambience changes or Lipstick restarts as they are stored in the dconf database and stay there. 
This means once applied through the app, they will always stay the same until you change them again in the App. 
You can use the reset option documented below to get rid of them if necessary.
&lt;br /&gt;
The app frequently gets confused about colors when switching editing modes, applying colors to system, or taking palettes from the Cupboard.
If that happens, try reloading the colors from the system, that helps in most cases.&lt;br /&gt;
&lt;br /&gt;
It is possible to create color schemes which make parts of the UI unreadable. Check especially non-obvious areas such as the virtual Keyboard.&lt;br /&gt;
It&apos;s a good idea to store a known-good color scheme in the Cupboard so you can go restore easily.&lt;br /&gt;
&lt;br /&gt;
If you have messed up the colors somehow, use the PullUp menu option to reset everything, or from command line issue:&lt;br /&gt;
&lt;pre&gt;dconf dump /desktop/jolla/theme/color/&lt;/pre&gt;
&lt;pre&gt;dconf reset /desktop/jolla/theme/color/highlightBackground&lt;/pre&gt;
and repeat for all the other colors stored there. &lt;br /&gt;
Changing the Ambience from the System Settings may also help.</source>
        <translation type="vanished">Jolla Ambiences only define four colours, primary (which is black or white depending on Ambience Scheme), secondary which is a little less opaque primary with some added,
plus highlight and secondary highlight which are those selected in the Ambience Settings.&lt;br /&gt;
The App lets you edit other colours than these four, but they will not be affected by Ambience changes or Lipstick restarts as they are stored in the dconf database and stay there.
This means once applied through the app, they will always stay the same until you change them again in the App.
You can use the reset option documented below to get rid of them if necessary.
&lt;br /&gt;
The app frequently gets confused about colours when switching editing modes, applying colours to system, or taking palettes from the Cupboard.
If that happens, try reloading the colours from the system, that helps in most cases.&lt;br /&gt;
&lt;br /&gt;
It is possible to create colour schemes which make parts of the UI unreadable. Check especially non-obvious areas such as the virtual Keyboard.&lt;br /&gt;
It&apos;s a good idea to store a known-good colour scheme in the Cupboard so you can go restore easily.&lt;br /&gt;
&lt;br /&gt;
If you have messed up the colours somehow, use the PullUp menu option to reset everything, or from command line issue:&lt;br /&gt;
&lt;pre&gt;dconf dump /desktop/jolla/theme/color/&lt;/pre&gt;
&lt;pre&gt;dconf reset /desktop/jolla/theme/color/highlightBackground&lt;/pre&gt;
and repeat for all the other colours stored there. &lt;br /&gt;
Changing the Ambience from the System Settings may also help.</translation>
    </message>
    <message>
        <source>This application allows you to modify the current color scheme of Lipstick. It will not (yet) however, change or create new Ambiences, nor will your changes survive an Ambience change, Lipstick restart, or device reboot.&lt;br /&gt;
Currently, only some colors can be edited. There are other colors in use by the system, which are autocomputed from the basic four, and can not be modified.&lt;br/&gt;
&lt;br/&gt;
We are working on overcoming some of these limitations.</source>
        <translation type="vanished">This application allows you to modify the current colour scheme of Lipstick. It will not (yet) however, change or create new Ambiences, nor will your changes survive an Ambience change, Lipstick restart, or device reboot.&lt;br /&gt;
Currently, only some colours can be edited. There are other colours in use by the system, which are autocomputed from the basic four, and can not be modified.&lt;br/&gt;
&lt;br/&gt;
We are working on overcoming some of these limitations.</translation>
    </message>
    <message>
        <source>The top area on the first page (&quot;Showroom&quot;) shows the colors that are selected currently using various UI elements.&lt;br /&gt;Here you can preview your creation.&lt;br /&gt;Tapping on either area will hide it. To unhide, tap the title header.</source>
        <translation type="vanished">The top area on the first page (&quot;Showroom&quot;) shows the colours that are selected currently using various UI elements.&lt;br /&gt;Here you can preview your creation.&lt;br /&gt;Tapping on either area will hide it. To unhide, tap the title header.</translation>
    </message>
    <message>
        <source>In Slider input mode, use the Sliders in the lower section to adjust the colors. In Text input mode, you can enter color values directly. Randomizer does what it sais, and Jolla Original you already know. Swapper lets you change color definitions.&lt;br /&gt;
Check what your theme will look like in the Showroom display.&lt;br /&gt;
&lt;br /&gt;
When you&apos;re done, use the PullDown Menu to apply the colors to the curent session.</source>
        <translation type="vanished">In Slider input mode, use the Sliders in the lower section to adjust the colours. In Text input mode, you can enter colour values directly. Randomizer does what it sais, and Jolla Original you already know. Swapper lets you change colour definitions.&lt;br /&gt;
Check what your theme will look like in the Showroom display.&lt;br /&gt;
&lt;br /&gt;
When you&apos;re done, use the PullDown Menu to apply the colours to the current session.</translation>
    </message>
    <message>
        <source>This area allows you to store your created palettes for re-use later. There is one global Cupboard, and one specific for the current Ambience.&lt;br /&gt;
Note that only system-wide Ambiences have a name, custom ones will show as anonymous (for now)</source>
        <translation type="vanished">This area allows you to store your created palettes for re-use later. There is one global Cupboard, and one specific for the current Ambience.&lt;br /&gt;
Note that only system-wide Ambiences have a name, custom ones will show as anonymous (for now)</translation>
    </message>
    <message>
        <source>Jolla Ambiences only define four colors, primary (which is black or white depending on Ambience Scheme), secondary which is a little less opaque primary, plus highlight and secondary highlight which are those selected in the Ambience Settings.&lt;br /&gt;
The App lets you edit other colors than these four, but they will not be affected by Ambience changes or Lipstick restarts as they are stored in the dconf database and stay there. This means once applied through the app, they will always stay the same until you change them again in the App. You can use the reset option documented below to get rid of them if necessary.
&lt;br /&gt;
The app frequently gets confused about colors when switching editing modes, applying colors to system, or taking palettes from the Cupboard. If that happens, try reloading from the system, that helps in most cases.&lt;br /&gt;
&lt;br /&gt;
It is possible to create color schemes which make parts of the UI unreadable. Check especially non-obvious areas such as the virtual Keyboard.&lt;br /&gt;
It&apos;s a good idea to store a known-good color scheme in the Cupboard so you can go restore easily.&lt;br /&gt;
&lt;br /&gt;
If you have messed up the colors somehow, use the PullUp menu option to reset everything, or from command line issue:&lt;br /&gt;
&lt;pre&gt;dconf dump /desktop/jolla/theme/color/&lt;/pre&gt;
&lt;pre&gt;dconf reset /desktop/jolla/theme/color/highlightBackground&lt;/pre&gt;
and repeat for all the other colors stored there. &lt;br /&gt;
Changing the Ambience from the System Settings may also help.</source>
        <translation type="vanished">Jolla Ambiences only define four colours, primary (which is black or white depending on Ambience Scheme), secondary which is a little less opaque primary, plus highlight and secondary highlight which are those selected in the Ambience Settings.&lt;br /&gt;
The App lets you edit other colours than these four, but they will not be affected by Ambience changes or Lipstick restarts as they are stored in the dconf database and stay there. This means once applied through the app, they will always stay the same until you change them again in the App. You can use the reset option documented below to get rid of them if necessary.
&lt;br /&gt;
The app frequently gets confused about colours when switching editing modes, applying colours to system, or taking palettes from the Cupboard. If that happens, try reloading the colours from the system, that helps in most cases.&lt;br /&gt;
&lt;br /&gt;
It is possible to create colour schemes which make parts of the UI unreadable. Check especially non-obvious areas such as the virtual Keyboard.&lt;br /&gt;
It&apos;s a good idea to store a known-good colour scheme in the Cupboard so you can go restore easily.&lt;br /&gt;
&lt;br /&gt;
If you have messed up the colours somehow, use the PullUp menu option to reset everything, or from command line issue:&lt;br /&gt;
&lt;pre&gt;dconf dump /desktop/jolla/theme/color/&lt;/pre&gt;
&lt;pre&gt;dconf reset /desktop/jolla/theme/color/highlightBackground&lt;/pre&gt;
and repeat for all the other colours stored there. &lt;br /&gt;
Changing the Ambience from the System Settings may also help.</translation>
    </message>
    <message>
        <source>Version:</source>
        <translation type="vanished">Version:</translation>
    </message>
    <message>
        <source>Copyright:</source>
        <translation type="vanished">Copyright:</translation>
    </message>
    <message>
        <source>License:</source>
        <translation type="vanished">License:</translation>
    </message>
    <message>
        <source>Source Code:</source>
        <translation type="vanished">Source Code:</translation>
    </message>
    <message>
        <source>Translations:</source>
        <translation type="vanished">Translations:</translation>
    </message>
    <message>
        <source>Jolla Ambiences only define four colors, primary (which is black or white depending on Ambience Scheme), secondary which is a little less opaque primary with some added, plus highlight and secondary highlight which are those selected in the Ambience Settings.&lt;br /&gt;
The App lets you edit other colors than these four, but they will not be affected by Ambience changes or Lipstick restarts as they are stored in the dconf database and stay there. This means once applied through the app, they will always stay the same until you change them again in the App.  You can use the reset option documented below to get rid of them if necessary.
&lt;br /&gt;
The app frequently gets confused about colors when switching editing modes, applying colors to system, or taking palettes from the Cupboard. If that happens, try reloading the colors from the system, that helps in most cases.&lt;br /&gt;
&lt;br /&gt;
It is possible to create color schemes which make parts of the UI unreadable. Check especially non-obvious areas such as the virtual Keyboard.&lt;br /&gt;
It&apos;s a good idea to store a known-good color scheme in the Cupboard so you can go restore easily.&lt;br /&gt;
&lt;br /&gt;
If you have messed up the colors somehow, use the PullUp menu option to reset everything, or from command line issue:&lt;br /&gt;
&lt;pre&gt;dconf dump /desktop/jolla/theme/color/&lt;/pre&gt;
&lt;pre&gt;dconf reset /desktop/jolla/theme/color/highlightBackground&lt;/pre&gt;
and repeat for all the other colors stored there. &lt;br /&gt;
Changing the Ambience from the System Settings may also help.</source>
        <translation type="vanished">Jolla Ambiences only define four colours, primary (which is black or white depending on Ambience Scheme), secondary which is a little less opaque primary with some added, plus highlight and secondary highlight which are those selected in the Ambience Settings.&lt;br /&gt;
The App lets you edit other colours than these four, but they will not be affected by Ambience changes or Lipstick restarts as they are stored in the dconf database and stay there. This means once applied through the app, they will always stay the same until you change them again in the App. You can use the reset option documented below to get rid of them if necessary.
&lt;br /&gt;
The app frequently gets confused about colours when switching editing modes, applying colours to system, or taking palettes from the Cupboard. If that happens, try reloading the colours from the system, that helps in most cases.&lt;br /&gt;
&lt;br /&gt;
It is possible to create colour schemes which make parts of the UI unreadable. Check especially non-obvious areas such as the virtual Keyboard.&lt;br /&gt;
It&apos;s a good idea to store a known-good colour scheme in the Cupboard so you can go restore easily.&lt;br /&gt;
&lt;br /&gt;
If you have messed up the colours somehow, use the PullUp menu option to reset everything, or from command line issue:&lt;br /&gt;
&lt;pre&gt;dconf dump /desktop/jolla/theme/color/&lt;/pre&gt;
&lt;pre&gt;dconf reset /desktop/jolla/theme/color/highlightBackground&lt;/pre&gt;
and repeat for all the other colours stored there. &lt;br /&gt;
Changing the Ambience from the System Settings may also help.</translation>
    </message>
    <message>
        <source>About</source>
        <translation type="vanished">About</translation>
    </message>
    <message>
        <source>Version: </source>
        <translation type="vanished">Version: </translation>
    </message>
    <message>
        <source>Copyright: </source>
        <translation type="vanished">Copyright: </translation>
    </message>
    <message>
        <source>License: </source>
        <translation type="vanished">Licence: </translation>
    </message>
    <message>
        <source>Source Code: </source>
        <translation type="vanished">Source Code: </translation>
    </message>
    <message>
        <source>General Information</source>
        <translation type="vanished">General Information</translation>
    </message>
    <message>
        <source>About the UI</source>
        <translation type="vanished">About the UI</translation>
    </message>
    <message>
        <source>About Colors</source>
        <translation type="vanished">About Colours</translation>
    </message>
    <message>
        <source>Exporting Ambiences</source>
        <translation type="vanished">Exporting Ambiences</translation>
    </message>
    <message>
        <source>How it works</source>
        <translation type="vanished">How it works</translation>
    </message>
    <message>
        <source>Limitations, Tips etc.</source>
        <translation type="vanished">Limitations, Tips etc.</translation>
    </message>
    <message>
        <source>Credits</source>
        <translation type="vanished">Credits</translation>
    </message>
    <message>
        <source>Handbook</source>
        <comment>User Manual, Application Manual, Handbook, Howto</comment>
        <translation type="vanished">Handbook</translation>
    </message>
    <message>
        <source>Chapter %1:</source>
        <comment>chapter in the handbook. argument is a number</comment>
        <translation type="vanished">Chapter %1:</translation>
    </message>
    <message>
        <source>Application Manual</source>
        <translation type="vanished">Application Manual</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="24"/>
        <source>General Information</source>
        <comment>Help Index</comment>
        <translation>Informations générales</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="25"/>
        <source>About the UI</source>
        <comment>Help Index</comment>
        <translation>À propos de l’interface utilisater</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="26"/>
        <source>About Colors</source>
        <comment>Help Index</comment>
        <translation>À propos des couleurs</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="27"/>
        <source>Exporting Ambiences</source>
        <comment>Help Index</comment>
        <translation>Exportation d’ambiances</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="28"/>
        <source>Watcher Daemon</source>
        <comment>Help Index</comment>
        <translation>Démon de surveillance</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="29"/>
        <source>How it works</source>
        <comment>Help Index</comment>
        <translation>Comment ça fonctionne</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="30"/>
        <source>Limitations , Tips etc.</source>
        <comment>Help Index</comment>
        <translation>Limitations, conseils, etc.</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="31"/>
        <source>Credits</source>
        <comment>Help Index</comment>
        <translation>Crédits</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="40"/>
        <source>Handbook</source>
        <comment>Help Index</comment>
        <extracomment>User Manual, Application Manual, Handbook, Howto</extracomment>
        <translation>Guide d’utilisation</translation>
    </message>
    <message>
        <location filename="../qml/pages/HelpPage.qml" line="46"/>
        <source>Chapter %1:</source>
        <comment>Help Index Chapter Prefix</comment>
        <extracomment>chapter in the handbook. argument is a number</extracomment>
        <translation>Chapitre %1 :</translation>
    </message>
</context>
<context>
    <name>HelpTips</name>
    <message>
        <source>Tips and Caveats</source>
        <translation type="vanished">Tips and Caveats</translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpTips.qml" line="6"/>
        <source>Tips and Caveats</source>
        <comment>Help Section</comment>
        <translation>Conseils et mises en garde</translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpTips.qml" line="8"/>
        <source>
&lt;p&gt;
The app frequently gets confused about colors when switching editing modes, applying colors to system, or taking palettes from the Cupboard. If that happens, try reloading from the system, that helps in most cases.&lt;br /&gt;
&lt;/p&gt;
&lt;p&gt;
It is possible to create color schemes which make parts of the UI unreadable. Check especially non-obvious areas such as the virtual Keyboard.&lt;br /&gt;
It&apos;s a good idea to store a known-good color scheme in the Cupboard so you can go restore easily.
&lt;/p&gt;
&lt;p&gt;
If you have messed up the colors somehow, use the PullUp menu option to reset everything, or from command line issue:&lt;br /&gt;
&lt;pre&gt;dconf dump /desktop/jolla/theme/color/&lt;/pre&gt;
&lt;pre&gt;dconf reset /desktop/jolla/theme/color/highlightBackground&lt;/pre&gt;
and repeat for all the other colors stored there. &lt;br /&gt;
Changing the Ambience from the System Settings may also help.
&lt;/p&gt;
</source>
        <translation>
&lt;p&gt;
L&apos;application s&apos;embrouille fréquemment dans les couleurs lorsqu&apos;on change de mode d&apos;édition, qu&apos;on applique des couleurs au système ou qu&apos;on prend des palettes dans l&apos;Armoire. Si cela se produit, essayez de recharger à partir du système, cela aide dans la plupart des cas.&lt;br /&gt;
&lt;/p&gt;
&lt;p&gt;
Il est possible de créer des jeux de couleurs qui rendent certaines parties de l&apos;interface utilisateur illisibles. Vérifiez notamment les zones non évidentes comme le clavier virtuel.&lt;br /&gt;
C’est une bonne idée de stocker un schéma de couleurs connu et bon dans le placard afin de pouvoir le restaurer facilement.
&lt;/p&gt;
&lt;p&gt;
Si vous avez gâché les couleurs d&apos;une manière ou d&apos;une autre, utilisez l&apos;option du menu PullUp pour tout réinitialiser, ou à partir de la ligne de commande :&lt;br /&gt;
&lt;pre&gt;dconf dump /desktop/jolla/theme/color/&lt;/pre&gt;
&lt;pre&gt;dconf reset /desktop/jolla/theme/color/highlightBackground&lt;/pre&gt;
et répétez pour toutes les autres couleurs stockées là. &lt;br /&gt;
Modifier l&apos;ambiance à partir des paramètres du système peut également aider.
&lt;/p&gt;
</translation>
    </message>
</context>
<context>
    <name>HelpUIMain</name>
    <message>
        <source>The main UI sections</source>
        <translation type="vanished">The main UI sections</translation>
    </message>
    <message>
        <source>&lt;p&gt;The main UI consists of the following:
       &lt;ul&gt;
       &lt;li&gt;The Showroom&lt;/li&gt;
       &lt;li&gt;The Laboratory&lt;/li&gt;
       &lt;li&gt;The Cupboards&lt;/li&gt;
       &lt;li&gt;PullDown and PushUp Menus&lt;/li&gt;
       &lt;/ul&gt;&lt;/p&gt;</source>
        <translation type="vanished">&lt;p&gt;L’interface utilisateur principale se compose des éléments suivants :
       &lt;ul&gt;
       &lt;li&gt;La Salle d&apos;exposition&lt;/li&gt;
       &lt;li&gt;Le Laboratoire&lt;/li&gt;
       &lt;li&gt;Les Placards&lt;/li&gt;
       &lt;li&gt;Menus déroulants (haut et bas)&lt;/li&gt;
       &lt;/ul&gt; &lt;/p&gt;</translation>
    </message>
    <message>
        <source>The Showroom</source>
        <translation type="vanished">The Showroom</translation>
    </message>
    <message>
        <source>&lt;p&gt;The top area on the first page (&quot;Showroom&quot;) shows the colors that are selected currently.
This is so you can preview and check how your creations will look.&lt;/p&gt;
&lt;p&gt;
There are two parts: the top one showing text in various combinations of colors, and the bottom one for the UI elements like buttons etc.
Tapping on either of those parts will hide it. When both are hidden, a miniature version will appear.&lt;br /&gt;
To unhide all again tap the section header.
&lt;/p&gt;</source>
        <translation type="vanished">&lt;p&gt;The top area on the first page (&quot;Showroom&quot;) shows the colours that are selected currently.
This is so you can preview and check how your creations will look.&lt;/p&gt;
&lt;p&gt;
There are two parts: the top one showing text in various combinations of colours, and the bottom one for the UI elements like buttons etc.
Tapping on either of those parts will hide it. When both are hidden, a miniature version will appear.&lt;br /&gt;
To unhide all again tap the section header.
&lt;/p&gt;</translation>
    </message>
    <message>
        <source>The Laboratory</source>
        <translation type="vanished">The Laboratory</translation>
    </message>
    <message>
        <source>&lt;p&gt;The next component is the Laboratory. This is the where you edit the various colors.&lt;/p&gt;
&lt;p&gt;
On the top you find the input mode selector.
There are several ways to do this editing, called Input Modes, explained below.
The selector switches between them.
&lt;ul&gt;</source>
        <translation type="vanished">&lt;p&gt;The next component is the Laboratory. This is the where you edit the various colours.&lt;/p&gt;
&lt;p&gt;
On the top you find the input mode selector.
There are several ways to do this editing, called Input Modes, explained below.
The selector switches between them.
&lt;ul&gt;</translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpUIMain.qml" line="7"/>
        <source>The main UI sections</source>
        <comment>Help Section</comment>
        <translation>Les principales sections de l’interface utilisateur</translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpUIMain.qml" line="9"/>
        <source>&lt;p&gt;The main UI consists of the following:
       &lt;ul&gt;
       &lt;li&gt;The Showroom&lt;/li&gt;
       &lt;li&gt;The Laboratory&lt;/li&gt;
       &lt;li&gt;The Cupboards&lt;/li&gt;
       &lt;li&gt;PullDown Menu&lt;/li&gt;
       &lt;/ul&gt;&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpUIMain.qml" line="17"/>
        <source>The Showroom</source>
        <comment>Help Section</comment>
        <translation>La Salle d’exposition</translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpUIMain.qml" line="26"/>
        <source>&lt;p&gt;The top area on the first page (%1) shows the colors that are selected currently.
This is so you can preview and check how your creations will look.&lt;/p&gt;
&lt;p&gt;
There are two parts: the top one showing text in various combinations of colors, and the bottom one for the UI elements like buttons etc.
Tapping on either of those parts will hide it. When both are hidden, a miniature version will appear.&lt;br /&gt;
To unhide all again tap the section header.
&lt;/p&gt;</source>
        <extracomment>argument is the translation of &quot;Showroom&quot;</extracomment>
        <translation>&lt;p&gt;La zone supérieure de la première page (%1) affiche les couleurs qui sont actuellement sélectionnées.
Cela vous permet de prévisualiser et de vérifier l&apos;aspect de vos créations&lt;/p&gt;.
&lt;p&gt;
Il y a deux parties : celle du haut qui montre le texte dans diverses combinaisons de couleurs, et celle du bas pour les éléments de l&apos;interface utilisateur comme les boutons, etc.
En tapant sur l&apos;une de ces parties, vous la masquerez. Lorsque les deux sont cachées, une version miniature apparaît &lt;br /&gt;.
Pour tout dé-cacher à nouveau, appuyez sur l&apos;en-tête de la section.
&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpUIMain.qml" line="41"/>
        <source>The Laboratory</source>
        <comment>Help Section</comment>
        <translation>Le Laboratoire</translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpUIMain.qml" line="44"/>
        <source>&lt;p&gt;The next component is the %1. This is the where you edit the various colors.&lt;/p&gt;
&lt;p&gt;
On the top you find the input mode selector.
There are several ways to do this editing, called Input Modes, explained below.
The selector switches between them.
&lt;ul&gt;</source>
        <extracomment>argument is the translation of &quot;Laboratory&quot;</extracomment>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpUIMain.qml" line="50"/>
        <source>&lt;li&gt;&lt;strong&gt;Slider&lt;/strong&gt; input mode is the main editing mode.&lt;br /&gt;
There is a set of sliders for each color you can edit. Use them to adjust the colors using their red, green, blue (RGB) channels. 
Two slim indicators on top of each slider section shows the current, and the edited color, and the hex value will be shown in the area below them. If you tap that area, the sliders will reset to the value of the unedited color.&lt;br /&gt;
The button with the keyboard symbol opens a dialog where you can put in the value directly.&lt;br /&gt;
The button on the left side opens the Color Selector dialog from Sailfish Silica, in case you want to select a color from there.&lt;br /&gt;
At the bottom below the RGB sliders you will find the Glow editor. This works a little different than the others, allowing you to select the hue of any UI elements that have a glow effect. Below the slider there is a set of buttons that apply either black, white, or the four default colors to the Glow color.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpUIMain.qml" line="115"/>
        <source>&lt;p&gt;
       The PullDown menu contains the most used commands.
       &lt;p&gt;
       &lt;ul&gt;
       &lt;li&gt;Apply: The central command of the app. This will apply the current color theme to the system.&lt;/li&gt;
       &lt;li&gt;Reload: Loads the theme that is currently in use in the system.&lt;/li&gt;
       &lt;li&gt;Export: opens the Export Ambience page. See the &lt;b&gt;Export&lt;/b&gt; chapter for more.&lt;/li&gt;
       &lt;li&gt;Advanced: takes you to a page containing various lesser used, experimental or destructive commands.&lt;/li&gt;
       &lt;/ul&gt;
       &lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;li&gt;&lt;strong&gt;Text&lt;/strong&gt; input mode is similar to the Slider mode, but instead of sliders you enter RGB color values directly as text by tapping the keyboard icon.&lt;/li&gt;</source>
        <translation type="vanished">Le mode d&apos;entrée &lt;li&gt;&lt;strong&gt;Texte&lt;/strong&gt; est similaire au mode Curseur, mais au lieu des curseurs, vous entrez les valeurs de couleur RVB directement sous forme de texte en touchant l&apos;icône du clavier.&lt;/li&gt;</translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpUIMain.qml" line="58"/>
        <source>&lt;li&gt;&lt;strong&gt;Generators:&lt;/strong&gt; this is a collection of various functions which manipulate all colors of the theme. Some are more useful than others, some are just for fun.
&lt;ul&gt;
&lt;li&gt;&lt;i&gt;Randomizer&lt;/i&gt; shuffles the color values to generate a random theme&lt;/li&gt;
&lt;li&gt;&lt;i&gt;Filters&lt;/i&gt; will change the existing theme colors slightly, e.g. darken or brighten them&lt;/li&gt;
&lt;li&gt;&lt;i&gt;Scheme Generators&lt;/i&gt; is a collection of things which will create a full theme&lt;/li&gt;
&lt;/ul&gt;
&lt;/li&gt;</source>
        <translation>&lt;li&gt;&lt;strong&gt;Générateurs :&lt;/strong&gt; c&apos;est une collection de diverses fonctions qui manipulent toutes les couleurs du thème. Certaines sont plus utiles que d&apos;autres, d&apos;autres sont juste pour s&apos;amuser.
&lt;ul&gt;
&lt;li&gt;&lt;i&gt;Randomiseur&lt;/i&gt; mélange les valeurs des couleurs pour générer un thème aléatoire &lt;/li&gt;
&lt;li&gt;&lt;i&gt;Filtres&lt;/i&gt; modifie légèrement les couleurs du thème existant, par exemple en les assombrissant ou en les éclaircissant&lt;/li&gt;.
&lt;li&gt;&lt;i&gt;Générateurs de schémas&lt;/i&gt; est une collection de choses qui vont créer un thème complet&lt;/li&gt;
&lt;/ul&gt;
&lt;/li&gt;</translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpUIMain.qml" line="67"/>
        <source>&lt;li&gt;&lt;strong&gt;Swapper/Copier&lt;/strong&gt; is a helper mode which lets you copy the value of one color to another, or swap two values.&lt;/li&gt;</source>
        <translation>&lt;li&gt;&lt;strong&gt;Permuter/Copier&lt;/strong&gt; est un mode d&apos;aide qui vous permet de copier la valeur d&apos;une couleur sur une autre, ou de permuter deux valeurs.&lt;/li&gt;</translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpUIMain.qml" line="68"/>
        <source>&lt;li&gt;&lt;strong&gt;Jolla Original&lt;/strong&gt; you already know, a remake of the element used in the Ambience Settings&lt;/li&gt;</source>
        <translation>&lt;li&gt;&lt;strong&gt;Jolla Original&lt;/strong&gt; que vous connaissez déjà, un réfection de l&apos;élément utilisé dans les paramètres d&apos;ambiance&lt;/li&gt;</translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpUIMain.qml" line="83"/>
        <source>The Cupboard</source>
        <comment>Help Section</comment>
        <translation>Le Placard</translation>
    </message>
    <message>
        <source>&lt;li&gt;&lt;strong&gt;Jolla Original&lt;/strong&gt; you already know, it&apos;s a remake of the element used in the Ambience Settings&lt;/li&gt;</source>
        <translation type="vanished">&lt;li&gt;&lt;strong&gt;Jolla Original&lt;/strong&gt; you already know, it&apos;s a remake of the element used in the Ambience Settings&lt;/li&gt;</translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpUIMain.qml" line="86"/>
        <source>&lt;p&gt;
Cupboards are found to the right of the main page. These allow you to store your created palettes for re-use later.&lt;br /&gt;
The first page is the global Cupboard, where you can store any palette.
The second page is the Cupboard specific for the current Ambience, and its contents will change when the Ambience changes.
&lt;/p&gt;
&lt;p&gt;
Note that only Ambiences installed from a package can have a name, those created from Gallery will show as anonymous (for now)
&lt;/p&gt;
&lt;p&gt;
Per default the shelves are empty (showing all color pots as gray), but tapping the &lt;i&gt;%1&lt;/i&gt; button will save your current palette to that shelf, overwriting any values that may have been there.
The &lt;i&gt;%2&lt;/i&gt; button will load the stored palette and switch back to the main page.
&lt;/p&gt;
</source>
        <extracomment>arguments are the names of the menu entries &quot;Put on Shelf&quot; and &quot;Take to Lab&quot;</extracomment>
        <translation>&lt;p&gt;
Les placards se trouvent à droite de la page principale. Ils vous permettent de stocker vos palettes créées pour les réutiliser plus tard.&lt;br /&gt;
La première page est le Cupboard global, où vous pouvez stocker n&apos;importe quelle palette.
La deuxième page est le Cupboard spécifique à l&apos;Ambiance actuelle, et son contenu changera lorsque l&apos;Ambiance changera.
&lt;/p&gt;
&lt;p&gt;
Notez que seules les Ambiances installées depuis un paquet peuvent avoir un nom, celles créées depuis Gallery s&apos;afficheront comme anonymes (pour le moment).
&lt;/p&gt;
&lt;p&gt;
Par défaut, les étagères sont vides (affichant tous les pots de couleur en gris), mais en tapant sur le bouton &lt;i&gt;%1&lt;/i&gt;, votre palette actuelle sera enregistrée sur cette étagère, écrasant toutes les valeurs qui pouvaient s&apos;y trouver.
Le bouton &lt;i&gt;%2&lt;/i&gt; chargera la palette enregistrée et reviendra à la page principale.
&lt;/p&gt;
</translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpUIMain.qml" line="99"/>
        <source>Put on Shelf</source>
        <translation>Mettre sur l’Étagère</translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpUIMain.qml" line="99"/>
        <source>Take to Lab</source>
        <translation>Emporter au Labo</translation>
    </message>
    <message>
        <location filename="../qml/pages/help/HelpUIMain.qml" line="107"/>
        <source>Menus</source>
        <comment>Help Section</comment>
        <translation>Menus</translation>
    </message>
    <message>
        <source>&lt;p&gt;
       The PullDown menu contains the most used commands.
       &lt;p&gt;
       &lt;ul&gt;
       &lt;li&gt;Apply: The central command of the app. This will apply the current color theme to the system.&lt;/li&gt;
       &lt;li&gt;Reload: Loads the theme that is currently in use in the system.&lt;/li&gt;
       &lt;li&gt;Export: opens the Export Ambience page. See the &lt;b&gt;Export&lt;/b&gt; chapter for more.&lt;/li&gt;
       &lt;/ul&gt;
       &lt;/p&gt;
       &lt;p&gt;
       The PullUp menu at the bottom of the main page has various experimental or destructive commands.
       &lt;ul&gt;
       &lt;li&gt;Restart Lipstick: This will restart the whole Home Screen, closing all applications.&lt;/li&gt;
       &lt;li&gt;Restart ambienced: This will restart the ambience daemon, which will also reinitialize the colors from the current Ambience.&lt;/li&gt;
       &lt;li&gt;Reset: this will delete color values saved by the App in DConf. Use it if something went wrong and some UI elements have become unreadable.&lt;/li&gt;
       &lt;/ul&gt;
       &lt;/p&gt;
       &lt;/p&gt;</source>
        <translation type="vanished">&lt;p&gt;
       Le menu PullDown contient les commandes les plus utilisées.
       &lt;p&gt;
       &lt;ul&gt;
       &lt;li&gt;Appliquer : la commande centrale de l&apos;application. Elle permet d&apos;appliquer le thème de couleurs actuel au système.&lt;/li&gt;
       &lt;li&gt;Recharger : charge le thème qui est actuellement utilisé dans le système.&lt;/li&gt;
       &lt;li&gt;Exporter : ouvre la page d&apos;exportation d&apos;ambiance. Voir le chapitre &lt;b&gt;Export&lt;/b&gt; pour en savoir plus.&lt;/li&gt;
       &lt;/ul&gt;
       &lt;/p&gt;
       &lt;p&gt;
       Le menu PullUp en bas de la page principale comporte diverses commandes expérimentales ou destructives.
       &lt;ul&gt;
       &lt;li&gt;Redémarrer Lipstick : ceci redémarre tout l&apos;écran d&apos;accueil, en fermant toutes les applications.&lt;/li&gt;
       &lt;li&gt;Redémarrer ambienced : ceci redémarrera le démon ambience, qui réinitialisera également les couleurs de l&apos;Ambience actuelle.&lt;/li&gt;
       &lt;li&gt;Réinitialiser : ceci supprimera les valeurs de couleurs enregistrées par l&apos;application dans DConf. Utilisez-la si quelque chose s&apos;est mal passé et que certains éléments de l&apos;interface utilisateur sont devenus illisibles.&lt;/li&gt;
       &lt;/ul&gt;
       &lt;/p&gt;
       &lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;This is the where you edit the various colors.&lt;/p&gt;
&lt;p&gt;
On the top you find the input mode selector.
There are several ways to do this editing, called Input Modes, explained below.
The selector switches between them.
&lt;ul&gt;
&lt;li&gt;&lt;strong&gt;Slider&lt;/strong&gt; input mode is the main editing mode. 
There is a set of sliders for each color you can edit. Use them to adjust the colors using their red, green, blue (RGB) channels. 
Two slim indicators on top of each slider section shows the current, and the edited color, and the hex value will be shown in the area below them. If you tap that area, the sliders will reset to the value of the unedited color.&lt;br /&gt;
The button on the left side opens the Color Selector dialog from Sailfish Silica, in case you want to select a color from there.&lt;br /&gt;
At the bottom below the RGB sliders you will find the Glow editor. This works a little different than the others, allowing you to select the hue of any UI elements that have a glow effect. Below the slider there is a set of buttons that apply either black, white, or the four default colors to the Glow color.
&lt;li&gt;&lt;strong&gt;Text&lt;/strong&gt; input mode is similar to the Slider mode, but instead of sliders you enter RGB color values directly as text by tapping the keyboard icon.&lt;/li&gt;
&lt;li&gt;&lt;strong&gt;Generators:&lt;/strong&gt; this is a collection of various functions which manipulate all colors of the theme. Some are more useful than others, some are just for fun.
&lt;ul&gt;
&lt;li&gt;&lt;i&gt;Randomizer&lt;/i&gt; shuffles the color values to generate a random theme&lt;/li&gt;
&lt;li&gt;&lt;i&gt;Filters&lt;/i&gt; will change the existing theme colors slightly, e.g. darken or brighten them&lt;/li&gt;
&lt;li&gt;&lt;i&gt;Scheme Generators&lt;/i&gt; is a collection of things which will create a full theme&lt;/li&gt;
&lt;/ul&gt;
&lt;/li&gt;
&lt;li&gt;&lt;strong&gt;Swapper/Copier&lt;/strong&gt; is a helper mode which lets you copy the value of one color to another, or swap two values.&lt;/li&gt;
&lt;li&gt;&lt;strong&gt;Jolla Original&lt;/strong&gt; you already know, it&apos;s a remake of the element used in the Ambience Settings&lt;/li&gt;
&lt;/ul&gt;
&lt;/p&gt;
</source>
        <translation type="vanished">&lt;p&gt;This is the where you edit the various colours.&lt;/p&gt;
&lt;p&gt;
On the top you find the input mode selector.
There are several ways to do this editing, called Input Modes, explained below.
The selector switches between them.
&lt;ul&gt;
&lt;li&gt;&lt;strong&gt;Slider&lt;/strong&gt; input mode is the main editing mode. 
There is a set of sliders for each colour you can edit. Use them to adjust the colours using their red, green, blue (RGB) channels. 
Two slim indicators on top of each slider section shows the current, and the edited colour, and the hex value will be shown in the area below them. If you tap that area, the sliders will reset to the value of the unedited colour.&lt;br /&gt;
The button on the left side opens the Colour Selector dialog from Sailfish Silica, in case you want to select a colour from there.&lt;br /&gt;
At the bottom below the RGB sliders you will find the Glow editor. This works a little different than the others, allowing you to select the hue of any UI elements that have a glow effect. Below the slider there is a set of buttons that apply either black, white, or the four default colours to the Glow color.
&lt;li&gt;&lt;strong&gt;Text&lt;/strong&gt; input mode is similar to the Slider mode, but instead of sliders you enter RGB colour values directly as text by tapping the keyboard icon.&lt;/li&gt;
&lt;li&gt;&lt;strong&gt;Generators:&lt;/strong&gt; this is a collection of various functions which manipulate all colours of the theme. Some are more useful than others, some are just for fun.
&lt;ul&gt;
&lt;li&gt;&lt;i&gt;Randomizer&lt;/i&gt; shuffles the colour values to generate a random theme&lt;/li&gt;
&lt;li&gt;&lt;i&gt;Filters&lt;/i&gt; will change the existing theme colorus slightly, e.g. darken or brighten them&lt;/li&gt;
&lt;li&gt;&lt;i&gt;Scheme Generators&lt;/i&gt; is a collection of things which will create a full theme&lt;/li&gt;
&lt;/ul&gt;
&lt;/li&gt;
&lt;li&gt;&lt;strong&gt;Swapper/Copier&lt;/strong&gt; is a helper mode which lets you copy the value of one colour to another, or swap two values.&lt;/li&gt;
&lt;li&gt;&lt;strong&gt;Jolla Original&lt;/strong&gt; you already know, it&apos;s a remake of the element used in the Ambience Settings&lt;/li&gt;
&lt;/ul&gt;
&lt;/p&gt;
</translation>
    </message>
    <message>
        <source>The Cupboard</source>
        <translation type="vanished">The Cupboard</translation>
    </message>
    <message>
        <source>Menus</source>
        <translation type="vanished">Menus</translation>
    </message>
    <message>
        <source>&lt;p&gt;
       The PullDown menu contains the most used commands.
       &lt;ul&gt;
       &lt;li&gt;Apply: The central command of the app. This will apply the current color theme to the system.&lt;/li&gt;
       &lt;li&gt;Reload: two options to reset the app theme to the ones used by the system currently. &quot;from System&quot; loads the theme that is currently in use. &quot;from Config&quot; loads from the stored config in DConf. &lt;br /&gt;Both should have the same effect in practice.&lt;/li&gt;
       &lt;li&gt;Export: opens the Export Ambience page. See the &lt;b&gt;Export&lt;/b&gt; help section for more.&lt;/li&gt;
       &lt;/ul&gt;
       The PullUp menu at the bottom of the main page has various experimental or destructive commands.
       &lt;ul&gt;
       &lt;li&gt;Restart Lipstick: This will restart the whole Home Screen, closing all applications.&lt;/li&gt;
       &lt;li&gt;Restart ambienced: This will restart the ambience daemon, which will also reinitialize the colors from the current Ambience.&lt;/li&gt;
       &lt;li&gt;Reset: this will delete color values saved by the App in DConf. Use it if something went wrong and some UI elements have become unreadable.&lt;/li&gt;
       &lt;li&gt;&lt;/li&gt;
       &lt;/ul&gt;
       &lt;/p&gt;</source>
        <translation type="vanished">&lt;p&gt;
       The PullDown menu contains the most used commands.
       &lt;ul&gt;
       &lt;li&gt;Apply: The central command of the app. This will apply the current colour theme to the system.&lt;/li&gt;
       &lt;li&gt;Reload: two options to reset the app theme to the ones used by the system currently. &quot;from System&quot; loads the theme that is currently in use. &quot;from Config&quot; loads from the stored config in DConf. &lt;br /&gt;Both should have the same effect in practice.&lt;/li&gt;
       &lt;li&gt;Export: opens the Export Ambience page. See the &lt;b&gt;Export&lt;/b&gt; help section for more.&lt;/li&gt;
       &lt;/ul&gt;
       The PullUp menu at the bottom of the main page has various experimental or destructive commands.
       &lt;ul&gt;
       &lt;li&gt;Restart Lipstick: This will restart the whole Home Screen, closing all applications.&lt;/li&gt;
       &lt;li&gt;Restart ambienced: This will restart the ambience daemon, which will also reinitialize the colours from the current Ambience.&lt;/li&gt;
       &lt;li&gt;Reset: this will delete colour values saved by the App in DConf. Use it if something went wrong and some UI elements have become unreadable.&lt;/li&gt;
       &lt;li&gt;&lt;/li&gt;
       &lt;/ul&gt;
       &lt;/p&gt;</translation>
    </message>
</context>
<context>
    <name>LoadAmbience</name>
    <message>
        <location filename="../qml/pages/advanced/LoadAmbience.qml" line="32"/>
        <source>Load</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/LoadAmbience.qml" line="93"/>
        <source>Load Theme</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/LoadAmbience.qml" line="93"/>
        <source>Take to Lab</source>
        <translation type="unfinished">Emporter au Labo</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/LoadAmbience.qml" line="111"/>
        <source>Here you can load a .ambience (JSON) file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/LoadAmbience.qml" line="115"/>
        <source>current</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/LoadAmbience.qml" line="121"/>
        <source>file data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/LoadAmbience.qml" line="127"/>
        <source>File name </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/LoadAmbience.qml" line="129"/>
        <source>Click to select</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/LoadAmbience.qml" line="136"/>
        <source>File content</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/LoadAmbience.qml" line="145"/>
        <source>No file loaded</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LootBoxItem</name>
    <message>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="23"/>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="28"/>
        <source>ThemeColor</source>
        <translation>CouleurDuThème</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="27"/>
        <source>A Lootbox was delivered!</source>
        <translation>Une Caisse de récompense a été livrée !</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="28"/>
        <source>now has more shelves!</source>
        <translation>a maintenant plus d’étagères !</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="28"/>
        <source>Your persistence has been rewarded.</source>
        <translation>Votre persévérance a été récompensée.</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="29"/>
        <source>Your persistence has been rewarded!</source>
        <translation>Votre persévérance a été récompensée !</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="39"/>
        <source>Purchase Options</source>
        <translation>Options d’achat</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="48"/>
        <source>Payment* received!</source>
        <translation>Paiement* reçu !</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="48"/>
        <source>Buy more shelves</source>
        <translation>Acheter plus d’étagères</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="53"/>
        <source>Using Jolla Shop credentials to buy Storage Lootbox</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="70"/>
        <source>Thank you for your purchase!&lt;br /&gt;Your extra shelves will be delivered in the next update!</source>
        <translation>Merci de votre achat !&lt;br /&gt;Vos étagères supplémentaires seront livrées dans la prochaine mise à jour !</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/LootBoxItem.qml" line="84"/>
        <source>*) No seriously there are no in-app-purchases in this app or Lootboxes in the Jolla Shop. That would be ridiculous.&lt;br /&gt;No funds were transfered.&lt;br /&gt; In fact, nothing really happened just now.&lt;br /&gt;…or did it?</source>
        <translation></translation>
    </message>
    <message>
        <source>*) No seriously there are no in-app-purchases in this app or Lootboxes in the Jolla Shop. That would be ridiculous.&lt;br /&gt;No funds were transfered.&lt;br /&gt; In fact, nothing really happened just now.&lt;br /&gt;...or did it?</source>
        <translation type="vanished">*) No seriously there are no in-app-purchases in this app or Lootboxes in the Jolla Shop. That would be ridiculous.&lt;br /&gt;No funds were transfered.&lt;br /&gt; In fact, nothing really happened just now.&lt;br /&gt;...or did it?</translation>
    </message>
</context>
<context>
    <name>SaveAmbience</name>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="47"/>
        <source>Export Functions</source>
        <translation>Fonctions d’exportation</translation>
    </message>
    <message>
        <source>Here you can export your creation to a .ambience (json) file.&lt;br /&gt;
                      This is not terribly useful at the moment, but you can use that file to build your own ambiences by adding an image, optionally some sounds, and package the whole thing as an RPM.
                     </source>
        <translation type="vanished">Here you can export your creation to a .ambience (json) file.&lt;br /&gt;
                      This is not terribly useful at the moment, but you can use that file to build your own ambiences by adding an image, optionally some sounds, and package the whole thing as an RPM.
                     </translation>
    </message>
    <message>
        <source>Here you can export your creation to a .ambience (json) file.&lt;br /&gt;
                      This file can be picked up by the ThemeColor® RPM Builder™ companion tool which will package the whole thing as an RPM which you can then install.
                     </source>
        <translation type="vanished">Here you can export your creation to a .ambience (json) file.&lt;br /&gt;
                      This file can be picked up by the ThemeColour® RPM Builder™ companion tool which will package the whole thing as an RPM which you can then install.
                     </translation>
    </message>
    <message>
        <source>Here you can export your creation to an .ambience (JSON) file.&lt;br /&gt;
                      This file can be picked up by the ThemeColor® RPM Builder™ companion tool, which will package the whole thing as an RPM you can then install.
                     </source>
        <translation type="vanished">Here you can export your creation to an .ambience (JSON) file.&lt;br /&gt;
                      This file can be picked up by the ThemeColour® RPM Builder™ companion tool, which will package the whole thing as an RPM you can then install.
                     </translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="61"/>
        <source>Here you can export your creation to a .ambience (JSON) file, create an installable package from it, and optionally install that on your device.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="63"/>
        <source>To do that, first enter a name for your %1, then tap the &amp;quot;%2&amp;quot; button. After this, you can either edit the file, or generate a package from it.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="65"/>
        <source>Both the .ambience file as well as any built packages will be stored in the %1 folder. (%2)</source>
        <comment>first argument is the short name of the locatin, second argument is the full path</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="65"/>
        <source>Documents</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="83"/>
        <source>Export to File</source>
        <translation>Exporter dans un fichier</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="88"/>
        <source>Ambience Name</source>
        <translation>Nom de l’ambiance</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="109"/>
        <source>Click to prepare</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="110"/>
        <source>File Name</source>
        <translation>Nom du fichier</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="196"/>
        <source>Open File</source>
        <translation>Ouvrir le fichier</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="197"/>
        <source>Launch Builder</source>
        <translation>Lancer le Constructeur</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="68"/>
        <source>Disclaimer</source>
        <translation>Clause de non responsabilité</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="35"/>
        <source>Ambience Package</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="80"/>
        <source>If you decide to share or otherwise distribute the package make sure you respect copy- and other rights for any content included.</source>
        <translation></translation>
    </message>
    <message>
        <source>If you decide to share or otherwise distribute the package make sure you respect copy- and other rights for any content included in them.</source>
        <translation type="vanished">If you decide to share or otherwise distribute the package make sure you respect copy- and other rights for any content included in them.</translation>
    </message>
    <message>
        <source>Help</source>
        <translation type="obsolete">Help</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="63"/>
        <source>Ambience</source>
        <translation type="unfinished">Ambience</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/SaveAmbience.qml" line="90"/>
        <source>A cool Ambience Name</source>
        <translation>Un super nom d’ambiance</translation>
    </message>
    <message>
        <source>Click to export</source>
        <translation type="vanished">Cliquer pour exporter</translation>
    </message>
</context>
<context>
    <name>SaveSlot</name>
    <message>
        <location filename="../qml/components/saver/SaveSlot.qml" line="40"/>
        <location filename="../qml/components/saver/SaveSlot.qml" line="41"/>
        <location filename="../qml/components/saver/SaveSlot.qml" line="42"/>
        <source>Ambience</source>
        <translation>Ambiance</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/SaveSlot.qml" line="40"/>
        <location filename="../qml/components/saver/SaveSlot.qml" line="41"/>
        <location filename="../qml/components/saver/SaveSlot.qml" line="42"/>
        <location filename="../qml/components/saver/SaveSlot.qml" line="44"/>
        <source>Shelf</source>
        <translation>Étagère</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/SaveSlot.qml" line="40"/>
        <source>default</source>
        <translation>par défaut</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/SaveSlot.qml" line="41"/>
        <source>night mode</source>
        <translation>mode nuit</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/SaveSlot.qml" line="71"/>
        <source>Take to Lab</source>
        <translation>Emporter au Labo</translation>
    </message>
    <message>
        <location filename="../qml/components/saver/SaveSlot.qml" line="72"/>
        <source>Put on Shelf</source>
        <translation>Mettre sur l’Étagère</translation>
    </message>
</context>
<context>
    <name>Saver</name>
    <message>
        <location filename="../qml/pages/Saver.qml" line="99"/>
        <source>Global Cupboard</source>
        <translation>Placard global</translation>
    </message>
    <message>
        <location filename="../qml/pages/Saver.qml" line="129"/>
        <source>Clean out this cupboard</source>
        <translation>Nettoyer ce placard</translation>
    </message>
    <message>
        <location filename="../qml/pages/Saver.qml" line="129"/>
        <source>Spring Clean</source>
        <translation>Nettoyage de printemps</translation>
    </message>
</context>
<context>
    <name>SaverPlus</name>
    <message>
        <source>anonymous</source>
        <translation type="vanished">anonyme</translation>
    </message>
    <message>
        <location filename="../qml/pages/SaverPlus.qml" line="115"/>
        <source>Ambience Cupboard</source>
        <translation>Placard d’ambiances</translation>
    </message>
    <message>
        <source>default</source>
        <translation type="vanished">default</translation>
    </message>
    <message>
        <source>night mode</source>
        <translation type="vanished">night mode</translation>
    </message>
    <message>
        <location filename="../qml/pages/SaverPlus.qml" line="14"/>
        <source>User Ambience</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SaverPlus.qml" line="103"/>
        <source>System Ambience</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/SaverPlus.qml" line="169"/>
        <source>Clean out this cupboard</source>
        <translation>Nettoyer ce placard</translation>
    </message>
    <message>
        <location filename="../qml/pages/SaverPlus.qml" line="169"/>
        <source>Spring Clean</source>
        <translation>Nettoyage de printemps</translation>
    </message>
</context>
<context>
    <name>ShowRoom</name>
    <message>
        <source>A very long line showing Text in </source>
        <translation type="vanished">A very long line showing Text in </translation>
    </message>
    <message>
        <source>Color</source>
        <translation type="vanished">Colour</translation>
    </message>
    <message>
        <source>Primary Color</source>
        <translation type="vanished">Primary Colour</translation>
    </message>
    <message>
        <source>Secondary Color</source>
        <translation type="vanished">Secondary Colour</translation>
    </message>
    <message>
        <source>Highlight Color</source>
        <translation type="vanished">Highlight Colour</translation>
    </message>
    <message>
        <source>Secondary Highlight Color</source>
        <translation type="vanished">Secondary Highlight Colour</translation>
    </message>
    <message>
        <source>Error Color</source>
        <translation type="vanished">Error Colour</translation>
    </message>
    <message>
        <source>Background Color</source>
        <translation type="vanished">Background Colour</translation>
    </message>
    <message>
        <source>four kinds of background overlay opacities and colors</source>
        <translation type="vanished">four kinds of background overlay opacities and colours</translation>
    </message>
    <message>
        <source>Text</source>
        <translation type="vanished">Text</translation>
    </message>
    <message>
        <source>Highlight Background Color</source>
        <translation type="vanished">Highlight Background Colour</translation>
    </message>
    <message>
        <source>Overlay Background Color</source>
        <translation type="vanished">Overlay Background Colour</translation>
    </message>
    <message>
        <source>Dim Highlight Color</source>
        <translation type="vanished">Dim Highlight Colour</translation>
    </message>
    <message>
        <source>Progress Bar Demo</source>
        <translation type="vanished">Progress Bar Demo</translation>
    </message>
    <message>
        <source>Tap to restart Demos</source>
        <translation type="vanished">Tap to restart Demos</translation>
    </message>
    <message>
        <source>Remorse Item Demo</source>
        <translation type="vanished">Remorse Item Demo</translation>
    </message>
    <message>
        <source>MenuItem</source>
        <translation type="vanished">MenuItem</translation>
    </message>
    <message>
        <source>selected</source>
        <translation type="vanished">selected</translation>
    </message>
    <message>
        <source>disabled</source>
        <translation type="vanished">disabled</translation>
    </message>
    <message>
        <source>Button</source>
        <translation type="vanished">Button</translation>
    </message>
    <message>
        <location filename="../qml/components/ShowRoom.qml" line="16"/>
        <source>Text Elements</source>
        <translation>Éléments de texte</translation>
    </message>
    <message>
        <location filename="../qml/components/ShowRoom.qml" line="24"/>
        <source>UI Elements</source>
        <translation>Éléments de l’IU</translation>
    </message>
</context>
<context>
    <name>ShowRoomMini</name>
    <message>
        <location filename="../qml/components/ShowRoomMini.qml" line="21"/>
        <source>Mini</source>
        <comment>small showroom</comment>
        <translation>Mini</translation>
    </message>
    <message>
        <location filename="../qml/components/ShowRoomMini.qml" line="21"/>
        <source>Showroom</source>
        <translation>Salle d’exposition</translation>
    </message>
</context>
<context>
    <name>ShowRoomText</name>
    <message>
        <source>A very long line showing Text in </source>
        <translation type="vanished">A very long line showing Text in </translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="17"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="29"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="38"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="63"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="72"/>
        <source>Primary Color</source>
        <translation>Couleur principale</translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="17"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="18"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="19"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="20"/>
        <source>A very long line showing Text in</source>
        <translation>Une très longue ligne montrant le texte en</translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="18"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="30"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="39"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="64"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="73"/>
        <source>Secondary Color</source>
        <translation>Couleur secondaire</translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="19"/>
        <source>Highlight Color</source>
        <translation>Couleur de surbrillance</translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="20"/>
        <source>Secondary Highlight Color</source>
        <translation>Couleur de surbrillance secondaire</translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="63"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="64"/>
        <source>Cover Background Color</source>
        <translation>Couleur de fond de l&apos;en-tête</translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="72"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="73"/>
        <source>Wallpaper Overlay Color</source>
        <translation>Couleur de superposition du fond d’écran</translation>
    </message>
    <message>
        <source>Error Color</source>
        <translation type="vanished">Error Colour</translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="29"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="30"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="38"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="39"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="63"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="64"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="72"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="73"/>
        <source>Text</source>
        <translation>Texte</translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="29"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="30"/>
        <source>Highlight Background Color</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="38"/>
        <location filename="../qml/components/showroom/ShowRoomText.qml" line="39"/>
        <source>Overlay Background Color</source>
        <translation></translation>
    </message>
    <message>
        <source>Dim Highlight Color</source>
        <translation type="vanished">Dim Highlight Colour</translation>
    </message>
</context>
<context>
    <name>ShowRoomUI</name>
    <message>
        <location filename="../qml/components/showroom/ShowRoomUI.qml" line="32"/>
        <source>Progress Bar Demo</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomUI.qml" line="35"/>
        <source>Remorse Item Demo</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomUI.qml" line="90"/>
        <location filename="../qml/components/showroom/ShowRoomUI.qml" line="91"/>
        <location filename="../qml/components/showroom/ShowRoomUI.qml" line="92"/>
        <source>MenuItem</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomUI.qml" line="92"/>
        <source>selected</source>
        <translation>sélectionné</translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomUI.qml" line="91"/>
        <source>disabled</source>
        <translation>désactivé</translation>
    </message>
    <message>
        <location filename="../qml/components/showroom/ShowRoomUI.qml" line="124"/>
        <location filename="../qml/components/showroom/ShowRoomUI.qml" line="125"/>
        <source>Button</source>
        <translation>Bouton</translation>
    </message>
</context>
<context>
    <name>TransparencyEditor</name>
    <message>
        <location filename="../qml/pages/advanced/TransparencyEditor.qml" line="41"/>
        <source>Edit Transparency</source>
        <translation>Modifier la transparence</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/TransparencyEditor.qml" line="49"/>
        <source>This will edit the alpha channel of the color. Note that in the Sailfish UI many elements use their own transparency values and are not affected by the alpha channel of the color. Remorse Timers are one such example.</source>
        <translation></translation>
    </message>
    <message>
        <source>General Opacity Values</source>
        <translation type="vanished">General Opacity Values</translation>
    </message>
    <message>
        <source>Four opacity types are saved in the theme: Faint, Medium, Low, and Overlay. The slider below sets general Opacity, the type values are computed from your selection.</source>
        <translation type="vanished">Four opacity types are saved in the theme: Faint, Medium, Low, and Overlay. The slider below sets general Opacity, the type values are computed from your selection.</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/TransparencyEditor.qml" line="78"/>
        <source>Highlight Background Opacity</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/TransparencyEditor.qml" line="79"/>
        <source>This is used e.g. for Pulley Menu background.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/TransparencyEditor.qml" line="81"/>
        <source>Wallpaper Overlay Opacity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/TransparencyEditor.qml" line="83"/>
        <source>Cover Overlay Opacity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/TransparencyEditor.qml" line="84"/>
        <source>Transparency of application covers.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/TransparencyEditor.qml" line="90"/>
        <source>Color Alpha Channel</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/TransparencyEditor.qml" line="91"/>
        <source>Here you can edit Alpha channels for colors that have one.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/TransparencyEditor.qml" line="92"/>
        <source>Secondary Color</source>
        <translation>Couleur secondaire</translation>
    </message>
    <message>
        <source>Highlight Color</source>
        <translation type="vanished">Highlight Colour</translation>
    </message>
    <message>
        <location filename="../qml/pages/advanced/TransparencyEditor.qml" line="94"/>
        <source>Secondary Highlight Color</source>
        <translation>Couleur de surbrillance secondaire</translation>
    </message>
    <message>
        <source>Highlight Background Color</source>
        <translation type="vanished">Highlight Background Colour</translation>
    </message>
</context>
</TS>
