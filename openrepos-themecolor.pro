#######################################################################
# This is for translation ONLY, use build.sh or release.sh for building
#######################################################################

TEMPLATE = aux
TARGET = openrepos-themecolor
CONFIG += sailfishapp_qml sailfishapp_i18n

lupdate_only {
SOURCES += \
    qml/$${TARGET}.qml \
    $$files(qml/components/*.qml) \
    $$files(qml/components/showroom/*.qml) \
    $$files(qml/components/generators/*.qml) \
    $$files(qml/components/controls/*.qml) \
    $$files(qml/components/saver/*.qml) \
    $$files(qml/components/saver/images/*.qml) \
    $$files(qml/pages/*.qml) \
    $$files(qml/pages/help/*.qml) \
    $$files(qml/pages/advanced/*.qml) \
    $$files(qml/cover/*.qml)

}

# Input
TRANSLATIONS += translations/openrepos-themecolor-de.ts \
                translations/openrepos-themecolor-en_GB.ts \
                translations/openrepos-themecolor-en_IE.ts \
                translations/openrepos-themecolor-es.ts \
                translations/openrepos-themecolor-fr.ts \
                translations/openrepos-themecolor-nb_NO.ts \
                translations/openrepos-themecolor-sv.ts \
                translations/openrepos-themecolor-zh_CN.ts


